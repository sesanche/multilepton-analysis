# remove 2018 prefiring

import CMGRDF

from CMGRDF import *
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4

from CMGRDF.modifiers import Prepend 
from data.trigger import trigger_definition_sequence
from utils.dataDriven import FlipRateDefine, FakeRateDefine
from utils.leptonScaleFactors  import leptonSFsequences
from utils.btagSF              import btagsf_sequences
from utils.jmesequences        import jme_sequences, jme_sequences_after
from data.datasets import data_3l as all_data_run2
from data.datasets_run3 import data_3l as all_data_run3

all_data = all_data_run2+all_data_run3

import ROOT
ROOT.EnableImplicitMT(16)
import numpy as np

verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(),
                                                  ROOT.Experimental.ELogLevel.kInfo)# 255-ROOT.Experimental.ELogLevel.kDebug)

from CMGRDF.stat import DatacardWriter

from data.common_selections import lepton_selection, tau_selection, jet_selection_notau, jet_selection_forward
eras  =  ["2018", "2016APV", "2016", "2017", "2022", "2022EE"] 

lumi = {
    '2016APV': 19.5,
    '2016': 16.8,
    '2017': 41.5,
    '2018': 59.7,
    '2022': 8.0,
    "2022EE" : 26.7,
}


P="root://eoscms.cern.ch//eos/cms/store/cmst3/group/tthlep/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root"




runSnapshot=False
runPlots=False
testSignal=False
runCards=True


ROOT.gInterpreter.Declare(open( "functions.cc").read())

cuts=[         AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter && Flag_ecalBadCalibFilter"),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_2lss"),
               lepton_selection,
               Cut("trilep", "nLepFO>=3"),
               Cut("cleanup", "minMllFO > 20"),
               Cut("pt251515", "(LepFO_conePt[0] > 25) && (LepFO_conePt[1] > 15) && (LepFO_conePt[2] > 15)"),
               Cut("TTT", "(LepFO_isTight[0]) && (LepFO_isTight[1]) && (LepFO_isTight[2])"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Define( "nMuons", "(abs(LepFO_pdgId[0]) == 13) +(abs(LepFO_pdgId[1]) == 13) +(abs(LepFO_pdgId[2]) == 13) "),
               jme_sequences,
               jet_selection_forward,
               jme_sequences_after,
               leptonSFsequences,
               btagsf_sequences,
               AddWeight("mcWeight", "puWeight*leptonSF_weight_3l*leptonSFreco_weight_3l*btagSF_fixedWP*weight_jetPUId", onData=False, onDataDriven=False), #
                              

]



cuts_onz_bjet = Flow("3l_onz_bjet", cuts + [
     Cut( "1bl", "nBJetMedium25>0"),
     Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15"),
 ])
cuts_offz_bjet = Flow("3l_offz_bjet", cuts + [
    Cut( "1bl", "nBJetMedium25>0"),
    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) > 15"),
])
cuts_offz_nobjet = Flow("3l_offz_nobjet", cuts + [
    Cut( "1bl", "nBJetMedium25==0"),
    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) > 15"),
])



plots=[
    
    Plot("lep1_pt"  , "LepFO_conePt[0]" , (20,15,200)),
    Plot("lep2_pt"  , "LepFO_conePt[1]" , (20,15,200)),
    Plot("lep3_pt"  , "LepFO_conePt[2]" , (20,15,200)),
    Plot("nJet"     , "nJet25"          , (5,-0.5,4.5), moreY=2.0),

    Plot("nBJetMedium25", "nBJetMedium25"       , (5,-0.5,4.5)),
    Plot("nBJetLoose25", "nBJetLoose25"       , (5,-0.5,4.5)),
    
]
maker = Processor(cache=SimpleCache())



maker.book(all_data  , lumi, cuts_onz_bjet  , plots, eras=eras, withUncertainties=True)
maker.book(all_data  , lumi, cuts_offz_bjet  , plots, eras=eras, withUncertainties=True)
maker.book(all_data  , lumi, cuts_offz_nobjet  , plots, eras=eras, withUncertainties=True)

results=maker.runPlots()
PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=False, noStackSignals=True).printSet(results, "plots_oct10/{flow}/{era}",  maxRatioRange=(0.5,1.5), showRatio=True)




    
    
