#include <ROOT/RVec.hxx>

ROOT::RVec<float> conept_TTH( const ROOT::RVec<int>& pdgId, const ROOT::RVec<int>& mediumId,
                              const ROOT::RVec<float>& mvaTTHUL, const ROOT::RVec<float>& jetRelIso,
                              const ROOT::RVec<float>& pt, float mvaelwp, float mvamuwp)
{

  auto pt_mask =   ((abs(pdgId)!=11) & (abs(pdgId)!=13));
  pt_mask = (pt_mask) | ((abs(pdgId)==13) & ( mediumId>0) & (mvaTTHUL > mvamuwp));
  pt_mask = (pt_mask) | ((abs(pdgId)==11) & (mvaTTHUL > mvaelwp));

  return pt * pt_mask + (!pt_mask)*0.90 * pt * (1 + jetRelIso);

}

ROOT::RVec<float> conept_run3( const ROOT::RVec<int>& pdgId,
                              const ROOT::RVec<float>& mvaTTH, const ROOT::RVec<float>& jetRelIso,
                              const ROOT::RVec<float>& pt, float mvaelwp, float mvamuwp)
{

  auto pt_mask =   ((abs(pdgId)!=11) & (abs(pdgId)!=13));
  pt_mask = (pt_mask) | ((abs(pdgId)==13) & (mvaTTH > mvamuwp));
  pt_mask = (pt_mask) | ((abs(pdgId)==11) & (mvaTTH > mvaelwp));

  return pt * pt_mask + (!pt_mask)*0.90 * pt * (1 + jetRelIso);

}

ROOT::RVec<bool> isMatchCharge( const ROOT::RVec<int>& lep_pdgid, const ROOT::RVec<int>& lep_genIdx,
                                const ROOT::RVec<int>& gen_pdgId)
{
  ROOT::RVec<bool> mask(lep_pdgid.size(), 0);
  for (int i=0; i < lep_pdgid.size(); ++i){
    if (lep_genIdx[i] < 0) continue; // default is 0
    if (lep_pdgid[i]*gen_pdgId[lep_genIdx[i]] > 0) mask[i]=true;
  }
  return mask;
}


ROOT::RVec<bool> ttH_idEmu_cuts_E3( const ROOT::RVec<int>& pdgid, const ROOT::RVec<float>& hoe,
                                    const ROOT::RVec<float>& eta, const ROOT::RVec<float>& deltaEtaSC,
                                    const ROOT::RVec<float>& eInvMinusPInv, const ROOT::RVec<float>& sieie ){

  auto id_mask  = (abs(pdgid) == 13);
  auto hoe_mask = (hoe>=(0.10-0.00*(abs(eta+deltaEtaSC)>1.479)));
  auto eInvMinusPInv_mask=(eInvMinusPInv<=-0.04);
  auto sie_mask=(sieie>=(0.011+0.019*(abs(eta+deltaEtaSC)>1.479)));
  
  return id_mask | ((!hoe_mask) & (!eInvMinusPInv_mask) & (!sie_mask));

}

auto bound_to( ROOT::RVec<float> value, float x1, float x2){
  return value*(value >= x1)*(value < x2)+x1*(value<x1)+x2*(value>=x2);
    
}

ROOT::RVec<float> smoothBFlav(const ROOT::RVec<float> jetpt, float ptmin, float ptmax, float wploose, float wpmedium, float scale_loose=1.0)
{
  auto x = bound_to( (jetpt - ptmin)/(ptmax-ptmin), 0.0, 1.0);
  return x*wploose*scale_loose + (1-x)*wpmedium;
}

ROOT::RVec<float> smoothSIP(const ROOT::RVec<float> pt, float sipmin, float sipmax, float ptmin, float ptmax)
{
  auto x = bound_to( (pt - ptmin)/(ptmax-ptmin), 0.0, 1.0);
  return x*sipmax+(1-x)*sipmin;
}



ROOT::RVec<bool> isLoose( const ROOT::RVec<float>& miniPFRelIso_all, const ROOT::RVec<float>& sip3d,
                          const ROOT::RVec<int>& pdgId, const ROOT::RVec<int>& lostHits, const ROOT::RVec<int>& looseId)
{
  return (miniPFRelIso_all<0.4) & (sip3d < 8) & ( (abs(pdgId) != 11) || (lostHits<=1)) & ( (abs(pdgId) != 13) || (looseId!=0));
}

ROOT::RVec<bool> isLoose_run3( const ROOT::RVec<float>& miniPFRelIso_all, const ROOT::RVec<float>& sip3d,
                               const ROOT::RVec<int>& pdgId, const ROOT::RVec<int>& lostHits, const ROOT::RVec<int>& mediumId,
                               const ROOT::RVec<float>& etaSC )
{
  return (miniPFRelIso_all<0.4) & (sip3d < 8) & ( (abs(pdgId) != 11) || ((lostHits<=1) & ((abs(etaSC) < 1.4442) | (abs(etaSC) > 1.566)))) & ( (abs(pdgId) != 13) || (mediumId!=0));
}


ROOT::RVec<bool> isFO_TTH( const ROOT::RVec<float>& conePt, const ROOT::RVec<float>& jetBTagDeepFlav,
                           const ROOT::RVec<int>& pdgId, const ROOT::RVec<bool>& ttH_idEmu_cuts_E3,
                           const ROOT::RVec<float>& mvaTTHUL, const ROOT::RVec<float>& pt,
                           const ROOT::RVec<float>& jetRelIso, const ROOT::RVec<int>& mvaFall17V2noIso_WP90,
                           float bL, float bM,
                           const ROOT::RVec<int>& lostHits, const ROOT::RVec<int>& convVeto, float mvaelwp, float mvamuwp)
{



  auto el_mask = (abs(pdgId) == 11 );
  auto mvacut = mvaelwp*el_mask + (!el_mask)*mvamuwp;
  auto djsmoothcut = smoothBFlav(0.9*pt*(1+jetRelIso), 20, 45, bL, bM);
  auto ret =  ((conePt > 10) & (jetBTagDeepFlav < bM) & ttH_idEmu_cuts_E3 & ((abs(pdgId) == 13) | ((lostHits == 0) & (convVeto !=0 ))) & ( (mvaTTHUL > mvacut) | \
                                                                                                                                           ((abs(pdgId) == 13) & (jetBTagDeepFlav<djsmoothcut) & (jetRelIso < 0.50)) | \
                                                                                                                                           ((abs(pdgId) == 11) & (jetBTagDeepFlav<djsmoothcut) & mvaFall17V2noIso_WP90 & (jetRelIso < 1.0))));

  return ret; 

                                                                    

}

ROOT::RVec<bool> isFO_run3( const ROOT::RVec<float>& conePt, const ROOT::RVec<float>& jetBTagDeepFlav,
                            const ROOT::RVec<int>& pdgId, const ROOT::RVec<bool>& ttH_idEmu_cuts_E3,
                            const ROOT::RVec<float>& mvaTTH, const ROOT::RVec<float>& pt,
                            const ROOT::RVec<float>& jetRelIso, const ROOT::RVec<float>& sip3d, const ROOT::RVec<float>& mvaIso,
                            float bL, float bM,
                            const ROOT::RVec<int>& lostHits, const ROOT::RVec<int>& convVeto, float mvaelwp, float mvamuwp)
{


  //  std::cout << "Running this function" << std::endl;
  auto el_mask = (abs(pdgId) == 11 );
  auto mvacut = mvaelwp*el_mask + (!el_mask)*mvamuwp;
  auto djsmoothcut = smoothBFlav(0.9*pt*(1+jetRelIso), 20, 45, bL, bM);
  auto sipmoothcut = smoothSIP(  0.9*pt*(1+jetRelIso), 2.5,8.0, 15, 45);
  auto ret =  ((conePt > 15) & (jetBTagDeepFlav < bM) & ttH_idEmu_cuts_E3 & ((abs(pdgId) == 13) | ((lostHits == 0) & (convVeto !=0 ))) & ( (mvaTTH > mvacut) | \
                                                                                                                                           ((abs(pdgId) == 13) & (jetBTagDeepFlav<djsmoothcut) & (jetRelIso < 0.50) & (sip3d < sipmoothcut)) | \
                                                                                                                                           ((abs(pdgId) == 11) & (jetRelIso < 1.0) & (mvaIso > 0.6))));

  return ret; 

                                                                    

}

ROOT::RVec<bool> isTight( const ROOT::RVec<bool>& isFO, const ROOT::RVec<int>& pdgId,
                          const ROOT::RVec<int>& mediumId, const ROOT::RVec<float>& mvaTTHUL, float mvaelwp, float mvamuwp){
  auto el_mask = (abs(pdgId) == 11 );
  auto mvacut = mvaelwp*el_mask + (!el_mask)*mvamuwp;
  return (isFO) &  ((abs(pdgId) != 13) | (mediumId)) & ( mvaTTHUL > mvacut );
}

ROOT::RVec<bool> isTight_run3( const ROOT::RVec<bool>& isFO, const ROOT::RVec<int>& pdgId,
                               const ROOT::RVec<float>& mvaTTHUL, float mvaelwp, float mvamuwp){
  auto el_mask = (abs(pdgId) == 11 );
  auto mvacut = mvaelwp*el_mask + (!el_mask)*mvamuwp;
  return (isFO)  & ( mvaTTHUL > mvacut );
}


int dummyPrint( const ROOT::RVec<float>& isSelected, const ROOT::RVec<float>& eta, const ROOT::RVec<float>& phi){
  for (int i=0; i<isSelected.size(); ++i){
    std::cout << isSelected[i] << " ";
  }
  return 1;
}

int printEvt( long int event, long int lumi, int run, std::string suffix=""){
  std::cout << run << "|" << event << "|" << lumi << suffix <<  std::endl;
  return 1;
}

int printVar( float var ){
  std::cout << var << std::endl;
  return 1;
}

float mt_2(float pt1, float phi1, float pt2, float phi2) {
  return std::sqrt(2*pt1*pt2*(1-std::cos(phi1-phi2)));
}
