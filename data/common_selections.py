import CMGRDF

from CMGRDF import *
from data.btagWPs import _btagWPs
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4

run2eras=[ "2016APV", "2016", "2017", "2018"]
run3eras=["2022", "2022EE"]

lepton_selection = [
    Define(  "mvaelwp", "0.9", eras=run2eras),
    Define(  "mvamuwp", "0.85", eras=run2eras),
    [Define( "bTagCut_loose" , "%f"%_btagWPs[f"DeepFlav_UL{era}_L"][1], eras=[era]) for era in run2eras + run3eras],
    [Define( "bTagCut_medium", "%f"%_btagWPs[f"DeepFlav_UL{era}_M"][1], eras=[era]) for era in run2eras + run3eras],
    [Define( "bTagCut_tight" , "%f"%_btagWPs[f"DeepFlav_UL{era}_T"][1], eras=[era]) for era in run2eras + run3eras],
    # Run 2 selection
    Define(  "LepGood_conePt", "conept_TTH( LepGood_pdgId, LepGood_mediumId, LepGood_mvaTTHUL, LepGood_jetRelIso, LepGood_pt, mvaelwp, mvamuwp)", eras=run2eras),
    Define(  "LepGood_ttH_idEmu_cuts_E3", "ttH_idEmu_cuts_E3( LepGood_pdgId, LepGood_hoe, LepGood_eta, LepGood_deltaEtaSC,LepGood_eInvMinusPInv,LepGood_sieie )", eras=run2eras),
    Define(  "LepGood_isLoose", "isLoose( LepGood_miniPFRelIso_all, LepGood_sip3d, LepGood_pdgId, LepGood_lostHits, LepGood_looseId)", eras=run2eras),
    Define(  "LepGood_isFO", "(LepGood_isLoose) & (isFO_TTH( LepGood_conePt, LepGood_jetBTagDeepFlav, LepGood_pdgId, LepGood_ttH_idEmu_cuts_E3, LepGood_mvaTTHUL, LepGood_pt, LepGood_jetRelIso, LepGood_mvaFall17V2noIso_WP90, bTagCut_loose, bTagCut_medium, LepGood_lostHits, LepGood_convVeto, mvaelwp, mvamuwp))", eras=run2eras),
    Define(  "LepGood_isTight", "isTight( LepGood_isFO, LepGood_pdgId, LepGood_mediumId, LepGood_mvaTTHUL, mvaelwp, mvamuwp)", eras=run2eras),
    Define(  "Jet_analysisPUId", "(Jet_puId > 0) | (Jet_pt > 50)", eras=run2eras), # loose pu id in central jets below 50 gev, to be consistent with btagging sfs

    # Run3 selection
    Define(  "mvaelwp", "0.9", eras=run3eras),
    Define(  "mvamuwp", "0.64", eras=run3eras),
    Define(  "LepGood_conePt", "conept_run3( LepGood_pdgId, LepGood_mvaTTH_run3, LepGood_jetRelIso, LepGood_pt, mvaelwp, mvamuwp)", eras=run3eras),
    Define(  "LepGood_ttH_idEmu_cuts_E3", "ttH_idEmu_cuts_E3( LepGood_pdgId, LepGood_hoe, LepGood_eta, LepGood_deltaEtaSC,LepGood_eInvMinusPInv,LepGood_sieie )", eras=run3eras),
    Define(  "LepGood_EEveto", "(abs(LepGood_pdgId) == 13) |  ((LepGood_eta + LepGood_deltaEtaSC) < 1.56) | (LepGood_seediEtaOriX > 45) | (LepGood_seediPhiOriY < 72)", eras=["2022EE"]),
    Define(  "LepGood_EEveto", "LepGood_pt>=0", eras=["2022"]), # true flag
    Define(  "LepGood_isLoose", "LepGood_EEveto & (isLoose_run3( LepGood_miniPFRelIso_all, LepGood_sip3d, LepGood_pdgId, LepGood_lostHits, LepGood_mediumId, LepGood_eta+LepGood_deltaEtaSC))", eras=run3eras),
    Define(  "LepGood_isFO", "(LepGood_isLoose) & (isFO_run3( LepGood_conePt, LepGood_jetBTagDeepFlav, LepGood_pdgId, LepGood_ttH_idEmu_cuts_E3, LepGood_mvaTTH_run3, LepGood_pt, LepGood_jetRelIso, LepGood_sip3d, LepGood_mvaIso, bTagCut_loose, bTagCut_medium, LepGood_lostHits, LepGood_convVeto, mvaelwp, mvamuwp))", eras=run3eras),
    Define(  "LepGood_isTight", "isTight_run3( LepGood_isFO, LepGood_pdgId, LepGood_mvaTTH_run3, mvaelwp, mvamuwp)", eras=run3eras),
    Define(  "Jet_analysisPUId", "(Jet_pt > -1)", eras=run3eras), # we use puppi in run3, no need for pu id

    # Common
    DefineSkimmedCollection( "LepFO", "LepGood", mask="LepGood_isFO", members=( 'conePt',  'eta', 'phi','jetIdx', 'pdgId', 'convVeto', 'isTight','charge', 'lostHits', 'tightCharge', 'pt', 'mass', 'jetRelIso'), optMembers=['genPartFlav', 'genPartIdx', 'awayJet_pt', 'awayJet_jetId']),
    DefineP4("LepFO"),
    DefineMinMass("minMllFO", "LepFO"),
    Define(  "nLepTight", "Sum(LepFO_isTight > 0)")
    
]

tau_selection=[
    [Define(f"LepLooseForTauCleaning_{x}", f"LepGood_{x}[(LepGood_miniPFRelIso_all < 0.4) & (LepGood_sip3d < 8) & ((abs(LepGood_pdgId)!=11) | (LepGood_lostHits<=1)) & ((abs(LepGood_pdgId)!=13) or (LepGood_looseId != 0))]") for x in [ 'eta', 'phi']],
    Define("Tau_isClean", "cleanByDR(Tau_eta, Tau_phi, LepLooseForTauCleaning_eta, LepLooseForTauCleaning_phi,0.3)"),
    Define("Tau_isSelFO", "(Tau_pt > 20) & (abs(Tau_eta) < 2.3) & (abs(Tau_dxy) < 1000) & (abs(Tau_dz) < 0.2) & ((Tau_idDeepTau2017v2p1VSjet  & 2) != 0)"),  # VVLoose WP
    Define("Tau_isFO", "(Tau_isClean & Tau_isSelFO)"),
    Define("TauFO_jetIdx", "Tau_jetIdx[Tau_isFO]"),
    Define("nTauFO", "Sum( Tau_isFO != 0)"),
    Define("TauFO_eta", "Tau_eta[Tau_isFO]"),
]


jet_common=[
    DefineSkimmedCollection("SelJet", "Jet", mask="Jet_isSelected & (Jet_pt>25)", members=( 'eta', 'phi', 'pt', 'btagDeepFlavB', 'mass', 'rawFactor', 'area', 'jetId'), optMembers=['hadronFlavour', 'partonFlavour', 'genJetIdx']),
    Define("nJet25", "Sum( (SelJet_pt > 25) > 0)"),
    Define("nJet30", "Sum( (SelJet_pt > 30) > 0)"),
    Define("nBJetLoose25", "Sum( ( (SelJet_pt > 25) & (abs(SelJet_eta) < 2.4) & (SelJet_btagDeepFlavB > bTagCut_loose) ) > 0) "),
    Define("nBJetMedium25", "Sum( ( (SelJet_pt > 25) & (abs(SelJet_eta) < 2.4) & (SelJet_btagDeepFlavB > bTagCut_medium) ) > 0) "),
    DefineP4("SelJet"),
]

jet_selection=[Define("Jet_isSelected", "(Jet_pt > 20) &  (abs(Jet_eta) < 2.4) & (cleanByIndex(nJet,LepFO_jetIdx>=0, LepFO_jetIdx)) & (cleanByIndex(nJet,TauFO_jetIdx>=0,TauFO_jetIdx)) & (Jet_jetId > 0) & (Jet_analysisPUId > 0)")] + jet_common

jet_selection_notau=[Define("Jet_isSelected", "(Jet_pt > 20) &  (abs(Jet_eta) < 2.4) & (cleanByIndex(nJet,LepFO_jetIdx>=0, LepFO_jetIdx))  & (Jet_jetId > 0) & ((abs(Jet_eta) <= 2.7) | ((abs(Jet_eta) > 2.7) & (abs(Jet_eta) <= 3.0) & (Jet_neHEF < 0.99)) | ((abs(Jet_eta) > 3.0) & (Jet_neEmEF < 0.4)))  & (Jet_analysisPUId > 0)")] + jet_common
jet_selection_forward=[Define("Jet_isSelected", "(Jet_pt > 20) &  (abs(Jet_eta) < 5.0) & (cleanByIndex(nJet,LepFO_jetIdx>=0, LepFO_jetIdx))  & (Jet_jetId > 0) & ((abs(Jet_eta) <= 2.7) | ((abs(Jet_eta) > 2.7) & (abs(Jet_eta) <= 3.0) & (Jet_neHEF < 0.99)) | ((abs(Jet_eta) > 3.0) & (Jet_neEmEF < 0.4)))  & (Jet_analysisPUId > 0)",eras=run2eras),
                       Define("Jet_isSelected", "(Jet_pt > 20) & ( (abs(Jet_eta) < 2.5) )  &  (abs(Jet_eta) < 5.0) & (cleanByIndex(nJet,LepFO_jetIdx>=0, LepFO_jetIdx))  & (Jet_jetId > 0) & (Jet_analysisPUId > 0)",eras=run3eras)] \
                       + jet_common + [ Define("SelJet_isCentral", "(abs(SelJet_eta) < 2.4)")]

