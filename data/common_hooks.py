from CMGRDF.flow import Cut, AddWeight, Define, AddWeightUncertainty
from CMGRDF.modifiers import Append, Replace

flipSign=Replace( Cut("opposite-sign", "LepFO_charge[0]*LepFO_charge[1] < 0"), name='same-sign')

applRegion_2lss=Replace( Cut("application_region", "!(LepFO_isTight[0] && LepFO_isTight[1])"), name="TT")
applRegion_3l  =Replace( Cut("application_region", "!(LepFO_isTight[0] && LepFO_isTight[1] && LepFO_isTight[2])"), name="TTT")
negativeWeight=Append( AddWeight("negWeight", "-1", onData=False, onDataDriven=False, onMC=True))


lepton_gen_flags = [
    Define( "LepFO_isMatchRightCharge", "((LepFO_genPartFlav==1) | (LepFO_genPartFlav == 15)) & (isMatchCharge(LepFO_pdgId,LepFO_genPartIdx,GenPart_pdgId))" ),
    Define( "LepFO_mcMatchId", "(LepFO_genPartFlav==1) | (LepFO_genPartFlav == 15)"),
    Define( "LepFO_mcPromptGamma", "LepFO_genPartFlav==22"),
]

prompt_2lss_selection = Append( lepton_gen_flags + [Cut("gen_prompt_2lss", "(LepFO_isMatchRightCharge[0]!=0) && (LepFO_isMatchRightCharge[1]!=0)")])
prompt_3l_selection   = Append( lepton_gen_flags + [Cut("gen_prompt_3l"  , "(LepFO_mcMatchId[0]!=0) && (LepFO_mcMatchId[1]!=0)  && (LepFO_mcMatchId[2]!=0)")])
conv_2lss_selection = Append( lepton_gen_flags + [Cut("gen_conv_2lss", "(LepFO_mcPromptGamma[0]==1) | (LepFO_mcPromptGamma[1]==1)")])
conv_3l_selection   = Append( lepton_gen_flags + [Cut("gen_conv_3l"  , "(LepFO_mcPromptGamma[0]==1) | (LepFO_mcPromptGamma[1]==1) | (LepFO_mcPromptGamma[2]==1)")])

fsr_common=[AddWeightUncertainty("ps_fsr", "PSWeight[1]", "PSWeight[3]")] + [ AddWeightUncertainty(f"pdf_{i}", f"max(0.95f,min(1.05f,LHEPdfWeight[{i}]))", f"1./max(0.95f,min(1.05f,LHEPdfWeight[{i}]))") for i in range(100)] # some samples have outliers that break everything
