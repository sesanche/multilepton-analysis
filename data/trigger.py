from CMGRDF import *
from CMGRDF.flow import DefineDefault
from collections import OrderedDict

triggerGroups_dict=OrderedDict(
    Trigger_1e={
        "2016"    :  ['HLT_Ele27_WPTight_Gsf' , 'HLT_Ele25_eta2p1_WPTight_Gsf' , 'HLT_Ele27_eta2p1_WPLoose_Gsf'],
        "2016APV" :  ['HLT_Ele27_WPTight_Gsf' , 'HLT_Ele25_eta2p1_WPTight_Gsf' , 'HLT_Ele27_eta2p1_WPLoose_Gsf'],
        "2017"    :  ['HLT_Ele32_WPTight_Gsf' , 'HLT_Ele35_WPTight_Gsf'],
        "2018"    :  ['HLT_Ele32_WPTight_Gsf'],
        "2022"    :  ['HLT_Ele32_WPTight_Gsf'],
        "2022EE"  :  ['HLT_Ele32_WPTight_Gsf'], 
    },
    Trigger_1m={
        "2016"    :  ['HLT_IsoMu24' , 'HLT_IsoTkMu24' , 'HLT_IsoMu22_eta2p1' , 'HLT_IsoTkMu22_eta2p1' , 'HLT_IsoMu22' , 'HLT_IsoTkMu22'],
        "2016APV" :  ['HLT_IsoMu24' , 'HLT_IsoTkMu24' , 'HLT_IsoMu22_eta2p1' , 'HLT_IsoTkMu22_eta2p1' , 'HLT_IsoMu22' , 'HLT_IsoTkMu22'],
        "2017"    :  ['HLT_IsoMu24' , 'HLT_IsoMu27'],
        "2018"    :  ['HLT_IsoMu24'],
        "2022"    :  ['HLT_IsoMu24'],
        "2022EE"  :  ['HLT_IsoMu24'], 
    },
    Trigger_2e={
        "2016"    :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2016APV" :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2017"    :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'],
        "2018"    :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'],
        "2022"    :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'],
        "2022EE"  :  ['HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'],
    },
    Trigger_2m={
        "2016"    :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL' , 'HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL' ,  'HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ' , 'HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ'],
        "2016APV" :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL' , 'HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL' ,  'HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ' , 'HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ'],
        "2017"    :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8' , 'HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8'],
        "2018"    :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8'],
        "2022"    :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8'],
        "2022EE"  :  ['HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8'],
    },
    Trigger_em={
        "2016"    :   ['HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL' , 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ', 'HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL' , 'HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2016APV" :   ['HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL' , 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ', 'HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL' , 'HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2017"    :   ['HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL', 'HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ'        , 'HLT_Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'        , 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2018"    :   ['HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL', 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'        , 'HLT_Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2022"    :   ['HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL', 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'        , 'HLT_Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'],
        "2022EE"  :   ['HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL', 'HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'        , 'HLT_Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'],
        
    },
    Trigger_3e={
        "2016"    :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'],
        "2016APV" :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'],
        "2017"    :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'],
        "2018"    :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'], # prescaled in the two years according to https://twiki.cern.ch/twiki/bin/view/CMS/EgHLTRunIISummary#2018
        "2022"    :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'],
        "2022EE"  :  ['HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL'], 
    },
    Trigger_3m={
        "2016"    :  ['HLT_TripleMu_12_10_5'],
        "2016APV" :  ['HLT_TripleMu_12_10_5'],
        "2017"    :  ['HLT_TripleMu_12_10_5'],
        "2018"    :  ['HLT_TripleMu_12_10_5'],
        "2022"    :  ['HLT_TripleMu_12_10_5'],
        "2022EE"  :  ['HLT_TripleMu_12_10_5'],
    },
    Trigger_mee={
        "2016"    :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
        "2016APV" :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
        "2017"    :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
        "2018"    :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
        "2022"    :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
        "2022EE"  :  ['HLT_Mu8_DiEle12_CaloIdL_TrackIdL'],
    },
    Trigger_mme={
        "2016"    :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL'   ],
        "2016APV" :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL'   ],
        "2017"    :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ'],
        "2018"    :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ'],
        "2022"    :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ'],
        "2022EE"  :  ['HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ'],
    },
    Trigger_MET={ 
        "2016"    : ["HLT_PFMET120_PFMHT120_IDTight"],
        "2016APV" : ["HLT_PFMET120_PFMHT120_IDTight"],
        "2017"    : ["HLT_PFMET120_PFMHT120_IDTight"],
        "2018"    : ["HLT_PFMET120_PFMHT120_IDTight"],
    },
    Trigger_2lss={
        "2016"      : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
        "2016APV"   : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
        "2017"      : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
        "2018"      : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
        "2022"      : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
        "2022EE"    : ["Trigger_1e" , "Trigger_1m" , "Trigger_2e" , "Trigger_2m" , "Trigger_em"],
    },
    Trigger_3l={
        "2016"      : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
        "2016APV"   : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
        "2017"      : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
        "2018"      : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
        "2022"      : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
        "2022EE"    : ["Trigger_2lss" , "Trigger_3e" , "Trigger_3m" , "Trigger_mee" , "Trigger_mme"],
    },

    
)

trigger_definition_sequence = []

for key, trigger_dict in triggerGroups_dict.items():
    for era, path_list in trigger_dict.items():
        for path in path_list:
            trigger_definition_sequence.append(DefineDefault( path, False)) # dont do anything if they already exist

        trigger_definition_sequence.append( Define( key, " || ".join( path_list ), eras=[era]))
