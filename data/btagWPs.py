
_btagWPs = {

    "DeepFlav_UL2016APV_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0508),
    "DeepFlav_UL2016APV_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.2598),
    "DeepFlav_UL2016APV_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.6502),

    "DeepFlav_UL2016_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0480),
    "DeepFlav_UL2016_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.2489),
    "DeepFlav_UL2016_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.6377),

    "DeepFlav_UL2017_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0532),
    "DeepFlav_UL2017_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.3040),
    "DeepFlav_UL2017_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.7476),

    "DeepFlav_UL2018_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0490),
    "DeepFlav_UL2018_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.2783),
    "DeepFlav_UL2018_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.7100),
    
    "DeepFlav_UL2022_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0583),
    "DeepFlav_UL2022_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.3086),
    "DeepFlav_UL2022_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.7183),

    "DeepFlav_UL2022EE_L": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.0614),
    "DeepFlav_UL2022EE_M": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.3196),
    "DeepFlav_UL2022EE_T": ("pfDeepFlavourJetTags:probb + pfDeepFlavourJetTags:probbb + pfDeepFlavourJetTags:problepb", 0.73),

}
