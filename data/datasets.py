from collections import defaultdict
import os
import ROOT 
from CMGRDF import AddWeight, Data, DataSample, DataDrivenSample, Process, NormUncertainty, AddWeightUncertainty, Append, MCGroup, MCSample, Source
from utils.dataDriven import FakeRateDefine
from CMGRDF.cms.eras import lumiUncertainties, run2eras, run3eras

from data.common_hooks import *


P = "root://eoscms.cern.ch//eos/cms/store/cmst3/group/tthlep/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root"
if "TTCPV_PATH" in os.environ:
    P = os.environ["TTCPV_PATH"]+"/{era}/{name}.root"
    print(f"Will use {P}")

eras = run2eras + run3eras

fakeRate_3l = {}
for era in eras:
    frdef = FakeRateDefine(era=era, nlep=3, doSyst=True)
    frdef.init()
    fakeRate_3l[era] = Append(frdef)

fakeRateWeight_3l = Append(AddWeight("frWeight", "fake_rate_weight_3l", onData=False, onDataDriven=True, onMC=True))

data_3l = []

all_samples = {}
for era in eras:
    all_samples[era] = defaultdict(list)
    if era in run2eras:
        groups = dict(
            Rares="VHToNonbb,TTTT,TTWH,WWW,WWZ,WZG,WZZ,ZZZ,tWll_tlep_wlep,tWll_tlep_whad,tWll_thad_wlep,TTHnobb_fxfx,TTWToLNu_fxfx,TTWJetsToLNu_EWK_5f_NLO".split(","),
            TTZ=[ "TTZToLLNuNu_amc", "TTZToLLNuNu_m1to10", "TTSemi_pow", "TTLep_pow"],
            tZq=["TZQToLL"],
            Diboson=["WZTo3LNu_fxfx", "ZZTo4L"],
            Conversions=["TTGJets", "TGJets_lep", "ZGTo2LG", 'DYJetsToLL_M50'],
        )
    else:
        groups = dict(
            Rares = "THQ_ctcvcp_sm,THW_ctcvcp_sm,TTHtoNon2B,TTLNu_1Jets,WZZ,ZZZ,TWZ_Tto2Q_WtoLNu_Zto2L_DR1,TWZ_TtoLNu_Wto2Q_Zto2L_DR1,TWZ_TtoLNu_WtoLNu_Zto2L_DR1".split(","),
            TTZ = [ "TTLL_MLL_50","TTLL_MLL_4to50", "TTto2L2Nu" ],
            tZq = ["TZQB"],
            Diboson = ["WZto3LNu","ZZto4L"],
            Conversions = ["TTG_1Jets_PTG_100to200", "TTG_1Jets_PTG_10to100", "TTG_1Jets_PTG_200","WGtoLNuG_PTG_100to200", "WGtoLNuG_PTG_10to100", "WZGtoLNuZG", "DYto2L_2Jets_MLL_50"],
        )
    for group, names in groups.items():
        all_samples[era][group] += [MCSample(name, P, xsec="xsec", eras=[era]) for name in names]
prompt_process_groups = ["Rares", "TTZ", "tZq", "Diboson"]

def makeMCProc(name, hooks, **kwargs):
    samples = [s.clone(hooks=hooks) for era in eras for s in all_samples[era][name]]
    samplesPerEra = max(len(all_samples[era][name]) for era in eras)
    return Process(name, MCGroup(name, samples) if samplesPerEra > 1 else samples, **kwargs) 

data_3l = [
    makeMCProc( "Rares",       [prompt_3l_selection], fillColor=ROOT.kAzure-9, normUncertainty=[NormUncertainty("CMS_TOP24012_raresnorm", 1.5)]+lumiUncertainties),
    makeMCProc( "tZq",         [prompt_3l_selection]+[Append(x) for x in fsr_common +  [ AddWeightUncertainty("tZq_ps_isr", "PSWeight[0]", "PSWeight[2]")]], fillColor=ROOT.kRed, normUncertainty=[NormUncertainty("tZq_norm", 1.1)]+lumiUncertainties),
    makeMCProc( "TTZ",         [Append(AddWeight('kfactor', str(1.12))), prompt_3l_selection]+[Append(x) for x in fsr_common + [ AddWeightUncertainty("ttZ_ps_isr", "PSWeight[0]", "PSWeight[2]") ]], fillColor=ROOT.kSpring+2, normUncertainty=[NormUncertainty("ttZ_norm", 1.1)]+ lumiUncertainties),
    makeMCProc( "Diboson",     [prompt_3l_selection, Append(AddWeightUncertainty("diboson_njets", "1+0.1*min(nJet25,5)"))], fillColor=ROOT.kViolet-4,normUncertainty=[NormUncertainty("CMS_TOP24012_dibosonnorm", 1.2)]+ lumiUncertainties),
    makeMCProc( "Conversions", [conv_3l_selection], fillColor=ROOT.kOrange, normUncertaintyertainty=[NormUncertainty("CMS_TOP24012_convnorm", 1.3)]+ lumiUncertainties),
]



data_samples = {
    '2018':    [([f"{pd}_Run2018{era}_UL18_GT36"   for pd in ["DoubleMuon", "SingleMuon", "MuonEG", "EGamma"]], era) for era in ['A', 'B', 'C', 'D']],
    '2017':    [([f"{pd}_Run2017{era}_UL2017"      for pd in ["DoubleEG", "DoubleMuon", "MuonEG", "SingleElectron", "SingleMuon"]], era)  for era in ['B', 'C', 'D', 'E', 'F']],
    '2016':    [([f"{pd}_Run2016{era}_UL16"        for pd in ["DoubleEG", "DoubleMuon", "MuonEG", "SingleElectron", "SingleMuon"]], era)  for era in ['F', 'G', 'H']],
    '2016APV': [([f"{pd}_Run2016{era}_HIPM_UL16" for pd in ["DoubleEG", "DoubleMuon", "MuonEG", "SingleElectron", "SingleMuon"]], era[0])  for era in ['Bv2', 'C', 'D', 'E', 'F']],
    '2022':    [([f"{pd}_Run2022C"         for pd in ["EGamma", "MuonEG", "Muon", "SingleMuon", "DoubleMuon"]], 'C'),
                ([f"{pd}_Run2022D"         for pd in ["EGamma", "MuonEG", "Muon"]], 'D')],
    '2022EE':  [([f"{pd}_Run2022{subera}"  for pd in ["EGamma", "MuonEG", "Muon"]], subera) for subera in ['E', 'F', 'G']], 
}

for_data=[]
for_fakes_3l=[]

for era in eras:
    for name in prompt_process_groups: 
        samples = [s.clone(hooks=[applRegion_3l,fakeRate_3l[era],fakeRateWeight_3l,negativeWeight,prompt_3l_selection], postfix="_AR") for s in all_samples[era][name]]
        for_fakes_3l.append(MCGroup(name, samples) if len(samples) > 1 else samples[0])

    for pds,subera in data_samples[era]:
        sources = {era: Source(f"Data_{era}{subera}", [P.format(era=era, name=pd) for pd in pds], era=era)}
        for_data.append(  DataSample(f"Data_{era}{subera}", sources, eras=[era], subera=subera ))
        for_fakes_3l.append( DataDrivenSample(f"Data_{era}{subera}_AR", sources, eras=[era], weight="1", subera=subera, hooks=[applRegion_3l  ,fakeRate_3l[era]  ,fakeRateWeight_3l, ]) )

    
data_3l.append( Data( for_data ) )

data_3l.append( Process( "Fakes",
                           for_fakes_3l, fillColor=ROOT.kBlack,fillStyle=3005))
