# remove 2018 prefiring

import CMGRDF

import pathlib
datadir = str(pathlib.Path(__file__).resolve().parent / "data")

from CMGRDF import *
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4

from CMGRDF.modifiers import Prepend 
from data.trigger import trigger_definition_sequence
from utils.dataDriven import FlipRateDefine, FakeRateDefine
from utils.leptonScaleFactors  import leptonSFsequences
from utils.btagSF              import btagsf_sequences
from utils.jmesequences        import jme_sequences
from data.datasets_mconly import data_3l as all_data
import ROOT
ROOT.EnableImplicitMT(16)
import numpy as np

from CMGRDF.stat import DatacardWriter

from data.common_selections import lepton_selection, tau_selection, jet_selection_notau, jet_selection_forward
eras  =  ["2018", "2016APV", "2016", "2017", "2022", "2022EE"] 

lumi = {
    '2016APV': 19.5,
    '2016': 16.8,
    '2017': 41.5,
    '2018': 59.7,
    '2022': 8.0,
    "2022EE" : 26.7
}


P="root://eoscms.cern.ch//eos/cms/store/cmst3/group/tthlep/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root" # careful, signal samples are not prescaled here 



runSnapshot=False
runPlots=True
testSignal=False
runCards=False

ROOT.gInterpreter.Declare(open( "functions.cc").read())
ROOT.gInterpreter.Declare(open( "cpv/training_class_v2.cc").read())


networks={
    "sep18_peryear_ctZI_full_nb_2000_lr_1e-4_fromRun2_32" : "ctZI",
    "sep18_peryear_ctWI_all_nb_2000_lr_5e-5v0_fromRun2_37": "ctWI", 
}

for network in networks:
    ROOT.gInterpreter.Declare(f'OnnxDNNEvaluator {network.replace("-","m")}_eval("{datadir}/networks/{network}.onnx", true);')
                         
inputVars=[x for x in dir(ROOT.cpvTraining) if x.startswith("_input")]


cuts=[         AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter && Flag_ecalBadCalibFilter"),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_2lss"),
               lepton_selection,
               Cut("trilep", "nLepFO>=3"),
               Cut("cleanup", "minMllFO > 20"),
               Cut("pt251515", "(LepFO_conePt[0] > 25) && (LepFO_conePt[1] > 15) && (LepFO_conePt[2] > 15)"),
               Cut("TTT", "(LepFO_isTight[0]) && (LepFO_isTight[1]) && (LepFO_isTight[2])"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Define( "nMuons", "(abs(LepFO_pdgId[0]) == 13) +(abs(LepFO_pdgId[1]) == 13) +(abs(LepFO_pdgId[2]) == 13) "),
               #jme_sequences,
               jet_selection_forward,
               Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15"),
               Cut( "1bl", "nBJetLoose25>0"), 
               leptonSFsequences,
               btagsf_sequences,
               AddWeight("mcWeight", "puWeight*leptonSF_weight_2l*leptonSFreco_weight_2l*btagSF_fixedWP", onData=False, onDataDriven=False), #
               Cut( "1j", "nJet25 > 1"), 
               [Define("eraFlag", "%d"%i, eras=[era]) for i,era in enumerate("2016,2016APV,2017,2018,2022,2022EE".split(","))],
               Define( "trainingInputs", "prepareTraining( LepFO_p4, LepFO_pdgId, SelJet_p4, (SelJet_btagDeepFlavB > bTagCut_medium) & (SelJet_isCentral), (SelJet_btagDeepFlavB > bTagCut_loose) & (SelJet_isCentral), Z_candidate_pair, MET_pt, MET_phi)"),
               [Define(x, f"trainingInputs.{x}") for x in inputVars],
               Define( "cpvWeights", "get_cpv_weights( LHEReweightingWeight )", onData=False, onDataDriven=False),
               [Define(x,  f"cpvWeights.{x}", onData=False, onDataDriven=False) for x in ["_input_weight_sm", "_input_weight_lin_ctZI", "_input_weight_lin_ctWI", "_input_weight_lin_ctGI"]],
               [Define(network.replace('-','m'), f"{network.replace('-','m')}_eval.run({{_input_lw_px,_input_lw_py,_input_lw_pz,_input_lw_q,_input_met_px,_input_met_py,_input_zplus_px,_input_zplus_py,_input_zplus_pz,_input_zminus_px,_input_zminus_py,_input_zminus_pz,_input_j1_px,_input_j1_py,_input_j1_pz,_input_j1_btag,_input_j2_px,_input_j2_py,_input_j2_pz,_input_j2_btag,_input_j3_px,_input_j3_py,_input_j3_pz,_input_j3_btag,_input_j4_px,_input_j4_py,_input_j4_pz,_input_j4_btag,_input_j5_px,_input_j5_py,_input_j5_pz,_input_j5_btag,float(eraFlag)}})") for network in networks],

]


cuts_3l = Flow("SR_3l", cuts)
flows=[  cuts_3l ]
for mvaelwp in  [0.0, -0.5]:
    for mvamuwp in  [mvaelwp]: # 0.85, 0.0, 0.3, -0.5]:
        continue
        newflow=cuts_3l.clone(f"SR_3l_{mvaelwp}_{mvamuwp}")
        newflow.remove( "mvaelwp" ) 
        newflow.remove( "mvamuwp" )
        newflow.prepend( Define( "mvaelwp", f"{mvaelwp}"))
        newflow.prepend( Define( "mvamuwp", f"{mvamuwp}"))
        flows.append( newflow ) 


maker = Processor()


skimpath="/data/sesanche/cpv_training_tuples_v8_fullrun2n3/{suffix}/{name}_{era}.root"
hook_even= Append( [Cut("even",  "event % 2 == 0"), AddWeight("sm_weight", "_input_weight_sm"), AddWeight("resampling_weight", "2.0")]) # multiply by 2 to get the right normalization
hook_odd = Append( [Cut("even",  "event % 2 != 0"), AddWeight("sm_weight", "_input_weight_sm"), AddWeight("resampling_weight", "2.0")]) # multiply by 2 to get the right normalization
hook_wztest     = Prepend( [Cut("even",  "event % 5 == 0" ), AddWeight("sm_weight", "1."),AddWeight("resampling_weight", "5.0*0.1")]) # multiply by 5 to get the right normalization, and by 0.1 because we select loose btags
hook_wztrain = Prepend( [Cut("even",  "event % 5 != 0" ), AddWeight("sm_weight", "1."),AddWeight("resampling_weight", "0.1*5/4")]) # multiply by 5/4 to get the right normalization  and by 0.1 because we select loose btags

def get_binning( filename, name, nq):
    print("getting binning for", filename, name)
    tf=ROOT.TFile.Open(filename)
    tzq_int=tf.Get(name)
    for i in range(tzq_int.FindBin(0.)):
        tzq_int.SetBinContent(i,0)
        #nq=20 # for ctZI, 5 for ctWI
    prob=np.linspace(0,1,nq); q=np.linspace(0,1,nq)
    y=tzq_int.GetQuantiles(len(prob),q,prob)
    bins= [x for x in q][1:]
    bins= [-x for x in reversed(bins)] + [0.] + [x for x in bins]
    return bins


if runSnapshot:

    P="root://eoscms.cern.ch//eos/cms/store/cmst3/group/tthlep/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root" # careful, signal samples are not prescaled here 
    
    all_data=[
        Process( "TTLL_CPV_train",     [ MCSample( "TTLL_CPV", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_even], suffix="train", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")]),
        Process( "TZQ_CPV_train",      [ MCSample( "TZQ_CPV", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_even], suffix="train", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")]),
        Process( "WZTo3LNu_pow_train", [
            MCSample( "WZTo3LNu_pow_M01", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_wztrain], suffix="train"),
            MCSample( "WZto3LNu", P, xsec="xsec", eras=["2022","2022EE"],hooks=[hook_wztrain], suffix="train"),
        ]),
        Process( "TTLL_CPV_test",     [ MCSample( "TTLL_CPV", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018", "2022", "2022EE"],hooks=[hook_odd], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")]),
        Process( "TZQ_CPV_test",      [ MCSample( "TZQ_CPV", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018", "2022", "2022EE"],hooks=[hook_odd], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")]),
        Process( "WZTo3LNu_pow_test", [
            MCSample( "WZTo3LNu_pow_M01", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_wztest], suffix="test"), # no resampling in the test dataset
            MCSample( "WZto3LNu", P, xsec="xsec", eras=["2022", "2022EE"],hooks=[hook_wztest], suffix="test"), # no resampling in the test dataset 
        ]),

    ]
    
    maker.book(all_data, lumi, cuts_3l,  Snapshot(skimpath, columnSel=inputVars+['ttZ_score', 'eraFlag', 'sm_weight', 'mcWeight', 'mcSampleWeight', 'resampling_weight']+["_input_weight_sm", "_input_weight_lin_ctZI", "_input_weight_lin_ctWI", "_input_weight_lin_ctGI"]+ [network.replace('-','m') for network in networks], compression=None), eras=eras )
    report = maker.runSnapshots()

    
    print(f"Snapshoted")
    for key, snap in report:
        print("%-10s  %-20s : %10u entries  %9.3f GB   %s" % (key.process, key.sample, snap.entries, snap.size / (1024.**3), snap.fname))


elif runPlots:
    print(P)
    maker = Processor()

    all_data.extend( [
        Process( "TTLL_CPV_ctWI",     [ MCSample( "TTLL_CPV", P, xsec="2*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_odd, Append(AddWeight("linear", "10*_input_weight_lin_ctWI/_input_weight_sm"))], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")], signal=True, fillColor=ROOT.kBlue), # multiply xsec by 2, because hook_odd multiplies it before by 2 ( and we have only event%4==1)
        Process( "TZQ_CPV_ctWI",      [ MCSample( "TZQ_CPV", P, xsec="2*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_odd, Append(AddWeight("linear", "10*_input_weight_lin_ctWI/_input_weight_sm"))], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")], signal=True, fillColor=ROOT.kRed), # multiply xsec by 2, because hook_odd multiplies it before by 2 ( and we have only event%4==1)
        Process( "TTLL_CPV_ctZI",     [ MCSample( "TTLL_CPV", P, xsec="2*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_odd, Append(AddWeight("linear", "10*_input_weight_lin_ctZI/_input_weight_sm"))], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")], signal=True, fillColor=ROOT.kBlue), # multiply xsec by 2, because hook_odd multiplies it before by 2 ( and we have only event%4==1)
        Process( "TZQ_CPV_ctZI",      [ MCSample( "TZQ_CPV", P, xsec="2*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[hook_odd, Append(AddWeight("linear", "10*_input_weight_lin_ctZI/_input_weight_sm"))], suffix="test", genSumWeightName="LHEReweightingSumw[81]*genEventSumw")], signal=True, fillColor=ROOT.kRed), # multiply xsec by 2, because hook_odd multiplies it before by 2 ( and we have only event%4==1)
    ])

    plots_3l=[ Plot(network.replace('-','m'), network.replace('-','m'), (10000,-0.5,0.5), logy=True, yMin=0.1) for network in networks ]
    for flow in flows:
        maker.book(all_data  , lumi, flow, plots_3l  , eras=eras, withUncertainties=False)
        
    results=maker.runPlots(mergeEras=True)
    PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True, noStackSignals=True).printSet(results, "plots_optimization_sep14/all/{flow}/",  maxRatioRange=(0.5,1.5), showRatio=True)
    maker.clear()


    for flow in flows:
        for network in networks:
            if networks[network] == "ctWI":
                signal_process="TZQ_CPV_ctWI"
            elif networks[network] == "ctZI":
                signal_process="TTLL_CPV_ctZI"
                
            bins=get_binning( f"plots_optimization_sep14/all/{flow.name}/{network.replace('-','m')}.root", signal_process, 6)
            print(signal_process, bins)



elif testSignal:
    
    plots_3l = [
        Plot("totWeight", "0", (1,-0.5,0.5)),
        # Plot("nBJetLoose25", "nBJetLoose25", (5,-0.5,4.5)),
        # Plot("nBJetMedium25", "nBJetMedium25", (5,-0.5,4.5)),
        # Plot("nJet25", "nJet25", (5,-0.5,4.5)),
        # Plot("PuppiMET_pt", "PuppiMET_pt", (50,0.,200.)),
    ]

    maker = Processor()
    sm_weight=Append([AddWeight("sm_weight", "_input_weight_sm") ])
    all_data=[
        Process( "TTLL_CPV",     [ MCSample( "TTLL_CPV", P, xsec="xsec",genSumWeightName="LHEReweightingSumw[81]*genEventSumw",  eras=["2016", "2016APV", "2017", "2018"],hooks=[sm_weight])]),
        Process( "TZQ_CPV",      [ MCSample( "TZQ_CPV",  P, xsec="xsec",genSumWeightName="LHEReweightingSumw[81]*genEventSumw",  eras=["2016", "2016APV", "2017", "2018"],hooks=[sm_weight])]),
        Process( "ttZ_central" ,      [ MCSample( "TTZToLLNuNu_amc", P, xsec="0.281", eras=["2016", "2016APV", "2017", "2018"],hooks=[])]),
        Process( "tZq_central" ,      [ MCSample( "TZQToLL", P, xsec="xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[])]),
    ]

    
    maker.book(all_data  , lumi, cuts_3l  , plots_3l  , eras=['2016'], withUncertainties=False)
    results=maker.runPlots()
    PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True).printSet(results, "plots_snapshot/{era}",  maxRatioRange=(0.5,1.5), showRatio=True) 

elif runCards:
    maker = Processor()

    for signal in ["TTLL_CPV", "TZQ_CPV"]:
        for point, index in [('sm', 81), ("ctZI_1p0", 11), ("ctZI_m1p0", 12), ("ctWI_1p0", 7), ("ctWI_m1p0", 8)]:
            all_data.append( Process( f"{signal}_{point}",     [ MCSample(signal, P, xsec="4*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[Append(AddWeight("point", f"LHEReweightingWeight[{index}]"))], genSumWeightName="LHEReweightingSumw[81]*genEventSumw")], signal=True, fillColor=ROOT.kBlue))


    for flow in flows:
        plots_3l=[]
        
        for network in networks:
            if networks[network] == "ctWI":
                signal_process="TZQ_CPV_ctWI"
            elif networks[network] == "ctZI":
                signal_process="TTLL_CPV_ctZI"
                
            network_name=network.replace('-','m')
            for nq in [5,10,20]:
                bins=get_binning( f"plots_optimization_jul30/all/{flow.name}/{network.replace('-','m')}.root", signal_process, nq)
                plots_3l.append( Plot(network_name+"_operatorbinned_nq_%d"%nq, f"TMath::Min(TMath::Max(float({network_name}[0]),float(-0.49)),float(0.49))" , bins, logy=True, yMin=0.1, legend="off") )
        maker.book(all_data  , lumi, flow  , plots_3l  , eras=eras, withUncertainties=False)

    results=maker.runPlots(mergeEras=True)
    PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True, noStackSignals=True).printSet(results, "plots_optimization_jul31/all_fordatacards/{flow}/",  maxRatioRange=(0.5,1.5), showRatio=True)

    cardMaker = DatacardWriter(regularize=True, autoMCStats=False)
    for network in networks:
        network_name=network.replace('-','m')
        for nq in [5,10,20]:
            cardMaker.makeCards(results, MultiKey(name=network_name+"_operatorbinned_nq_%d"%nq), f"plots_optimization_jul31/all_fordatacards_operatorbinned/{network}/flow_{nq}_{{flow}}", autoMCStats=True)



    
    
