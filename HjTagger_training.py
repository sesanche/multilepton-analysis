import argparse
from CMGRDF import *
import CMGRDF
ROOT.EnableImplicitMT()



from data.trigger import trigger_definition_sequence
from utils.leptonScaleFactors  import leptonSFsequences
from utils.btagSF              import btagsf_sequences
from utils.jmesequences        import jme_sequences, jme_sequences_after
from data.datasets import data_3l as all_data, run2eras, run3eras, eras, P
from tth.cuts_and_plots_configs.cuts_2lss import cuts_ttH_SR_2lss
from CMGRDF.collectionUtils import DefineSkimmedCollection

P0 = "root://eoscms.cern.ch//eos/cms/store/group/phys_higgs/moameen/tthmlep/NanoTrees_forCMGRDF_100524"
P = P0 + "/{era}/{name}.root"
print(f"Reading from {P}")


all_data = [
    Process("TTH_train", [MCSample("TTHtoNon2B", P , xsec="xsec", eras=run3eras, hooks=[], suffix="train")]),
]
lumi = {
    '2016APV': 19.5,
    '2016': 16.8,
    '2017': 41.5,
    '2018': 59.7,
    '2022': 8.0,
    "2022EE" : 26.7,
}

ROOT.gInterpreter.Declare('''
ROOT::RVec<bool>  isJetFromHiggs( const ROOT::RVec<bool> & GenJet_isMatchedTo_q_fromHiggs, const ROOT::RVec<int> & Jet_genJetIdx){
    ROOT::RVec<bool> ret(Jet_genJetIdx.size());
    for (int ijet=0; ijet < Jet_genJetIdx.size(); ++ijet){
       if (Jet_genJetIdx.at(ijet) >= 0)
           ret.at(ijet) = GenJet_isMatchedTo_q_fromHiggs.at( Jet_genJetIdx.at(ijet));
       else
           ret.at(ijet) = false;
    }
    return ret;
}
''')



flow=Flow( "selection", #cuts_ttH_SR_2lss,
           Define("GenPart_motherPdgId", "Take(GenPart_pdgId, GenPart_genPartIdxMother)"),
           Define("GenPart_grandMotherPdgId", "Take(GenPart_motherPdgId, GenPart_genPartIdxMother)"),
           
           DefineSkimmedCollection( "GenPart_q_fromW_fromHiggs", "GenPart", mask="((abs(GenPart_pdgId) == 1) | (abs(GenPart_pdgId) == 2) | (abs(GenPart_pdgId) == 3) | (abs(GenPart_pdgId) == 4)) & (GenPart_grandMotherPdgId == 25)", members=( 'eta', 'phi', 'genPartIdxMother', 'pt')),
           Define( "GenJet_isMatchedTo_q_fromHiggs", "!(cleanByDR(GenJet_eta, GenJet_phi, GenPart_q_fromW_fromHiggs_eta, GenPart_q_fromW_fromHiggs_phi,0.4))"),
           Define("Jet_isFromHiggs", "isJetFromHiggs(GenJet_isMatchedTo_q_fromHiggs,Jet_genJetIdx)")
           

)

if __name__ == "__main__":
     from CMGRDF.cmdline import processorFromCommandLineArgs
     import argparse
     parser = argparse.ArgumentParser()

     maker, args = processorFromCommandLineArgs(parser)

     plots=[
         Plot("nGenPart_q_fromW_fromHiggs", "nGenPart_q_fromW_fromHiggs", (5,-0.5,4.5)),
         Plot("Jet_isFromHiggs", "Jet_isFromHiggs", (2,-0.5,1.5)),
     ]
     maker.book( all_data, lumi, flow, plots, eras=run3eras)

     results=maker.runPlots()

     PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True, noStackSignals=True).printSet(results, "test/{flow}",  fixRatioRange=True, maxRatioRange=(0.0,2.0), showRatio=True)
