import argparse
import CMGRDF

import pathlib
datadir = str(pathlib.Path(__file__).resolve().parent / "data")

from data.trigger import trigger_definition_sequence
from utils.leptonScaleFactors  import leptonSFsequences
from utils.btagSF              import btagsf_sequences
from utils.jmesequences        import jme_sequences, jme_sequences_after
from data.datasets import data_3l as all_data, run2eras, run3eras, eras, P

from CMGRDF.stat import DatacardWriter

from data.common_selections import lepton_selection, jet_selection_forward

parser = argparse.ArgumentParser()
parser.add_argument("outname", type=str,
                    help="name of the output directory")
parser.add_argument("--skipCards", action="store_true",
                    help="do not run in SR with datacards")
parser.add_argument("--skipOffshell", action="store_true",
                    help="do not run in control regions")
parser.add_argument("--skipWZ", action="store_true",
                    help="do not evaluate the NNs in the WZ control region")
parser.add_argument("--dontMergeEras", action="store_true",
                    help="do not merge eras")
parser.add_argument("--perFlavor", action="store_true",
                    help="split cards in lepton flavor")
parser.add_argument("--plotInputVars", action="store_true",
                    help="plot input vars in signal regions") 

args = parser.parse_args()

eras  =  ["2018", "2016APV", "2016", "2017", "2022", "2022EE"] 

lumi = {
    '2016APV': 19.5,
    '2016': 16.8,
    '2017': 41.5,
    '2018': 59.7,
    '2022': 8.0,
    "2022EE" : 26.7,
}


P="root://eoscms.cern.ch//eos/cms/store/cmst3/group/tthlep/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root"



runSnapshot=False
runPlots=False
testSignal=False
runCards=True


Declare("".join(open( "functions.cc")))
Declare("".join(open("cpv/training_class_v2.cc")))


networks=[
    { "name" : "sep18_peryear_ctWI_all_nb_2000_lr_5e-5v0_fromRun2_37", "variable" : "ctWI_score", "binning" : [-1.0, -0.05731020599023443, -0.029543903845664653, -0.015674668782080695, -0.0066436611051414, 0.0, 0.0066436611051414, 0.015674668782080695, 0.029543903845664653, 0.05731020599023443, 1.0]},
    { "name" : "sep18_peryear_ctZI_full_nb_2000_lr_1e-4_fromRun2_32", "variable" : "ctZI_score", "binning"  : [-1.0, -0.02601987689311082, -0.017020959757288134, -0.011533643141187653, -0.00709526495176328, 0.0, 0.00709526495176328, 0.011533643141187653, 0.017020959757288134, 0.02601987689311082, 1.0]},
]

for network in networks:
    Declare(f'OnnxDNNEvaluator {network["variable"]}_eval("{datadir}/networks/{network["name"]}.onnx", true);')


Declare('''int scoreBinning( ROOT::VecOps::RVec<float>& score, std::vector<double> binning){
   for (int i=0; i<binning.size(); ++i){
       if (score.at(0) < binning.at(i)) return i;
   }
   return binning.size();
}''')
                          

inputVars=[x for x in dir(ROOT.cpvTraining) if x.startswith("_input")]
inputVarsBinning = {
    '_input_j1_btag':(3,-0.5,2.5),
    '_input_j1_px':(10,-200,200),
    '_input_j1_py':(10,-200,200),
    '_input_j1_pz':(10,-400,400),
    '_input_j2_btag':(3,-0.5,2.5),
    '_input_j2_px':(10,-200,200),
    '_input_j2_py':(10,-200,200),
    '_input_j2_pz':(10,-400,400),
    '_input_j3_btag':(4,-0.5,2.5),
    '_input_j3_px':(10,-200,200),
    '_input_j3_py':(10,-200,200),
    '_input_j3_pz':(10,-400,400),
    '_input_j4_btag':(3,-0.5,2.5),
    '_input_j4_px':(10,-200,200),
    '_input_j4_py':(10,-200,200),
    '_input_j4_pz':(10,-400,400),
    '_input_j5_btag':(3,-0.5,2.5),
    '_input_j5_px':(10,-200,200),
    '_input_j5_py':(10,-200,200),
    '_input_j5_pz':(10,-400,400),
    '_input_lw_px':(10,-200,200),
    '_input_lw_py':(10,-200,200),
    '_input_lw_pz':(10,-200,200),
    '_input_lw_q': (2,-1.5,1.5),
    '_input_met_px':(10,-200,200),
    '_input_met_py':(10,-200,200),
    '_input_zminus_px':(10,-200,200),
    '_input_zminus_py':(10,-200,200),
    '_input_zminus_pz':(10,-200,200),
    '_input_zplus_px': (10,-200,200),
    '_input_zplus_py': (10,-200,200),
    '_input_zplus_pz': (10,-200,200),
}

baseline=[         AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter && Flag_ecalBadCalibFilter"),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_2lss"),
               lepton_selection,
               Cut("trilep", "nLepFO>=3"),
               Cut("cleanup", "minMllFO > 20"),
               Cut("pt251515", "(LepFO_conePt[0] > 25) && (LepFO_conePt[1] > 15) && (LepFO_conePt[2] > 15)"),
               Cut("TTT", "(LepFO_isTight[0]) && (LepFO_isTight[1]) && (LepFO_isTight[2])"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Define( "nMuons", "(abs(LepFO_pdgId[0]) == 13) +(abs(LepFO_pdgId[1]) == 13) +(abs(LepFO_pdgId[2]) == 13) "),
               jme_sequences,
               jet_selection_forward,
               jme_sequences_after,
               leptonSFsequences,
               btagsf_sequences,
               AddWeight("mcWeight", "puWeight*leptonSF_weight_3l*leptonSFreco_weight_3l*btagSF_fixedWP*weight_jetPUId", onData=False, onDataDriven=False), #
               [Define("eraFlag", "%d"%i, eras=[era]) for i,era in enumerate("2016,2016APV,2017,2018,2022,2022EE".split(","))],
               Define( "trainingInputs", "prepareTraining( LepFO_p4, LepFO_pdgId, SelJet_p4, (SelJet_btagDeepFlavB > bTagCut_medium) & (SelJet_isCentral), (SelJet_btagDeepFlavB > bTagCut_loose) & (SelJet_isCentral), Z_candidate_pair, MET_pt, MET_phi)"),
               [Define(x,  f"trainingInputs.{x}") for x in inputVars],
               [Define(network['variable'], f"{network['variable']}_eval.run({{_input_lw_px,_input_lw_py,_input_lw_pz,_input_lw_q,_input_met_px,_input_met_py,_input_zplus_px,_input_zplus_py,_input_zplus_pz,_input_zminus_px,_input_zminus_py,_input_zminus_pz,_input_j1_px,_input_j1_py,_input_j1_pz,_input_j1_btag,_input_j2_px,_input_j2_py,_input_j2_pz,_input_j2_btag,_input_j3_px,_input_j3_py,_input_j3_pz,_input_j3_btag,_input_j4_px,_input_j4_py,_input_j4_pz,_input_j4_btag,_input_j5_px,_input_j5_py,_input_j5_pz,_input_j5_btag,float(eraFlag)}})") for network in networks]


]


pre_sr = baseline + [
    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15"),
    Cut( "1j", "nJet25 > 1"), 
    Define( "cpvWeights", "get_cpv_weights( LHEReweightingWeight )", onData=False, onDataDriven=False),
    [Define(x,  f"cpvWeights.{x}", onData=False, onDataDriven=False) for x in ["_input_weight_sm", "_input_weight_lin_ctZI", "_input_weight_lin_ctWI"]],
]

srs = pre_sr + [ Cut( "1bl", "nBJetMedium25>0") ]
wz_cr = pre_sr + [ Cut( "0bl", "nBJetMedium25==0") ]
wz_inclusive = baseline + [    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15")]

cuts_3l_ctZI_region =  srs + [Cut("ctZI_vs_ctWI", "TMath::Abs(ctZI_score[0]) >= TMath::Abs( ctWI_score[0] )")]
cuts_3l_ctWI_region =  srs + [Cut("ctZI_vs_ctWI", "TMath::Abs(ctZI_score[0]) < TMath::Abs( ctWI_score[0] )")]


cuts_onz_bjet = Flow("3l_onz_bjet", baseline + [
     Cut( "1bl", "nBJetMedium25>0"),
     Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15"),
 ])
cuts_offz_bjet = Flow("3l_offz_bjet", baseline + [
    Cut( "1bl", "nBJetMedium25>0"),
    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) > 15"),
])
cuts_offz_nobjet = Flow("3l_offz_nobjet", baseline + [
    Cut( "1bl", "nBJetMedium25==0"),
    Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) > 15"),
])


maker = Processor(cache=SimpleCache())


if args.plotInputVars:
    plots=[Plot(x, x, inputVarsBinning[x]) for x in inputVars]
    for network in networks:
        binningArray=str(network['binning']).replace("[","{").replace("]","}")
        variableExpr=f"scoreBinning({network['variable']}, {binningArray})"
        intBinning = [ i+0.5 for i,x in enumerate(network['binning'])]
        plots.append( Plot('binned_' + network['variable'], variableExpr , intBinning, logy=True, yMin=0.1, legend="off"))

    maker.book(all_data  , lumi, Flow("SR_3l", srs)  , plots, eras=eras, withUncertainties=True)
        
if not args.skipCards:

    all_data_withsignal = all_data.copy()
    crossed_terms=[ ('ctWI_1p0_ctZI_1p0', 46)]

    for signal in ["TTLL_CPV", "TZQ_CPV"]:
        kfactor = 1.12 if signal == "TTLL_CPV" else 1.0
        
        for point, index in [('sm', 81), ("ctZI_1p0", 11), ("ctZI_m1p0", 12), ("ctWI_1p0", 7), ("ctWI_m1p0", 8)] + crossed_terms: # , ("ctGI_1p0", 15), ("ctGI_m1p0", 16)  ]: 
            all_data_withsignal.append( Process( f"{signal}_{point}",
                                                 [
                                                     MCSample(signal+'_forEval', P, xsec="4*xsec", eras=["2016", "2016APV", "2017", "2018"],hooks=[Append(AddWeight('kfactor', str(kfactor))), Append(AddWeight("point", f"LHEReweightingWeight[{index}]"))], genSumWeightName="LHEReweightingSumw[81]*genEventSumw"),
                                                     MCSample(signal, P, xsec="xsec", eras=["2022", "2022EE"],hooks=[Append(AddWeight('kfactor', str(kfactor))), Append(AddWeight("point", f"LHEReweightingWeight[{index}]"))], genSumWeightName="LHEReweightingSumw[81]*genEventSumw"),
                                             ], signal=True, fillColor=ROOT.kBlue))

    if not args.perFlavor:
            
        plots=[Plot(networks[0]['variable'], networks[0]['variable'] , networks[0]['binning'], logy=True, yMin=0.1, legend="off") ]
        maker.book(all_data_withsignal  , lumi, Flow("SR_3l_ctWI", cuts_3l_ctWI_region)  , plots, eras=eras, withUncertainties=True)

        plots=[Plot(networks[1]['variable'], networks[1]['variable'] , networks[1]['binning'], logy=True, yMin=0.1, legend="off") ]
        maker.book(all_data_withsignal  , lumi, Flow("SR_3l_ctZI", cuts_3l_ctZI_region)  , plots, eras=eras, withUncertainties=True)

    else:
        for flav in "eee,eem,emm,mmm".split(","):
            flav_cuts = Cut( flav, "nMuons == %d"%(flav.count("m")))
            plots=[Plot(networks[0]['variable'], networks[0]['variable'] , networks[0]['binning'], logy=True, yMin=0.1, legend="off") ]
            maker.book(all_data_withsignal  , lumi, Flow(f"SR_3l_ctWI_{flav}", cuts_3l_ctWI_region + [flav_cuts])  , plots, eras=eras, withUncertainties=True)

            plots=[Plot(networks[1]['variable'], networks[1]['variable'] , networks[1]['binning'], logy=True, yMin=0.1, legend="off") ]
            maker.book(all_data_withsignal  , lumi, Flow(f"SR_3l_ctZI_{flav}", cuts_3l_ctZI_region + [flav_cuts])  , plots, eras=eras, withUncertainties=True)
            

if not args.skipOffshell:
    plots=[
        Plot("lep1_pt"  , "LepFO_conePt[0]" , (10,15,200)),
        Plot("lep2_pt"  , "LepFO_conePt[1]" , (10,15,200)),
        Plot("lep3_pt"  , "LepFO_conePt[2]" , (10,15,200)),
        Plot("nJet"     , "nJet25"          , (5,-0.5,4.5), moreY=2.0),
        Plot("nBJetMedium25", "nBJetMedium25"       , (5,-0.5,4.5)),
        Plot("nBJetLoose25", "nBJetLoose25"       , (5,-0.5,4.5)),
    ]
    for network in networks:
        binningArray=str(network['binning']).replace("[","{").replace("]","}")
        variableExpr=f"scoreBinning({network['variable']}, {binningArray})"
        intBinning = [ i+0.5 for i,x in enumerate(network['binning'])]
        plots.append( Plot('binned_' + network['variable'], variableExpr , intBinning, logy=True, yMin=0.1, legend="off"))


    maker.book(all_data  , lumi, cuts_onz_bjet  , plots, eras=eras, withUncertainties=True)
    maker.book(all_data  , lumi, cuts_offz_bjet  , plots, eras=eras, withUncertainties=True)
    maker.book(all_data  , lumi, cuts_offz_nobjet  , plots, eras=eras, withUncertainties=True)

if not args.skipWZ:
    plots=[]
    for network in networks:
        binningArray=str(network['binning']).replace("[","{").replace("]","}")
        variableExpr=f"scoreBinning({network['variable']}, {binningArray})"
        intBinning = [ i+0.5 for i,x in enumerate(network['binning'])]
        plots.append( Plot('binned_' + network['variable'], variableExpr , intBinning, logy=True, yMin=0.1, legend="off"))
    cuts = Flow("3l_onz_nobjet", wz_cr)
    maker.book(all_data  , lumi, cuts, plots, eras=eras, withUncertainties=True) 

    plots=[Plot("nJets", "Sum(SelJet_isCentral & SelJet_pt > 25)", (6,-0.5,5.5))]
    cuts = Flow("3l_onz_inclusive", wz_inclusive)
    maker.book(all_data  , lumi, cuts, plots, eras=eras, withUncertainties=True)


    
results=maker.runPlots(mergeEras=(not args.dontMergeEras))

PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True, noStackSignals=True).printSet(results, f"{args.outname}/{{flow}}/{{era}}" if args.dontMergeEras else f"{args.outname}/{{flow}}",  fixRatioRange=True, maxRatioRange=(0.0,2.0), showRatio=True)

if not args.skipCards:

    cardMaker = DatacardWriter(regularize=True, autoMCStats=True)
    cardName = f"{args.outname}/{{flow}}_{{era}}" if args.dontMergeEras else f"{args.outname}/{{flow}}"
    cardMaker.makeCards(results, MultiKey(name="ctZI_score",flow="SR_3l_ctZI"), cardName)
    cardMaker.makeCards(results, MultiKey(name="ctWI_score",flow="SR_3l_ctWI"), cardName)

# if __name__ == "__main__":
#     from CMGRDF.cmdline import processorFromCommandLineArgs
#     import argparse
#     parser = argparse.ArgumentParser()
#     parser.add_argument("-s", "--seq", help="Run sequentially", action="store_true")
#     parser.add_argument("-e", "--era", help="Era to run on", default="all", choices=eras+["all"])
#     maker, args = processorFromCommandLineArgs(parser)
#     erasToRun =eras if args.era == "all" else [args.era]
#     plotdir = "plots_nov20"
#     if args.era != "all": 
#         plotdir += f"_{args.era}"
#     if args.withSystematics:
#         plotdir += "_syst"

#     plots=[Plot(networks[0]['variable'], networks[0]['variable'] , networks[0]['binning'], logy=True, yMin=0.1, legend="off") ]
#     maker.book(all_data  , lumi, cuts_3l_ctWI_region  , plots, eras=erasToRun)

#     plots=[Plot(networks[1]['variable'], networks[1]['variable'] , networks[1]['binning'], logy=True, yMin=0.1, legend="off") ]
#     maker.book(all_data  , lumi, cuts_3l_ctZI_region  , plots, eras=erasToRun)
#     if args.seq: maker.runSourcesSequentially()
#     results=maker.runPlots(mergeEras=True)
#     PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True, noStackSignals=True).printSet(results, f"{plotdir}/{{flow}}",  maxRatioRange=(0.5,1.5), showRatio=True)

#     cardMaker = DatacardWriter(regularize=True, autoMCStats=True)
#     cardMaker.makeCards(results, MultiKey(name=networks[0]['variable']), f"{plotdir}/{{flow}}")
#     cardMaker.makeCards(results, MultiKey(name=networks[1]['variable']), f"{plotdir}/{{flow}}")
# >>>>>>> giovanni/refactor_datasets



    
    
