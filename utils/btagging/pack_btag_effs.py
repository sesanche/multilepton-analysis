import ROOT as r
import rich
import gzip
import correctionlib.convert
import correctionlib.schemav2 as cs
r.gROOT.SetBatch(True)
process={'b': 'ttll', 'c' : 'ZZ', 'l': 'ZZ'}
flavors={'b': 5, 'c': 4, 'l' : 0}

for year in ['2018','2017','2016','2016APV','2022EE', '2022']:
    tempfile=r.TFile.Open(f"tmp.root", "recreate") 
    for flav in 'b', 'c', 'l': 
        for wp in 'loose', 'medium', 'tight':
            numerator  =r.TFile.Open( f"plots_btageff/selection/{year}/pt_eta_{flav}_{wp}.root" )
            denominator=r.TFile.Open( f"plots_btageff/selection/{year}/pt_eta_{flav}_all.root" )
            h_num=numerator  .Get( process[flav] )
            h_den=denominator.Get( process[flav] ) 

            c=r.TCanvas()
            eff=r.TEfficiency( h_num, h_den)
            h=eff.GetTotalHistogram().Clone( f"btag_eff_{year}_{wp}_{flav}")
            h.Reset()
            for i in range(h.GetXaxis().GetNbins()):
                for j in range(h.GetYaxis().GetNbins()):
                    bin = h.GetBin(i+1,j+1)
                    h.SetBinContent( bin, eff.GetEfficiency( bin ) )
                    h.SetBinError  ( bin, max(eff.GetEfficiencyErrorUp( bin ), eff.GetEfficiencyErrorLow( bin )))
            h.Draw("colz text e")
            tempfile.WriteTObject( h, f"btag_eff_{year}_{wp}_{flav}")
            c.SaveAs(f"btag_eff_{year}_{wp}_{flav}.png") 
    tempfile.Close()

    flavor_integrated={}
    for flav in 'b', 'c', 'l':
        all_corrs={}
        for wp in 'loose', 'medium', 'tight':
            all_corrs[wp[0].upper()]=correctionlib.convert.from_uproot_THx(f"tmp.root:btag_eff_{year}_{wp}_{flav}", axis_names=['abseta', 'pt'])
        corrss=[ cs.CategoryItem(key=key, value=all_corrs[key].data) for key in all_corrs]
        flavor_integrated[flavors[flav]] = cs.Correction( name="btageff",
                                               version = 0,
                                               inputs=[
                                                   cs.Variable( name="wp", type="string"),
                                                   cs.Variable(name="abseta", type="real"),
                                                   cs.Variable(name="pt", type="real"),
                                               ],
                                               output = all_corrs[ 'L'].output,
                                               data=cs.Category(
                                                   nodetype="category",
                                                   input="wp",
                                                   content=corrss
                                               ))
    integrated_corrs = [ cs.CategoryItem(key=key, value=flavor_integrated[key].data) for key in flavor_integrated]
    correction = cs.Correction( name="btageff",
                                version = 0,
                                inputs=[
                                    cs.Variable( name="wp", type="string"),
                                    cs.Variable(name="flav", type="int"),
                                    cs.Variable(name="abseta", type="real"),
                                    cs.Variable(name="pt", type="real"),
                                ],
                                output = all_corrs[ 'L'].output,
                                data=cs.Category(
                                    nodetype="category",
                                    input="flav",
                                    content=integrated_corrs
                                ))
    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        corrections=[correction])
    
    with gzip.open(f"data/btagging_eff_{year}.json.gz", "wt") as fout:
        fout.write(cset.json(exclude_unset=True))


                                
        

                                                            
            
