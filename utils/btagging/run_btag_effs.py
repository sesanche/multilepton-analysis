# remove 2018 prefiring

import CMGRDF

from CMGRDF import *
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4

from data.trigger import trigger_definition_sequence
from utils.dataDriven import FlipRateDefine, FakeRateDefine
import ROOT
from data.btagWPs import _btagWPs
#ROOT.EnableImplicitMT(16)

from data.common_selections import lepton_selection, tau_selection, jet_selection_notau
from CMGRDF.cms.eras import run2eras, run3eras, lumis






ROOT.gInterpreter.Declare(open( "functions.cc").read())

                         
                         

cuts=[         AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter && Flag_ecalBadCalibFilter"),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_2lss"),
               lepton_selection,
               Cut("dilep", "nLepFO>=2"),
               jet_selection_notau,
]

flow=Flow('selection', cuts)

plots = [
    Plot("kk", "1", (1,0.5,1.5)),
]

all_data=[
    Process( "ttll", MCSample("TTLL_MLL_50", "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ '2022', '2022EE'])),
    Process( "WZ"  , MCSample("WZto3LNu"   , "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ '2022', '2022EE'])),
    Process( "ZZ"  , MCSample("ZZto4L"     , "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ '2022', '2022EE'])),
    Process( "ttll", MCSample("TTZToLLNuNu_amc", "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ "2016", "2016APV", "2017", "2018"])),
    Process( "WZ"  , MCSample("WZTo3LNu_pow"   , "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ "2016", "2016APV", "2017", "2018"])),
    Process( "ZZ"  , MCSample("ZZTo4L"     , "/data/sesanche/NanoTrees_forCMGRDF_100524_summerstudent/{era}/{name}.root", xsec="xsec", eras=[ "2016", "2016APV", "2017", "2018"])),
]
maker = Processor()
for flav in [("==5", 'b'), (" == 4", 'c'), ('<4', 'l')]:
    mask=f"SelJet_hadronFlavour {flav[0]}"
    plots.append( Plot(f"pt_{flav[1]}_all", f"SelJet_pt[{mask}]", (20,0,200)))
    plots.append( Plot(f"pt_eta_{flav[1]}_all", f"SelJet_pt[{mask}]:abs(SelJet_eta[{mask}])", [[0.0, 1.0,1.8,2.4], [20.,60.,120.,1000]], typ= "Histo2D"))
    for wp in [ 'loose', 'medium', 'tight' ]:
        mask=f"(SelJet_hadronFlavour {flav[0]}) & (SelJet_btagDeepFlavB > bTagCut_{wp})"
        plots.append( Plot(f"pt_eta_{flav[1]}_{wp}", f"SelJet_pt[{mask}]:abs(SelJet_eta[{mask}])", [[0.0, 1.0,1.8,2.4], [20.,60.,120.,1000]], typ= "Histo2D"))

maker.book(all_data  , lumis, flow  , plots  , eras=run2eras+run3eras)
results=maker.runPlots()
PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True).printSet(results, "plots_btageff/{flow}/{era}")


