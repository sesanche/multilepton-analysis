import os
import ROOT
import pathlib
datadir = str(pathlib.Path(__file__).resolve().parent.parent / "data")

from CMGRDF.init import Declare
from CMGRDF.flow import Define, Vary
from CMGRDF.CorrectionlibFactory import CorrectionlibFactory
from CMGRDF.cms.eras import run2eras, run3eras
from CMGRDF import AddWeightUncertainty
class LeptonScaleFactorDefine( Define ):
    def __init__(self, nlep, doSyst, **options):
        if len(options['eras']) != 1:
            raise RuntimeError("Can only use LeptonScaleFactorDefine for a single era")
        self.era=options['eras'][0]

        super().__init__( f"leptonSF_weight_{nlep}l",
                          f"leptonSF_{self.era}( LepFO_pt, LepFO_eta, LepFO_pdgId, {nlep}, <syst>)",
                          onData=False, onDataDriven=False, **options)

        
        self._fname = f"{datadir}/leptonSF_{self.era}.json.gz"
        self._corrname = f"leptonSF" 
        self.doSyst=doSyst
        self._init = False

    def init( self ):

        leptonCorrector = CorrectionlibFactory.loadCorrector( self._fname, self._corrname, check=True, access_method="compound().at" )[0]

        if not hasattr(ROOT, f'leptonSF_{self.era}'): # we may load this several time, one for each nlep
            Declare('''float  leptonSF_<era>(const ROOT::RVec<float>& pts, const ROOT::RVec<float>& etas, const ROOT::RVec<int>& pdgIds, int nlep, std::string syst  ){
        float sf=1.;
        std::string channel_str = ( nlep == 2) ? "2lss" : "3l";
        for (int i=0; i<min(int(nlep),int(pdgIds.size())); ++i){
            sf*=<CORRID>->evaluate({TMath::Abs(etas[i]), TMath::Max(float(15.1),TMath::Min(float(119.),float(pts[i]))), syst, TMath::Abs(pdgIds[i]), channel_str});
        }

        return sf;
}'''.replace( "<era>", self.era).replace( "<CORRID>", leptonCorrector))

            Declare('''ROOT::RVec<float> leptonSF_<era>_syst( const ROOT::RVec<float>& pts, const ROOT::RVec<float>& etas, const ROOT::RVec<int>& pdgIds, int nlep, std::string syst  ){
        ROOT::RVec<float> ret={1.,1.};
        ret[0]=leptonSF_<era>(pts,etas,pdgIds,nlep,syst+"up");
        ret[1]=leptonSF_<era>(pts,etas,pdgIds,nlep,syst+"dn");
        return ret;
}
'''.replace( "<era>", self.era)) 
        self._init=True

    def _attach( self, rdf, withUncertainties):
        if not self._init:
            self.init()
        try:
            rdf=rdf.Define(self.name, self.expr.replace("<syst>", '""'))
            if self.doSyst and withUncertainties:
                for syst in ['_el', '_mu']:
                    src=rdf
                    rdf=rdf.Vary( self.name,
                                  self.expr.replace("<syst>", '"%s"'%syst).replace(f"leptonSF_{self.era}", f"leptonSF_{self.era}_syst"),
                                  variationTags=['up', 'down'], variationName=f"CMS_TOP24012{syst}")
                    rdf._from=src
            return rdf
           
        
        except BaseException:
            print(f"ERROR attaching Define({self.name}, {self.expr}")
            raise
            


class ElectronRecoSFDefine( Define ):
    def __init__(self, nlep, doSyst, **options):

        if len(options['eras']) != 1:
            raise RuntimeError("Can only use ElectronRecoSFDefine for a single era")
        self.era=options['eras'][0]


        super().__init__( f"leptonSFreco_weight_{nlep}l",
                          f"leptonSF_reco_{self.era}( LepFO_pt, LepFO_eta, LepFO_pdgId, {nlep}, <syst>)",
                          onData=False, onDataDriven=False, **options)

        self._fname = f"{datadir}/electron_{self.era}.json"
        self._corrname = f"Electron-ID-SF" 
        self.doSyst=doSyst
        self._init = False

    def init( self ):
        leptonCorrector = CorrectionlibFactory.loadCorrector( self._fname, self._corrname, check=True )[0]
        
        if not hasattr(ROOT, f'leptonSF_reco_{self.era}'): # we may load this several time, one for each nlep
            Declare('''float leptonSF_reco_<era>(const ROOT::RVec<float>& pts, const ROOT::RVec<float>& etas, const ROOT::RVec<int>& pdgIds, int nlep, std::string syst  ){
        float sf=1.;

        for (int i=0; i<min(int(nlep),int(pdgIds.size())); ++i){
            if (TMath::Abs(pdgIds[i]) == 13) continue;
            for (auto& patch_string : {"RecoBelow20", "Reco20to75", "RecoAbove75"}){
                sf*=<CORRID>->evaluate({<YEARKEY>, syst, patch_string, etas[i], pts[i]});
            }
        }

        return sf;
}'''.replace( "<era>", self.era).replace( "<CORRID>", leptonCorrector).replace("<YEARKEY>", '"2022Re-recoBCD"' if self.era == "2022" else '"2022Re-recoE+PromptFG"'))

            Declare('''ROOT::RVec<float> leptonSF_reco_<era>_syst( const ROOT::RVec<float>& pts, const ROOT::RVec<float>& etas, const ROOT::RVec<int>& pdgIds, int nlep, std::string syst  ){
        ROOT::RVec<float> ret={1.,1.};
        ret[0]=leptonSF_reco_<era>(pts,etas,pdgIds,nlep,"sfup");
        ret[1]=leptonSF_reco_<era>(pts,etas,pdgIds,nlep,"sfdown");
        return ret;
}
'''.replace( "<era>", self.era)) 
        self._init=True

    def _attach( self, rdf, withUncertainties):
        if not self._init:
            self.init()
        try:
            src=rdf
            rdf=rdf.Define(self.name, self.expr.replace("<syst>", '"sf"'))
            src._from=rdf
            if self.doSyst and withUncertainties:
                src=rdf
                rdf=rdf.Vary( self.name,
                              self.expr.replace("<syst>", '""').replace(f"leptonSF_reco_{self.era}", f"leptonSF_reco_{self.era}_syst"),
                              variationTags=['up', 'down'], variationName=f"CMS_TOP24012_el")
                src._from=rdf
            return rdf
           
        
        except BaseException:
            print(f"ERROR attaching Define({self.name}, {self.expr}")
            raise
            

leptonSFsequences       = [ LeptonScaleFactorDefine(nlep, True, eras=[era]) for era in  run2eras+run3eras for nlep in [2,3]]
leptonSFsequences.extend( [ ElectronRecoSFDefine(nlep, True, eras=[era]) for era in  run3eras for nlep in [2,3]])
leptonSFsequences.extend( [ Define(f"leptonSFreco_weight_{nlep}l", "1", eras=[era], onData=False, onDataDriven=False) for era in  run2eras for nlep in [2,3]]) # reco-corrections are already included in the run 2 json
leptonSFsequences.extend( [ Define("triggerSF_3l", "1")] + [ AddWeightUncertainty(f"CMS_TOP24012_trigger_eff_{era}", "1.02", eras=[era], onData=False, onDataDriven=False) for era in run2eras] + [ AddWeightUncertainty(f"CMS_TOP24012_trigger_eff_{era}", "1.01", eras=[era], onData=False, onDataDriven=False) for era in run3eras])
