import os
import ROOT
import pathlib
datadir = str(pathlib.Path(__file__).resolve().parent.parent / "data")

from CMGRDF.init import Declare
from CMGRDF.flow import Define
from CMGRDF.CorrectionlibFactory import CorrectionlibFactory
from CMGRDF.cms.eras import run2eras

class FlipRateDefine( Define ):
    def __init__(self, era, **options):
        super().__init__( "flip_rate_weight",
                          f"flip_weight_{era}( LepFO_pt[0], LepFO_eta[0], LepFO_pdgId[0], LepFO_pt[1], LepFO_eta[1], LepFO_pdgId[1])",
                          onData=False, onDataDriven=True, **options)
        self.era=era
        self._init = False
        self._fname = f"{datadir}/flip_rates.json"
        self._corrname = f"flipRate_{era}"

    def init( self ):
        flipCorrector = CorrectionlibFactory.loadCorrector( self._fname, self._corrname, check=True )[0]

        Declare('''float flip_weight_<era>( const float pt1, const float eta1, const int pdgId1, const float pt2, const float eta2, const int pdgId2){
        if ( pdgId1 * pdgId2 > 0) return 0.;
        float thept1 = TMath::Min(float(199.), pt1);
        float thept2 = TMath::Min(float(199.), pt2);
        float theeta1= TMath::Abs(eta1);
        float theeta2= TMath::Abs(eta2);

        float fl1= (abs(pdgId1) == 11) ? <CORRID>->evaluate({thept1, theeta1}) : 0;
        float fl2= (abs(pdgId2) == 11) ? <CORRID>->evaluate({thept2, theeta2}) : 0;

        return fl1+fl2;
}'''.replace( "<era>", self.era).replace( "<CORRID>", flipCorrector))
        self._init=True

    def _attach( self, rdf, withUncertainties):
        if not self._init:
            self.init()
        try:
            return rdf.Define(self.name, self.expr)
        except BaseException:
            print(f"ERROR attaching Define({self.name}, {self.expr}")
            raise
            
                                                            

class FakeRateDefine( Define ):
    def __init__(self, era, nlep, doSyst, fname=f"{datadir}/fake_rates.json", corrname=None, **options):
        super().__init__( f"fake_rate_weight_{nlep}l",
                          f"fake_rate_weight_{nlep}l_{'2016APV_2016' if '2016' in era else '2022_2022EE' if '2022' in era else era}(" + ",".join( [f" LepFO_conePt[{i}], LepFO_eta[{i}], LepFO_pdgId[{i}], LepFO_isTight[{i}] " for i in range(nlep)] ) + ")",
                        onData=False, onDataDriven=True, onMC=True,
                        **options)
        self.era='2016APV_2016' if '2016' in era else '2022_2022EE' if '2022' in era else era
        self._init = False
        self.nlep = nlep
        self._fname = fname
        if corrname is None:
            self._corrname=f"fakeRate_{self.era}"
        else:
            self._corrname = corrname
        self.doSyst=doSyst
        
    def init( self ):
        if hasattr(ROOT, f"fake_rate_weight_{self.nlep}l_{self.era}"):
            return 
        fakeRateCorrector = CorrectionlibFactory.loadCorrector( self._fname, self._corrname, check=True )[0]
        variations=[ 'norm', ['_up', '_down']] , [ 'be', ['_be1', '_be2']] , [ 'pt', ['_pt1', '_pt2']]
        if self.nlep == 2:
            # nominal 
            Declare('''float fake_rate_weight_2l_<era>( float l1pt, float l1eta, int l1pdgId, bool l1pass,
                                                                          float l2pt, float l2eta, int l2pdgId, bool l2pass){
    if (l1pass && l2pass) return 0;
    float ret = -1.0;
    if (!l1pass) { 
        float l1fr = <CORRID>->evaluate({TMath::Min(l1pt, float(99.)), TMath::Abs(l1eta), "", TMath::Abs(l1pdgId)});
        ret *= -l1fr/(1.0f-l1fr);
    }
    if (!l2pass) {
        float l2fr = <CORRID>->evaluate({TMath::Min(l2pt, float(99.)), TMath::Abs(l2eta), "", TMath::Abs(l2pdgId)});
        ret *= -l2fr/(1.0f-l2fr);
    }
    return ret;
}'''.replace( "<era>", self.era).replace( "<CORRID>", fakeRateCorrector))

            # systematics
            for name, var in variations:
                Declare('''ROOT::RVec<float> fake_rate_weight_2l_<era>_syst_<name>( float l1pt, float l1eta, int l1pdgId, bool l1pass,
                                                                                                      float l2pt, float l2eta, int l2pdgId, bool l2pass, int abspdgid){ // abspdgid is the pdpgid to apply the uncertainties

    std::vector<std::string> variations= {<variations>};
    ROOT::RVec<float> ret(variations.size());
    for (unsigned int ivar=0; ivar < variations.size(); ++ivar){
        if (l1pass && l2pass){
            ret[ivar]= 0.;
            continue;
        }
        ret[ivar] = -1.0;
        if (!l1pass) { 
            std::string thevariation = (TMath::Abs(l1pdgId) == abspdgid)  ? variations[ivar] : "";
            float l1fr = <CORRID>->evaluate({TMath::Min(l1pt, float(99.)), TMath::Abs(l1eta), thevariation, TMath::Abs(l1pdgId)});
            ret[ivar] *= -l1fr/(1.0f-l1fr);
        }
        if (!l2pass) {
            std::string thevariation = (TMath::Abs(l2pdgId) == abspdgid)  ? variations[ivar] : "";
            float l2fr = <CORRID>->evaluate({TMath::Min(l2pt, float(99.)), TMath::Abs(l2eta), thevariation, TMath::Abs(l2pdgId)});
            ret[ivar] *= -l2fr/(1.0f-l2fr);
        }
    }
    return ret;
}'''.replace( "<era>", self.era).replace( "<CORRID>", fakeRateCorrector).replace("<name>", name).replace("<variations>", ",".join([ '"%s"'%v for v in var] )))

        elif self.nlep == 3:
            # nominal 
            Declare('''float fake_rate_weight_3l_<era>( float l1pt, float l1eta, int l1pdgId, bool l1pass,
                                                                          float l2pt, float l2eta, int l2pdgId, bool l2pass,
                                                                          float l3pt, float l3eta, int l3pdgId, bool l3pass){
    if (l1pass && l2pass && l3pass) return 0;
    float ret = -1.0;
    if (!l1pass) { 
        float l1fr = <CORRID>->evaluate({TMath::Min(l1pt, float(99.)), TMath::Abs(l1eta), "", TMath::Abs(l1pdgId)});
        ret *= -l1fr/(1.0f-l1fr);
    }
    if (!l2pass) {
        float l2fr = <CORRID>->evaluate({TMath::Min(l2pt, float(99.)), TMath::Abs(l2eta), "", TMath::Abs(l2pdgId)});
        ret *= -l2fr/(1.0f-l2fr);
    }
    if (!l3pass) {
        float l3fr = <CORRID>->evaluate({TMath::Min(l3pt, float(99.)), TMath::Abs(l3eta), "", TMath::Abs(l3pdgId)});
        ret *= -l3fr/(1.0f-l3fr);
    }
    return ret;
}'''.replace( "<era>", self.era).replace( "<CORRID>", fakeRateCorrector))

            # systematics
            for name, var in variations:
                Declare('''ROOT::RVec<float> fake_rate_weight_3l_<era>_syst_<name>( float l1pt, float l1eta, int l1pdgId, bool l1pass,
                                                                                                      float l2pt, float l2eta, int l2pdgId, bool l2pass,
                                                                                                      float l3pt, float l3eta, int l3pdgId, bool l3pass, int abspdgid){ // abspdgid is the pdpgid to apply the uncertainties

    std::vector<std::string> variations= {<variations>};
    ROOT::RVec<float> ret(variations.size());

    for (unsigned int ivar=0; ivar < variations.size(); ++ivar){
        if (l1pass && l2pass && l3pass){
            ret[ivar]= 0.;
            continue;
        }
        ret[ivar] = -1.0;
        if (!l1pass) { 
            std::string thevariation = (TMath::Abs(l1pdgId) == abspdgid)  ? variations[ivar] : "";
            float l1fr = <CORRID>->evaluate({TMath::Min(l1pt, float(99.)), TMath::Abs(l1eta), thevariation, TMath::Abs(l1pdgId)});
            ret[ivar] *= -l1fr/(1.0f-l1fr);
        }
        if (!l2pass) {
            std::string thevariation = (TMath::Abs(l2pdgId) == abspdgid)  ? variations[ivar] : "";
            float l2fr = <CORRID>->evaluate({TMath::Min(l2pt, float(99.)), TMath::Abs(l2eta), thevariation, TMath::Abs(l2pdgId)});
            ret[ivar] *= -l2fr/(1.0f-l2fr);
        }
        if (!l3pass) {
            std::string thevariation = (TMath::Abs(l3pdgId) == abspdgid)  ? variations[ivar] : "";
            float l3fr = <CORRID>->evaluate({TMath::Min(l3pt, float(99.)), TMath::Abs(l3eta), thevariation, TMath::Abs(l3pdgId)});
            ret[ivar] *= -l3fr/(1.0f-l3fr);
        }
    }
    return ret;
}'''.replace( "<era>", self.era).replace( "<CORRID>", fakeRateCorrector).replace("<name>", name).replace("<variations>", ",".join([ '"%s"'%v for v in var] )))


        else:
            raise NotImplementedError


        self._init=True

    def _attach( self, rdf, withUncertainties):
        if not self._init:
            self.init()
        try:
            src=rdf
            rdf= rdf.Define(self.name, self.expr)
            rdf._from=src
            if self.doSyst and withUncertainties:
                for lep in ['e', 'm'] :
                    src=rdf
                    rdf= rdf.Vary( self.name, self.expr.replace( '(', '_syst_norm(').replace(")", ", %d)"%(11 if lep == 'e' else 13)), variationTags=["up", "down"], variationName=f'CMS_TOP24012_FR{lep}_norm')
                    rdf._from=src
                    src=rdf
                    rdf= rdf.Vary( self.name, self.expr.replace( '(', '_syst_be(')  .replace(")", ", %d)"%(11 if lep == 'e' else 13)), variationTags=["up", "down"], variationName=f'CMS_TOP24012_FR{lep}_be')
                    src=rdf
                    rdf._from=src
                    src=rdf
                    rdf= rdf.Vary( self.name, self.expr.replace( '(', '_syst_pt(')  .replace(")", ", %d)"%(11 if lep == 'e' else 13)), variationTags=["up", "down"], variationName=f'CMS_TOP24012_FR{lep}_pt')
                    rdf._from=src
            return rdf

        
        except BaseException:
            print(f"ERROR attaching Define({self.name}, {self.expr}")
            raise
            
                                                            
class FakeRateDefineFromJindrich( Define ):
    def __init__(self, nlep=3, fnames=[], corrnames=[], suffix="", **options):
        super().__init__( f"fake_rate_weight{suffix}",
                          f"fake_rate_weight{suffix}( LepFO_conePt, LepFO_eta, LepFO_pdgId, LepFO_isTight, {nlep})",
                          onData=False, onDataDriven=True, onMC=True,
                          **options)

        self._init = False
        self.suffix=suffix
        self.nlep = nlep
        self._fnames    = fnames    # electron, muons
        self._corrnames = corrnames
        if len(self._fnames) != 2 or len(self._corrnames) != 2:
            raise RuntimeError
        
    def init( self ):

        fakeRateCorrectors = [ CorrectionlibFactory.loadCorrector( x, y, check=True )[0] for (x,y) in zip(self._fnames, self._corrnames)]


        Declare('''float fake_rate_weight<suffix>( const ROOT::RVecF & lpt, const ROOT::RVecF & leta, const ROOT::RVecI & lpdgId, const ROOT::RVecB & lpass, int nlep){
            
            float ret = -1.0; int nfail=0;
            for (int ilep=0; ilep<nlep;++ilep){
                 if (!lpass.at(ilep)){
                      auto corr = (abs(lpdgId.at(ilep)) == 11 ) ? <CORRIDMU> : <CORRIDEL>;
                      float fr = corr->evaluate({lpt.at(ilep), TMath::Abs(leta.at(ilep))});
                      ret *= -fr/(1.0f-fr);
                      nfail++;
                 }
            }
            if (nfail == 0){ return 0;}
            return ret;
}'''.replace( "<CORRIDEL>", fakeRateCorrectors[0]).replace( "<CORRIDMU>", fakeRateCorrectors[1]).replace("<suffix>", self.suffix))


        self._init=True

    def _attach( self, rdf, withUncertainties):
        if not self._init:
            self.init()
        try:
            return rdf.Define(self.name, self.expr)
        except BaseException:
            print(f"ERROR attaching Define({self.name}, {self.expr}")
            raise
        
