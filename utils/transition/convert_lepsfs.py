import correctionlib.convert
import ROOT as r
import rich
from correctionlib.schemav2 import Binning, Category, Correction, CorrectionSet
import correctionlib.schemav2 as cs
from array import array


                                

def correction_from_file( fil, name, correction_name, variation_names, pdgId, all_variations, flow='error'):
    tf=r.TFile.Open( fil )
    nominal=tf.Get( name )

    outf=r.TFile.Open( fil.replace(".root", "_withsyst.root"), 'recreate')
    outf.WriteTObject( nominal, name )
    up=nominal.Clone("up"); dn=nominal.Clone("dn")
    outf.WriteTObject( up, name + "_up")
    outf.WriteTObject( dn, name + "_dn")
    outf.Close()

    content={} 
    content['']                  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}"   , axis_names=['abseta', 'pt'], flow=flow)
    content[variation_names[0]]  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}_up", axis_names=['abseta', 'pt'], flow=flow)
    content[variation_names[1]]  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}_dn", axis_names=['abseta', 'pt'], flow=flow)

    all_variations.remove( variation_names[0])
    all_variations.remove( variation_names[1])
    for var in all_variations:
        content[var]=content['']

    contents=[ cs.CategoryItem(key=key, value=content[key].data) for key in content]

    corrections_allflavors={}
    corrections_allflavors[pdgId] = cs.Correction( name=correction_name, description=correction_name,
                                version = 0,
                                inputs = [
                                    cs.Variable(name="abseta", type="real"),
                                    cs.Variable(name="pt", type="real"),
                                    cs.Variable(name="syst", type="string"),
                                ],
                                output = content[ ''].output,
                                data=cs.Category(
                                    nodetype="category",
                                    input='syst',
                                    content=contents,
                                    ))

    corrections_allflavors[24-pdgId] = cs.Correction( name=correction_name+"_dummy", description="dummy correction, that returns one (muon sf applied to electrons or viceversa",
                                                      version=0,
                                                      inputs = [],
                                                      output=content[ ''].output,
                                                      data=1.0)
                                      
    contents_allflavors = [cs.CategoryItem(key=key, value=corrections_allflavors[key].data) for key in corrections_allflavors]

    correction_allflavors=cs.Correction( name=correction_name+"_allflavor", description=correction_name,
                                         version = 0,
                                         inputs = [
                                             cs.Variable(name="abseta", type="real"),
                                             cs.Variable(name="pt", type="real"),
                                             cs.Variable(name="syst", type="string"),
                                             cs.Variable(name="abspdgid", type="int"),
                                         ],
                                         output = content[ ''].output,
                                         data=cs.Category(
                                             nodetype="category",
                                             input='abspdgid',
                                             content=contents_allflavors))
                                        
    
    return correction_allflavors

def duplicate_channel( corr, channels ): 
    contents_channels = [cs.CategoryItem(key=key, value=corr.data) for key in channels]
    return cs.Correction( name=corr.name+"_channels", description=corr.name,
                          version = 0,
                          inputs = [
                              cs.Variable(name="abseta", type="real"),
                              cs.Variable(name="pt", type="real"),
                              cs.Variable(name="syst", type="string"),
                              cs.Variable(name="abspdgid", type="int"),
                              cs.Variable(name="channel", type="string"),
                          ],
                          output = corr.output,
                          data=cs.Category(
                              nodetype="category",
                              input='channel',
                              content=contents_channels))
                                        
def merge_channels( corrs, channels):
    contents_channels= [ cs.CategoryItem( key=key, value=corr.data) for key,corr in zip(channels,corrs)]
    return cs.Correction( name=corrs[0].name+"_channels", description=corrs[0].name,
                          version = 0,
                          inputs = [
                              cs.Variable(name="abseta", type="real"),
                              cs.Variable(name="pt", type="real"),
                              cs.Variable(name="syst", type="string"),
                              cs.Variable(name="abspdgid", type="int"),
                              cs.Variable(name="channel", type="string"),
                          ],
                          output = corrs[0].output,
                          data=cs.Category(
                              nodetype="category",
                              input='channel',
                              content=contents_channels))
    
    
all_variations=[ "_muup", "_mudn", "_elup", "_eldn"]
for era in [ "2016APV", "2016", "2017", "2018"]:

    mu1=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/muon/egammaEffi{era}_EGM2D.root"    ,"EGamma_SF2D", "mu_1", [ "_muup", "_mudn"], 13, all_variations.copy())
    mu2=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/muon/egammaEffi{era}_iso_EGM2D.root","EGamma_SF2D", "mu_2", [ "_muup", "_mudn"], 13, all_variations.copy())
    mu1_channel=duplicate_channel( mu1, [ '2lss', '3l'])
    mu2_channel=duplicate_channel( mu2, [ '2lss', '3l'])

    el1_2lss=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elecNEWmva/egammaEffi{era}_2lss_EGM2D.root", "EGamma_SF2D", "el1", ["_elup", "_eldn"], 11, all_variations.copy())
    el1_3l  =correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elecNEWmva/egammaEffi{era}_3l_EGM2D.root", "EGamma_SF2D", "el1", ["_elup", "_eldn"], 11, all_variations.copy())
    el1_channel=merge_channels( [el1_2lss,el1_3l], ["2lss", "3l"] )
    
    el2=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elec/egammaEffi{era}_iso_EGM2D.root", "EGamma_SF2D", "el2", ["_elup", "_eldn"], 11, all_variations.copy())
    el3=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elec/egammaEffi{era}_recoToloose_EGM2D.root", "EGamma_SF2D", "el3", ["_elup", "_eldn"], 11, all_variations.copy())

    el4=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elec/egammaEffi{era}_ptAbove20_EGM2D.root", "EGamma_SF2D", 'el4', ["_elup", "_eldn"], 11, all_variations.copy(), flow=1.0) # flows to 1 because this is only pt > 20
    el5=correction_from_file( f"cmgtools-lite/TTHAnalysis/data/leptonSF/elec/egammaEffi{era}_ptBelow20_EGM2D.root", "EGamma_SF2D", 'el5', ["_elup", "_eldn"], 11, all_variations.copy(), flow=1.0) # flows to 1 because this is only pt < 20
       
    
    el2_channel=duplicate_channel( el2, [ '2lss', '3l'])
    el3_channel=duplicate_channel( el3, [ '2lss', '3l'])
    el4_channel=duplicate_channel( el4, [ '2lss', '3l'])
    el5_channel=duplicate_channel( el5, [ '2lss', '3l'])
    
    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        corrections=[ mu1_channel,mu2_channel, el1_channel, el2_channel, el3_channel, el4_channel, el5_channel],
        compound_corrections=[
            cs.CompoundCorrection( name="leptonSF",
                                   inputs=[
                                       cs.Variable(name="abseta", type="real"),
                                       cs.Variable(name="pt", type="real"),
                                       cs.Variable(name="syst", type="string"),
                                       cs.Variable(name="abspdgid", type="int"),
                                       cs.Variable(name="channel", type="string"),
                                   ],
                                   inputs_update=[],
                                   output=mu1.output,
                                   output_op="*",
                                   input_op="*",
                                   stack=[
                                       'mu_1_allflavor_channels', 'mu_2_allflavor_channels',
                                       'el1_allflavor_channels', 'el2_allflavor_channels', 'el3_allflavor_channels',
                                       'el4_allflavor_channels', 'el5_allflavor_channels', 
                                   ]
                                  )
        ])

    import gzip

    with gzip.open(f"../data/leptonSF_{era}.json.gz", "wt") as fout:
        fout.write(cset.json(exclude_unset=True))


    
    
