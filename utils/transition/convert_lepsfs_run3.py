import correctionlib.convert
import ROOT as r
import rich
from correctionlib.schemav2 import Binning, Category, Correction, CorrectionSet
import correctionlib.schemav2 as cs
from array import array
import json

                                

def correction_from_file( fil, name, correction_name, variation_names, pdgId, all_variations, flow='error'):
    tf=r.TFile.Open( fil )
    nominal=tf.Get( name )

    outf=r.TFile.Open( fil.replace(".root", "_withsyst.root"), 'recreate')
    outf.WriteTObject( nominal, name )
    up=nominal.Clone("up"); dn=nominal.Clone("dn")
    for what,sign in [(up,1), (dn,-1)]:
        for binx in range(what.GetNbinsX()):
            for biny in range(what.GetNbinsY()):
                bin=what.GetBin(binx,biny)
                what.SetBinContent( bin, what.GetBinContent(bin) + sign*what.GetBinError(bin))
                
    outf.WriteTObject( up, name + "_up")
    outf.WriteTObject( dn, name + "_dn")
    outf.Close()

    content={} 
    content['']                  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}"   , axis_names=['abseta', 'pt'], flow=flow)
    content[variation_names[0]]  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}_up", axis_names=['abseta', 'pt'], flow=flow)
    content[variation_names[1]]  = correctionlib.convert.from_uproot_THx(fil.replace(".root", "_withsyst.root") + f":{name}_dn", axis_names=['abseta', 'pt'], flow=flow)

    all_variations.remove( variation_names[0])
    all_variations.remove( variation_names[1])
    for var in all_variations:
        content[var]=content['']

    contents=[ cs.CategoryItem(key=key, value=content[key].data) for key in content]

    corrections_allflavors={}
    corrections_allflavors[pdgId] = cs.Correction( name=correction_name, description=correction_name,
                                version = 0,
                                inputs = [
                                    cs.Variable(name="abseta", type="real"),
                                    cs.Variable(name="pt", type="real"),
                                    cs.Variable(name="syst", type="string"),
                                ],
                                output = content[ ''].output,
                                data=cs.Category(
                                    nodetype="category",
                                    input='syst',
                                    content=contents,
                                    ))

    corrections_allflavors[24-pdgId] = cs.Correction( name=correction_name+"_dummy", description="dummy correction, that returns one (muon sf applied to electrons or viceversa",
                                                      version=0,
                                                      inputs = [],
                                                      output=content[ ''].output,
                                                      data=1.0)
                                      
    contents_allflavors = [cs.CategoryItem(key=key, value=corrections_allflavors[key].data) for key in corrections_allflavors]

    correction_allflavors=cs.Correction( name=correction_name+"_allflavor", description=correction_name,
                                         version = 0,
                                         inputs = [
                                             cs.Variable(name="abseta", type="real"),
                                             cs.Variable(name="pt", type="real"),
                                             cs.Variable(name="syst", type="string"),
                                             cs.Variable(name="abspdgid", type="int"),
                                         ],
                                         output = content[ ''].output,
                                         data=cs.Category(
                                             nodetype="category",
                                             input='abspdgid',
                                             content=contents_allflavors))
                                        
    
    return correction_allflavors

    
    
all_variations=[ "_muup", "_mudn", "_elup", "_eldn"]
for era in [ "2022", "2022EE"]:



    mu = correction_from_file( f"data/sfs/{era}/MuonSF-mvaTTH-WP064_nanoAODv12.root", "EGamma_SF2D", "mu", [ "_muup", "_mudn"], 13, all_variations.copy())
    el = correction_from_file( f"data/sfs/{era}/ElectronSF-mvaTTH-WP090_nanoAODv12.root", "EGamma_SF2D", 'el', [ "_elup", "_eldn"], 11, all_variations.copy())
       
    
    
    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        corrections=[ mu, el],
        compound_corrections=[
            cs.CompoundCorrection( name="leptonSF",
                                   inputs=[
                                       cs.Variable(name="abseta", type="real"),
                                       cs.Variable(name="pt", type="real"),
                                       cs.Variable(name="syst", type="string"),
                                       cs.Variable(name="abspdgid", type="int"),
                                       cs.Variable(name="channel", type="string"),
                                   ],
                                   inputs_update=[],
                                   output=mu.output,
                                   output_op="*",
                                   input_op="*",
                                   stack=[
                                       'mu_allflavor', 'el_allflavor',
                                   ]
                                  )
        ])

    import gzip

    with gzip.open(f"data/leptonSF_{era}.json.gz", "wt") as fout:
        fout.write(cset.json(exclude_unset=True))


    
    
