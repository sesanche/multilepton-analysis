import correctionlib.convert
import ROOT as r
import rich
from correctionlib.schemav2 import Binning, Category, Correction, CorrectionSet
import correctionlib.schemav2 as cs


corrections=[]
flavor_map={
    "2016APV_2016" : [( 'FR_mva085_mu', 13), ('FR_mva090_el',11)],
    "2017"         : [( 'FR_mva085_mu', 13), ('FR_mva090_el',11)],
    "2018"         : [( 'FR_mva085_mu', 13), ('FR_mva090_el',11)],
    "2022_2022EE"  : [( 'FR_mva085_mu', 13), ('FR_mva064_el',11)],
}


all_corrections=[]
for era in [ "2016APV_2016", "2017", "2018", "2022_2022EE"]:
    content_perflavor={}
    for flavor, pdgId in flavor_map[era]:
        suffix = "" if "mu" in flavor else "_NC"
        content={}
        for syst in [ "", "_up", "_down", "_pt1", "_pt2", "_be1", "_be2"]:
            recorrected = "" if era == "2022_2022EE" else '_recorrected' 
            fakerate = correctionlib.convert.from_uproot_THx(f"data/fr_{era}{recorrected}.root:{flavor}_data_comb{suffix}{recorrected}{syst}", axis_names=['pt', 'abseta'], flow='error')
            content[syst]=fakerate
    
        contents=[ cs.CategoryItem(key=key, value=content[key].data) for key in content]
        perlep_corr = cs.Correction( name= f"fakeRate_{era}_{flavor}",
                                     description = f"Fakerate for {flavor}s",
                                     version = 0,
                                     inputs = [
                                         cs.Variable(name="pt", type="real"),
                                         cs.Variable(name="abseta", type="real"),
                                         cs.Variable(name="syst", type="string"),
                                     ],
                                     output = content[''].output,
                                     data=cs.Category(
                                         nodetype="category",
                                         input="syst",
                                         content=contents
                                     ))
    
        content_perflavor[pdgId]=perlep_corr
    contents_per_flavor=[ cs.CategoryItem(key=key, value=content_perflavor[key].data) for key in content_perflavor]

    perlep_corr = cs.Correction( name= f"fakeRate_{era}",
                                 description = f"Fakerate",
                                 version = 0,
                                 inputs = [
                                     cs.Variable(name="pt", type="real"),
                                     cs.Variable(name="abseta", type="real"),
                                     cs.Variable(name="syst", type="string"),
                                     cs.Variable(name="abspdgid", type="int"),
                                 ],
                                 output = content_perflavor[11].output,
                                 data=cs.Category(
                                     nodetype="category",
                                     input="abspdgid",
                                     content=contents_per_flavor
                                 ))
    rich.print(perlep_corr)
    all_corrections.append( perlep_corr ) 

cset = CorrectionSet.parse_obj({
    "schema_version": 2,
    "description"   : "Fakerates",
    "corrections"   : all_corrections})

outf=open( f"data/fake_rates.json", 'w')
outf.write( cset.json(indent=4) )
outf.close()
