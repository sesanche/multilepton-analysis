# remove 2018 prefiring

import CMGRDF

from CMGRDF import *
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4
from utils.leptonScaleFactors import LeptonScaleFactorDefine

from data.trigger import trigger_definition_sequence
from utils.dataDriven import FlipRateDefine, FakeRateDefine
from data.datasets import data_3l
import ROOT
ROOT.EnableImplicitMT(16)

from data.common_selections import lepton_selection, tau_selection, jet_selection
eras  = ["2018"]
addData=True
lumi = {'2018': 59.7}




verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(), ROOT.Experimental.ELogLevel.kInfo) 

ROOT.gInterpreter.Declare(open( "functions.cc").read())

                         
                         

cuts_3l = Flow("SR_3l",
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter"),
               Cut("filters_no2016", "Flag_ecalBadCalibFilter", eras=[2016]),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_3l"),
               lepton_selection,
               Cut("trilep", "nLepFO>=3"),
               Cut("exclusive", "nLepTight <= 3"),
               Cut("cleanup", "minMllFO > 12"),
               Cut("pt251515", "LepFO_conePt[0]>25  && LepFO_conePt[1]>15  && LepFO_conePt[2]>15"),
               Cut("TTT", " LepFO_isTight[0] && LepFO_isTight[1] && LepFO_isTight[2]"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Cut("q1", "abs(LepFO_charge[0]+LepFO_charge[1]+LepFO_charge[2]) == 1"),
               Cut("ele conv cuts", "(abs(LepFO_pdgId[0])!=11 || (LepFO_convVeto[0] && LepFO_lostHits[0]==0)) && (abs(LepFO_pdgId[1])!=11 || (LepFO_convVeto[1] && LepFO_lostHits[1]==0)) && (abs(LepFO_pdgId[2])!=11 || (LepFO_convVeto[2] && LepFO_lostHits[2]==0))"),
               LeptonScaleFactorDefine( "2018", 3, False),
               AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               #AddWeight("mcWeight", "leptonSF_3l*leptonSF_weight*triggerSF_3l*btagSF*puWeight*L1PreFiringWeight_Nom", onData=False, onDataDriven=False), 
               AddWeight("mcWeight", "leptonSF_weight*L1PreFiringWeight_Nom", onData=False, onDataDriven=False), 
               tau_selection,
               jet_selection,
)


plots_3l = [
    Plot("nLepFO"     , "nLepFO"     , (7,-0.5,6.5), xTitle="nLepFO"),
    Plot("nJet25", "nJet25", (7,-0.5,6.5)),
    Plot("nBJetMedium25", "nBJetMedium25", (4,-0.5,3.5)),
    Plot("Jet_isSelected", "Jet_isSelected", (2,-0.5,1.5)),
    Plot("mZ1_FO", "mZ1_FO", (50,0,200)),

]





maker = Processor()
maker.book(data_3l  , lumi, cuts_3l  , plots_3l  , eras=eras, withUncertainties=True)
results=maker.runPlots()
PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True).printSet(results, "plots_{flow}/{era}", maxRatioRange=(0.5,1.5), showRatio=True) 


