# remove 2018 prefiring

import CMGRDF

from CMGRDF import *
from CMGRDF.collectionUtils import DefineMinMass, DefineSkimmedCollection, DefineP4

from data.trigger import trigger_definition_sequence
from utils.dataDriven import FlipRateDefine, FakeRateDefine
from utils.leptonScaleFactors  import leptonSFsequences
from utils.btagSF              import btagsf_sequences
from utils.jmesequences        import jme_sequences
from data.datasets import data_3l
import ROOT
ROOT.EnableImplicitMT(16)

from data.common_selections import lepton_selection, tau_selection, jet_selection_notau
eras  = ["2018"] # ["2016APV"]#, "2016"] # ["2018"]#, "2017", 

lumi = {
    '2016APV': 19.5,
    '2016': 16.8,
    '2017': 41.5,
    '2018': 59.7,
}





#verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(), ROOT.Experimental.ELogLevel.kInfo) 

ROOT.gInterpreter.Declare(open( "functions.cc").read())

                         
                         

cuts=[         AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter && Flag_ecalBadCalibFilter"),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               trigger_definition_sequence, 
               Cut("trigger", "Trigger_2lss"),
               lepton_selection,
               Cut("dilep", "nLepFO>=3"),
               Cut("cleanup", "minMllFO > 20"),
               Cut("pt251515", "(LepFO_conePt[0] > 25) && (LepFO_conePt[1] > 15) && (LepFO_conePt[2] > 15)"),
               Cut("TTT", "(LepFO_isTight[0]) && (LepFO_isTight[1]) && (LepFO_isTight[2])"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Define( "nMuons", "(abs(LepFO_pdgId[0]) == 13) +(abs(LepFO_pdgId[1]) == 13) +(abs(LepFO_pdgId[2]) == 13) "),
               jme_sequences,
               jet_selection_notau,
               Define( "NlooseLep", "Sum( LepGood_isLoose != 0)"), 
               Cut( "zpeak", "TMath::Abs( mZ1_FO - 91.16) < 15"),
               leptonSFsequences,
               btagsf_sequences,
               AddWeight("mcWeight", "puWeight*leptonSF_weight_2l*leptonSFreco_weight_2l*btagSF_fixedWP", onData=False, onDataDriven=False), # 
               
]

cuts_3l = Flow("SR_3l", cuts)


plots_3l = [
    #Plot("totWeight", "0", (1,-0.5,0.5)),
    #Plot("Lep2_pt", "LepFO_pt[1]", (15,10.,180.)),
    #Plot("btagSF_forPlot", "btagSF_forPlot", (50,0.5,1.5)),
    Plot("nBJetLoose25", "nBJetLoose25", (5,-0.5,4.5)),
    Plot("nBJetMedium25", "nBJetMedium25", (5,-0.5,4.5)),
    Plot("nJet25", "nJet25", (5,-0.5,4.5)),
    #Plot("nJet30", "nJet30", (5,-0.5,4.5)),
    Plot("PuppiMET_pt", "PuppiMET_pt", (50,0.,200.)),
]

maker = Processor()

maker.book(data_3l  , lumi, cuts_3l  , plots_3l  , eras=eras, withUncertainties=True)
results=maker.runPlots(debug=True)
PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True).printSet(results, "plots_1906/{flow}/{era}",  maxRatioRange=(0.5,1.5), showRatio=True) 


