# remove 2018 prefiring

import CMGRDF

from CMGRDF import *
from utils.dataDriven import FlipRateDefine, FakeRateDefine
from data.datasets import data_2lss, data_3l
import ROOT
ROOT.EnableImplicitMT()
from data.common_selections import lepton_selection, tau_selection, jet_selection
from data.trigger import trigger_definition_sequence

ROOT.gInterpreter.Declare(open( "functions.cc").read())

eras  = ["2018"]
addData=True
lumi = {'2018': 59.7}




zee_veto = '''
if (abs(LepFO_pdgId[0]) != 11) return true;
if (abs(LepFO_pdgId[1]) != 11) return true;
TLorentzVector l1, l2;
l1.SetPtEtaPhiM( LepFO_pt[0], LepFO_eta[0], LepFO_phi[0], 0.);
l2.SetPtEtaPhiM( LepFO_pt[1], LepFO_eta[1], LepFO_phi[1], 0.);
return ( abs( (l1+l2).M() - 91.1) > 10.)'''

cuts_2lss = Flow("SR_2lss",
                 trigger_definition_sequence,
                 lepton_selection,
                 AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
                 AddWeight("mcWeight", "leptonSF_2lss*triggerSF_2lss*btagSF*puWeight*L1PreFiringWeight_Nom", onData=False, onDataDriven=False),
                 Cut("trigger", "Trigger_2lss"),
                 Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter"),
                 Cut("filters_no2016", "Flag_ecalBadCalibFilter", eras=[2016]),
                 Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
                 Cut("cleanup", "minMllFO > 12"),
                 Cut("dilep", "nLepFO>=2"),
                 Cut("pt2515", " LepFO_conePt[0]>25 && LepFO_conePt[1]>15"),
                 Cut("TT", " LepFO_isTight[0] && LepFO_isTight[1]"),
                 Cut("exclusive", " nLepTight<=2"),
                 Cut("same-sign", "LepFO_charge[0]*LepFO_charge[1] > 0") ,
                 Define( "zee_veto", zee_veto),
                 Cut("Zee_veto", "zee_veto"),
                 Cut("metLDee", "abs(LepFO_pdgId[0])!=11 || abs(LepFO_pdgId[1]) != 11 || MET_pt > 30"),
                 Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
                 Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
                 Cut("Z_veto", "abs(mZ1_FO-91.2)>10"),
                 Cut("eleID", "(abs(LepFO_pdgId[0])!=11 || (LepFO_convVeto[0] && LepFO_lostHits[0]==0 && LepFO_tightCharge[0]>=2)) && (abs(LepFO_pdgId[1])!=11 || (LepFO_convVeto[1] && LepFO_lostHits[1]==0 && LepFO_tightCharge[1]>=2))"), 
                 Cut("muTightCharge", " (abs(LepFO_pdgId[0])!=13 || LepFO_tightCharge[1]>=1) && (abs(LepFO_pdgId[1])!=13 || LepFO_tightCharge[1]>=1)"),
                 tau_selection,
                 jet_selection,

                 
)

cuts_3l = Flow("SR_3l",
               trigger_definition_sequence,
               lepton_selection,
               tau_selection,
               jet_selection,
               AddWeight("prescaleFromSkim", "prescaleFromSkim", onData=True, onDataDriven=True),
               AddWeight("mcWeight", "leptonSF_3l*triggerSF_3l*btagSF*puWeight*L1PreFiringWeight_Nom", onData=False, onDataDriven=False),
               Cut("trilep", "nLepFO>=3"),
               Cut("exclusive", "nLepTight <= 3"),
               Cut("cleanup", "minMllFO > 12"),
               Cut("trigger", "Trigger_3l"),
               Cut("filters", "Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonDzFilter && Flag_BadPFMuonFilter"),
               Cut("filters_no2016", "Flag_ecalBadCalibFilter", eras=[2016]),
               Cut("filters_data", "Flag_eeBadScFilter", onMC=False),
               Cut("pt251515", " LepFO_conePt[0]>25  && LepFO_conePt[1]>15  && LepFO_conePt[2]>15"),
               Cut("TTT", " LepFO_isTight[0] && LepFO_isTight[1]  && LepFO_isTight[2]"),
               Define( "Z_candidate_pair", "bestPairByMass( allPairs(nLepFO), LepFO_p4, 91.2)"), # gets the indices of the pair closest to 91.2
               Define( "mZ1_FO", "(LepFO_p4[Z_candidate_pair.first]+LepFO_p4[Z_candidate_pair.second]).M()"),
               Cut("Z_veto", "abs(mZ1_FO-91.2)>10"),
               Cut("met LD", "nJet25 >= 4 || MET_pt > 30 + 15*(mZ1_FO > 0)"),
               Cut("q1", "abs(LepFO_charge[0]+LepFO_charge[1]+LepFO_charge[2]) == 1"),
               Cut("ele conv cuts", "(abs(LepFO_pdgId[0])!=11 || (LepFO_convVeto[0] && LepFO_lostHits[0]==0)) && (abs(LepFO_pdgId[1])!=11 || (LepFO_convVeto[1] && LepFO_lostHits[1]==0)) && (abs(LepFO_pdgId[2])!=11 || (LepFO_convVeto[2] && LepFO_lostHits[2]==0))"),
)

plots_2lss = [
    Plot("tot_weight", "1", (1,0.5,1.5), xTitle="One", showErrors=True),
    #Plot("nJet25", "nJet25", (8, -0.5, 7.5), xTitle="Number of jets (p_{T} > 40)", logy=True, moreY=10),
    #Plot("met", "MET_pt", (40, 0, 400), xTitle="p_{T}^{miss} (GeV)", logy=True, moreY=10),
    #Plot("lep_pdgId", "(abs( LepFO_pdgId[0])==13) + (abs( LepFO_pdgId[1])==13)" , (3, -0.5, 2.5), xTitle="Number of muons"),
    #Plot("one",  "print_evtno(event)", (1,0.5,1.5), xTitle="One"),
]

plots_3l = [
    Plot("tot_weight", "1", (1,0.5,1.5), xTitle="One"),
    Plot("nJet25", "nJet25", (8, -0.5, 7.5), xTitle="Number of jets (p_{T} > 40)", logy=True, moreY=10),
    Plot("met", "MET_pt", (40, 0, 400), xTitle="p_{T}^{miss} (GeV)", logy=True, moreY=10),
    Plot("lep_pdgId", "(abs( LepFO_pdgId[0])==13) + (abs( LepFO_pdgId[1])==13) + (abs( LepFO_pdgId[2])==13)" , (4, -0.5, 3.5), xTitle="Number of muons"),
    
]





maker = Processor()

maker.book(data_2lss, lumi, cuts_2lss, plots_2lss, eras=eras, withUncertainties=True)
#maker.book(data_3l  , lumi, cuts_3l  , plots_3l  , eras=eras, withUncertainties=True)
results=maker.runPlots()
PlotSetPrinter(topRightText="L = %(lumi).1f fb^{-1} (13 TeV)", showErrors=True).printSet(results, "plots_{flow}/{era}")
maker.printRawCutFlowReports()
