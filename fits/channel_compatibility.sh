# change directory - only argument is the directory with the cards
pushd $1
combineCards.py $(for what in `ls SR_*.txt`; do  echo -n ${what/.txt/}_=${what} \ ; done ) > combination.dat
python3 `git rev-parse --show-toplevel`/fits/postprocess_card.py  combination.dat true

text2workspace.py combination_nlo.dat -o wps.root -P HiggsAnalysis.CombinedLimit.SimpleEFTInterferenceModel:simpleEFTInterferenceModel
combineTool.py wps.root -M ChannelCompatibilityCheck --redefineSignalPOIs ctWI -n lin_fix_ctWI -g 2016_ -g 2016APV_ -g 2017_ -g 2018_ -g 2022_ -g 2022EE_
combineTool.py wps.root -M ChannelCompatibilityCheck --redefineSignalPOIs ctZI -n lin_fix_ctZI -g 2016_ -g 2016APV_ -g 2017_ -g 2018_ -g 2022_ -g 2022EE_

text2workspace.py combination_nlo.dat -o wps_quad.root -P HiggsAnalysis.CombinedLimit.SimpleEFTQuadraticModel:simpleEFTQuadraticModel
combineTool.py wps_quad.root -M ChannelCompatibilityCheck --redefineSignalPOIs ctWI -n quad_fix_ctWI -g 2016_ -g 2016APV_ -g 2017_ -g 2018_ -g 2022_ -g 2022EE_
combineTool.py wps_quad.root -M ChannelCompatibilityCheck --redefineSignalPOIs ctZI -n quad_fix_ctZI -g 2016_ -g 2016APV_ -g 2017_ -g 2018_ -g 2022_ -g 2022EE_




