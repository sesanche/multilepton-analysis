# change directory - only argument is the directory with the cards
pushd /afs/cern.ch/work/s/sesanche/private/CPV_code/combine/CMSSW_14_1_0_pre4/src
cmsenv
popd
pushd $1

# Combine cards 
combineCards.py SR_*.txt > combination.dat
python3 `git rev-parse --show-toplevel`/fits/postprocess_card.py combination.dat


# Produce workspaces 
text2workspace.py combination_nlo.dat -o wps.root -P HiggsAnalysis.CombinedLimit.SimpleEFTInterferenceModel:simpleEFTInterferenceModel
text2workspace.py combination_nlo.dat -o wps_quad.root -P HiggsAnalysis.CombinedLimit.SimpleEFTQuadraticModel:simpleEFTQuadraticModel

# Linear fits (fixed) with and without uncertainties - to update in unblinded
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n lin_fix_ctWI --setParameterRanges ctWI=-5,5 -t -1
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n lin_fix_ctZI --setParameterRanges ctZI=-5,5 -t -1 
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n lin_fix_ctWI_nosyst --setParameterRanges ctWI=-5,5 -t -1  --freezeParameters allConstrainedNuisances
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n lin_fix_ctZI_nosyst --setParameterRanges ctZI=-5,5 -t -1  --freezeParameters allConstrainedNuisances
plot1DScan.py higgsCombinelin_fix_ctWI.MultiDimFit.mH120.root --POI ctWI -o scan_lin_fix_ctWI --other "higgsCombinelin_fix_ctWI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"
plot1DScan.py higgsCombinelin_fix_ctZI.MultiDimFit.mH120.root --POI ctZI -o scan_lin_fix_ctZI --other "higgsCombinelin_fix_ctZI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"

# Linear fits (profiled) with and without uncertainties - to update in unblinded
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n lin_prof_ctWI --setParameterRanges ctWI=-5,5 -t -1 --floatOtherPOIs 1 
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n lin_prof_ctZI --setParameterRanges ctZI=-5,5 -t -1 --floatOtherPOIs 1 
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n lin_prof_ctWI_nosyst --setParameterRanges ctWI=-5,5 -t -1 --floatOtherPOIs 1 --freezeParameters allConstrainedNuisances
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n lin_prof_ctZI_nosyst --setParameterRanges ctZI=-5,5 -t -1 --floatOtherPOIs 1 --freezeParameters allConstrainedNuisances
plot1DScan.py higgsCombinelin_prof_ctWI.MultiDimFit.mH120.root --POI ctWI -o scan_lin_prof_ctWI --other "higgsCombinelin_prof_ctWI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"
plot1DScan.py higgsCombinelin_prof_ctZI.MultiDimFit.mH120.root --POI ctZI -o scan_lin_prof_ctZI --other "higgsCombinelin_prof_ctZI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"


# Quadratic fits (fixed) with and without uncertainties - to update in unblinded
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n quad_fix_ctWI --setParameterRanges ctWI=-5,5 -t -1
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n quad_fix_ctZI --setParameterRanges ctZI=-5,5 -t -1 
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n quad_fix_ctWI_nosyst --setParameterRanges ctWI=-5,5 -t -1  --freezeParameters allConstrainedNuisances
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n quad_fix_ctZI_nosyst --setParameterRanges ctZI=-5,5 -t -1  --freezeParameters allConstrainedNuisances
plot1DScan.py higgsCombinequad_fix_ctWI.MultiDimFit.mH120.root --POI ctWI -o scan_quad_fix_ctWI --other "higgsCombinequad_fix_ctWI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"
plot1DScan.py higgsCombinequad_fix_ctZI.MultiDimFit.mH120.root --POI ctZI -o scan_quad_fix_ctZI --other "higgsCombinequad_fix_ctZI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"

# Quadratic fits (profiled) with and without uncertainties - to update in unblinded
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n quad_prof_ctWI --setParameterRanges ctWI=-5,5 -t -1 --floatOtherPOIs 1 
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n quad_prof_ctZI --setParameterRanges ctZI=-5,5 -t -1 --floatOtherPOIs 1 
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n quad_prof_ctWI_nosyst --setParameterRanges ctWI=-5,5 -t -1 --floatOtherPOIs 1 --freezeParameters allConstrainedNuisances
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n quad_prof_ctZI_nosyst --setParameterRanges ctZI=-5,5 -t -1 --floatOtherPOIs 1 --freezeParameters allConstrainedNuisances
plot1DScan.py higgsCombinequad_prof_ctWI.MultiDimFit.mH120.root --POI ctWI -o scan_quad_prof_ctWI --other "higgsCombinequad_prof_ctWI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"
plot1DScan.py higgsCombinequad_prof_ctZI.MultiDimFit.mH120.root --POI ctZI -o scan_quad_prof_ctZI --other "higgsCombinequad_prof_ctZI_nosyst.MultiDimFit.mH120.root:No syst:2" --main-label "Systematics"


# FitDiagnostics for plots 
combineTool.py wps.root -M FitDiagnostics  -n asimov_sm  -t -1 --setParameters ctWI=0,ctZI=0 --saveShapes --saveWithUncertainties

combineTool.py wps.root -M FitDiagnostics  -n asimov_ctWI_2  -t -1 --setParameters ctWI=2,ctZI=0 --saveShapes
combineTool.py wps.root -M FitDiagnostics  -n asimov_ctZI_2  -t -1 --setParameters ctWI=0,ctZI=2 --saveShapes
combineTool.py wps_quad.root -M FitDiagnostics  -n asimov_quad_ctWI_2  -t -1 --setParameters ctWI=2,ctZI=0 --saveShapes
combineTool.py wps_quad.root -M FitDiagnostics  -n asimov_quad_ctZI_2  -t -1 --setParameters ctWI=0,ctZI=2 --saveShapes

combineTool.py wps.root -M FitDiagnostics  -n asimov_ctWI_1  -t -1 --setParameters ctWI=1,ctZI=0 --saveShapes
combineTool.py wps.root -M FitDiagnostics  -n asimov_ctZI_1  -t -1 --setParameters ctWI=0,ctZI=1 --saveShapes
combineTool.py wps_quad.root -M FitDiagnostics  -n asimov_quad_ctWI_1  -t -1 --setParameters ctWI=1,ctZI=0 --saveShapes
combineTool.py wps_quad.root -M FitDiagnostics  -n asimov_quad_ctZI_1  -t -1 --setParameters ctWI=0,ctZI=1 --saveShapes


combineTool.py wps.root      -M MultiDimFit --algo grid -P ctWI -P ctZI -n lin_ctZI_ctWI  --setParameterRanges ctWI=-5,5:ctZI=-6,6 -t -1 --points 5000
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -P ctZI -n quad_ctZI_ctWI --setParameterRanges ctWI=-5,5:ctZI=-6,6 -t -1 --points 5000

python3 ../fits/plot_likelihood_2d.py higgsCombinelin_ctZI_ctWI.MultiDimFit.mH120.root plot_lin_ctZI_ctWI -6 2.5 -1 11 20
python3 ../fits/plot_likelihood_2d.py higgsCombinequad_ctZI_ctWI.MultiDimFit.mH120.root plot_quad_ctZI_ctWI -4 4 -2 3 40

popd 
