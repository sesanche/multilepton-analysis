cards=$1
what=$2 # run or collect

pushd $cards

if [ "$what" == "run" ]; then
    for op in `seq -4.0 0.25 4.0`; do
        #combineTool.py -M MultiDimFit wps.root      --algo fixed -P ctZI --redefineSignalPOIs ctZI --fixedPointPOIs ctZI=${op},ctWI=0 -t 5000 -s 1000:1009:1 --setParameters ctZI=${op},ctWI=0 --toysFrequentist    -n neyman_toys_lin_ctZI_${op}   --job-mode condor --task-name neyman_toys_lin_ctZI_${op}  --sub-opts='+JobFlavour="workday"' --freezeParameters ctWI
        combineTool.py -M MultiDimFit wps_quad.root --algo fixed -P ctZI --redefineSignalPOIs ctZI --fixedPointPOIs ctZI=${op},ctWI=0 -t 5000 -s 1000:1009:1 --setParameters ctZI=${op},ctWI=0 --toysFrequentist    -n neyman_toys_quad_ctZI_${op}  --job-mode condor --task-name neyman_toys_quad_ctZI_${op} --sub-opts='+JobFlavour="workday"' --freezeParameters ctWI
        #combineTool.py -M MultiDimFit wps.root      --algo fixed -P ctWI --redefineSignalPOIs ctWI --fixedPointPOIs ctZI=0,ctWI=${op} -t 5000 -s 1000:1009:1 --setParameters ctZI=0,ctWI=${op} --toysFrequentist    -n neyman_toys_lin_ctWI_${op}   --job-mode condor --task-name neyman_toys_lin_ctWI_${op}  --sub-opts='+JobFlavour="workday"' --freezeParameters ctZI
        combineTool.py -M MultiDimFit wps_quad.root --algo fixed -P ctWI --redefineSignalPOIs ctWI --fixedPointPOIs ctZI=0,ctWI=${op} -t 5000 -s 1000:1019:1 --setParameters ctZI=0,ctWI=${op} --toysFrequentist    -n neyman_toys_quad_ctWI_${op}  --job-mode condor --task-name neyman_toys_quad_ctWI_${op} --sub-opts='+JobFlavour="workday"' --freezeParameters ctZI
    done
    
elif [ "$what" == "collect" ]; then
    for op in `seq -4.0 0.25 4.0`; do

        for item in  higgsCombineneyman_toys_quad_ctWI_ higgsCombineneyman_toys_quad_ctZI_; do # higgsCombineneyman_toys_lin_ctZI_  higgsCombineneyman_toys_lin_ctWI_
            hadd ${item}${op}.MultiDimFit.mH120.merged.root ${item}${op}.MultiDimFit.mH120.100*.root
        done
        
        #python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_lin_ctZI_${op}.MultiDimFit.mH120.merged.root  > criterion_lin_ctZI_${op}.txt
        python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_quad_ctZI_${op}.MultiDimFit.mH120.merged.root > criterion_quad_ctZI_${op}.txt
        #python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_lin_ctWI_${op}.MultiDimFit.mH120.merged.root  > criterion_lin_ctWI_${op}.txt
        python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_quad_ctWI_${op}.MultiDimFit.mH120.merged.root > criterion_quad_ctWI_${op}.txt

        #python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_lin_ctZI_${op}.MultiDimFit.mH120.merged.root  > criterion_2sig_lin_ctZI_${op}.txt
        python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_quad_ctZI_${op}.MultiDimFit.mH120.merged.root > criterion_2sig_quad_ctZI_${op}.txt
        #python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_lin_ctWI_${op}.MultiDimFit.mH120.merged.root  > criterion_2sig_lin_ctWI_${op}.txt
        python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_quad_ctWI_${op}.MultiDimFit.mH120.merged.root > criterion_2sig_quad_ctWI_${op}.txt

    done
fi
