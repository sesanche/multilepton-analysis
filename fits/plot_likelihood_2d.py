import ROOT 
import numpy as np 
import math 
import sys

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)
import cmsstyle as CMS

def make_2d_graph( filein ) :
    tf=ROOT.TFile.Open( filein )
    limit=tf.Get("limit") 
    npoints=limit.GetEntries()
    gr=ROOT.TGraph2D(npoints)
    for i,(event) in enumerate(limit):
        gr.SetPoint(i,event.ctWI,event.ctZI,2*event.deltaNLL)
    gr.SetDirectory(0)
    tf.Close()
    return gr 



print(sys.argv)
gr_eq       =make_2d_graph( sys.argv[1] )
xmin,xmax,ymin,ymax,zmax=sys.argv[3:8]
toplot=[]
limits=[(2.3, 1), (5.99,3)]


gr_eq.SetNpx(400)
gr_eq.SetNpy(400)
gr_eq.Draw()
histo=gr_eq.GetHistogram()

import numpy as np

for  limit, s in limits:
    histo_cont=histo.Clone()
    histo_cont.SetContour(1, np.array([limit]))
    histo_cont.SetLineColor(ROOT.kBlack)
    histo_cont.SetLineWidth(2)
    histo_cont.SetLineStyle( s ) 
    toplot.append( histo_cont ) 

CMS.SetEnergy("")
CMS.ResetAdditionalInfo()
CMS.SetLumi("140 fb^{-1} (13 TeV) + 35 fb^{-1} (13.6 TeV)",unit="")

canv = CMS.cmsCanvas('', float(xmin), float(xmax), float(ymin), float(ymax), 'c_{tW}^{I}', 'c_{tZ}^{I}', square = CMS.kSquare, with_z_axis=True, iPos=0,scaleLumi=0.65, yTitOffset=1.)
canv.Draw()

CMS.SetAlternative2DColor( histo )

histo.Draw("colz,same")
histo.GetZaxis().SetRangeUser(0,float(zmax))
histo.GetZaxis().SetTitle("-2#Delta NLL")
for p in toplot:
    p.Draw("cont1,same")
    p.SetLineColor(ROOT.kRed)

sm_gr = ROOT.TGraph(1)
sm_gr.SetMarkerStyle(47)
sm_gr.SetMarkerSize(2)
sm_gr.SetPoint(0,0,0)
sm_gr.Draw("P,same")
canv.Update()
canv.RedrawAxis()

leg=ROOT.TLegend( 0.15, 0.15, 0.36, 0.28)
leg.SetTextSize(0.04 )
leg.AddEntry( toplot[0], "68% CL", "L")
leg.AddEntry( toplot[1], "95% CL", "L")
leg.AddEntry( sm_gr, "SM", "P")
leg.SetFillColor(ROOT.kWhite)
leg.Draw("same") 
    
canv.SaveAs(f"{sys.argv[2]}.pdf") 
        

