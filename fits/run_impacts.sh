cards=$1
what=$2 # run or collect

pushd $cards

#asimov="-t -1"
#prefix="blind"
asimov=""
prefix="unblind"

#asimov="-t -1 --setParameters ctWI=2.0,ctZI=0"
#prefix="ctWI_2p0"

#asimov="-t -1 --setParameters ctWI=0.0,ctZI=2.0"
#prefix="ctZI_2p0"


if [ "$what" == "run" ]; then
    for op in ctWI ctZI; do
        for wps in wps wps_quad; do
            combineTool.py -M Impacts -d ${wps}.root --doInitialFit --robustFit 1  $asimov -m 1 -n ${prefix}_${op}_${wps}  --redefineSignalPOIs ${op}
            combineTool.py -M Impacts -d ${wps}.root --doFits       --robustFit 1  $asimov -m 1 -n ${prefix}_${op}_${wps}  --redefineSignalPOIs ${op} --task-name impacts_${prefix}_${op}_${wps}  --job-mode condor --sub-opts='+MaxRuntime=1000 \n +AccountingGroup = "group_u_CMST3.all"' # --parallel 16  #
        done
    done
            
        
elif [ "$what" == "collect" ]; then
    for op in ctWI ctZI; do
        for wps in wps wps_quad; do
            combineTool.py -M Impacts -d ${wps}.root -o impacts_${prefix}_${op}_${wps}.json $asimov -m 1 -n ${prefix}_${op}_${wps} --redefineSignalPOIs ${op}
            plotImpacts.py -i impacts_${prefix}_${op}_${wps}.json -o impacts_${prefix}_${op}_${wps}
        done
    done
fi
