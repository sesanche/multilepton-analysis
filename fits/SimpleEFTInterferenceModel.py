from HiggsAnalysis.CombinedLimit.PhysicsModel import *
from HiggsAnalysis.CombinedLimit.SMHiggsBuilder import SMHiggsBuilder
import ROOT
import os
import json
import re 

class SimpleEFTInterferenceModel(PhysicsModel):
    def __init__( self):
        PhysicsModel.__init__(self)
        self.signals=[ "TTLL_CPV", "TZQ"]
        self.operators=[ "ctWI", "ctZI", "ctGI"]

    def setPhysicsOptions(self,physOptions):
        pass

    def doParametersOfInterest(self):
        self.modelBuilder.doVar("r[1,-10,10]")
        self.poiNames = "r,"+(",".join(self.operators))

        for op in self.operators:
            print( f"expr::func_{op}_1( @0/2., {op})" ) 
            self.modelBuilder.doVar( op + "[0,-20,20]")
            self.modelBuilder.factory_( f"expr::func_{op}_1(\"@0/2.\", {op})")
            self.modelBuilder.factory_( f"expr::func_{op}_m1(\"-@0/2.\", {op})") 
        self.modelBuilder.doSet("POI",self.poiNames)
        
    def getYieldScale(self,bin,process):
        for x in self.signals:
            if not process.startswith(x): continue
            if 'sm' in process:
                print(f"NLO SM already in the cards!!!! Removing {process}") 
                return 0
            suffix=process.replace(x+"_","")
            if len( suffix.split("_")) == 4:
                print("This is a crossed term, ignoring")
                return 0
            
            op=suffix.split("_")[0]
            if op not in self.operators:
                print(process)
                raise RuntimeError(f"Operator {op} not found")
            if process.endswith("_m1p0"):
                return f"func_{op}_m1"
            elif process.endswith("_1p0"):
                return f"func_{op}_1"
            else:
                raise RuntimeError("I dont know what to do with {process}")
            
        print(f"Process {process} is not a signal") 
        return 1
        

simpleEFTInterferenceModel= SimpleEFTInterferenceModel()
