from HiggsAnalysis.CombinedLimit.PhysicsModel import *
from HiggsAnalysis.CombinedLimit.SMHiggsBuilder import SMHiggsBuilder
import ROOT
import os
import json
import re 

class SimpleEFTQuadraticModel(PhysicsModel):
    def __init__( self):
        PhysicsModel.__init__(self)
        self.signals=[ "TTLL_CPV", "TZQ"]
        self.operators=[ "ctWI", "ctZI" ]
        print("This is hardcoded to two operators") 

    def setPhysicsOptions(self,physOptions):
        pass

    def doParametersOfInterest(self):
        self.modelBuilder.doVar("r[1,-10,10]")
        self.poiNames = "r,"+(",".join(self.operators))
        
        for op in self.operators:
            self.modelBuilder.doVar( op + "[0,-20,20]")

        self.modelBuilder.factory_( f"expr::func_sm_tzq(\"1-@0*@0-@1*@1+@0*@1\", ctZI, ctWI)")
        self.modelBuilder.factory_( f"expr::func_sm(\"-@0*@0-@1*@1+@0*@1\", ctZI, ctWI)") # normally it would be +1, but here we have the SM from the NLO samples, that are used as background
        self.modelBuilder.factory_( f"expr::func_ctZI_1p0(\"@0/2+@0*@0/2-@0*@1\", ctZI, ctWI)")
        self.modelBuilder.factory_( f"expr::func_ctWI_1p0(\"@0/2+@0*@0/2-@0*@1\", ctWI, ctZI)")
        self.modelBuilder.factory_( f"expr::func_ctZI_m1p0(\"-@0/2+@0*@0/2\", ctZI)")
        self.modelBuilder.factory_( f"expr::func_ctWI_m1p0(\"-@0/2+@0*@0/2\", ctWI)")
        self.modelBuilder.factory_( f"expr::func_ctWI_1p0_ctZI_1p0(\"@0*@1\", ctWI, ctZI)")
            
        self.modelBuilder.doSet("POI",self.poiNames)
        
    def getYieldScale(self,bin,process):
        if process == 'tZq':
            return 'func_sm_tzq'
        
        if process.startswith("TTLL"):
            suffix=process.split("_CPV_")[1]
            return f'func_{suffix}'
        elif process.startswith("TZQ"):
            suffix=process.split("TZQ_")[1]
            return f'func_{suffix}'
        else:
            print(f"Process {process} is not a signal") 
            return 1
        

simpleEFTQuadraticModel= SimpleEFTQuadraticModel()
