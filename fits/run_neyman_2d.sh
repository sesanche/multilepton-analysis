cards=$1
what=$2 # run or collect

pushd $cards

if [ "$what" == "run" ]; then
    for ctWI in `seq -3.0 0.25 3.0`; do
        for ctZI in `seq -3.0 0.25 3.0`; do
            combineTool.py -M MultiDimFit wps.root      --algo fixed -P ctZI -P ctWI --redefineSignalPOIs ctZI,ctWI --fixedPointPOIs ctZI=${ctZI},ctWI=${ctWI} -t 50000 --setParameters ctZI=${ctZI},ctWI=${ctWI} --toysFrequentist    -n neyman_toys_lin_ctZI_${ctZI}_ctWI_${ctWI}   --job-mode condor --task-name neyman_toys_lin_ctZI_${ctZI}_ctWI_${ctWI}  --sub-opts='+JobFlavour="workday"' 
            combineTool.py -M MultiDimFit wps_quad.root --algo fixed -P ctZI -P ctWI --redefineSignalPOIs ctZI,ctWI --fixedPointPOIs ctZI=${ctZI},ctWI=${ctWI} -t 50000 --setParameters ctZI=${ctZI},ctWI=${ctWI} --toysFrequentist    -n neyman_toys_quad_ctZI_${ctZI}_ctWI_${ctWI}  --job-mode condor --task-name neyman_toys_quad_ctZI_${ctZI}_ctWI_${ctWI} --sub-opts='+JobFlavour="workday"'
        done
    done
elif [ "$what" == "collect" ]; then
    for ctWI in `seq -3.0 0.25 3.0`; do
        for ctZI in `seq -3.0 0.25 3.0`; do
            
            python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_lin_ctZI_${ctZI}_ctWI_${ctWI}.MultiDimFit.mH120.123456.root  > criterion_2d_1sig_lin_ctZI_${ctZI}_ctWI_${ctWI}.txt
            python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --input higgsCombineneyman_toys_quad_ctZI_${ctZI}_ctWI_${ctWI}.MultiDimFit.mH120.123456.root  > criterion_2d_1sig_quad_ctZI_${ctZI}_ctWI_${ctWI}.txt

            python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_lin_ctZI_${ctZI}_ctWI_${ctWI}.MultiDimFit.mH120.123456.root > criterion_2d_2sig_lin_ctZI_${ctZI}_ctWI_${ctWI}.txt
            python3 `git rev-parse --show-toplevel`/fits/get_quantile.py --n-sigma 2 --input higgsCombineneyman_toys_quad_ctZI_${ctZI}_ctWI_${ctWI}.MultiDimFit.mH120.123456.root > criterion_2d_2sig_quad_ctZI_${ctZI}_ctWI_${ctWI}.txt

        done
    done
fi
