import os
import ROOT 
import ROOT as r 
import cmsstyle as CMS
from array import array

r.gROOT.SetBatch(True)
import sys
import uuid


r.gStyle.SetErrorX(0)

opvalue=2
fil_lin           =sys.argv[1] + "/" +  "fitDiagnosticsunblind_bestfit.root"
fil_quad          =sys.argv[1] + "/" +  "fitDiagnosticsunblind_bestfit_quad.root"

fit="fit_s"



process_groups = {
    "Conversions" : ["Conversions"],
    "Diboson"     : ["Diboson"],
    "Non-prompt"  : ["Fakes"],
    "Rares"       : ["Rares"],
    "t#bar{t}Z"   : ["TTLL_CPV_ctWI_1p0","TTLL_CPV_ctWI_m1p0","TTLL_CPV_ctZI_1p0", "TTLL_CPV_ctZI_m1p0", "TTZ", "TTLL_CPV_ctWI_1p0_ctZI_1p0", "TTLL_CPV_sm"],
    "tZq"         : ["TZQ_ctWI_1p0", "TZQ_ctWI_m1p0","TZQ_ctZI_1p0", "TZQ_ctZI_m1p0", "tZq", "TZQ_ctWI_1p0_ctZI_1p0"],
}

colors = { "Rares"       : r.TColor.GetColor("#3f90da"),
           "t#bar{t}Z"         : r.TColor.GetColor("#ffa90e"),
           "tZq"         : r.TColor.GetColor("#bd1f01"), 
           "Diboson"     : r.TColor.GetColor("#94a4a2"),
           "Non-prompt"       : r.TColor.GetColor("#832db6"), 
           "Conversions" : r.TColor.GetColor("#a96b59"),

}

other_colors = [r.TColor.GetColor(x) for x in [ "#e76300", "#b9ac70", "#717581", "#92dadd"]]

iPos = 0 
CMS.SetEnergy("")
CMS.ResetAdditionalInfo()
CMS.SetLumi("140 fb^{-1} (13 TeV) + 35 fb^{-1} (13.6 TeV)",unit="")

def transformHisto( histo ):
    if histo.InheritsFrom("TH1"):
        bins = [ histo.GetBinLowEdge(i+1) for i in range(histo.GetNbinsX())] + [ histo.GetBinLowEdge( histo.GetNbinsX() ) + histo.GetBinWidth( histo.GetNbinsX() )]
        bins_trans  = [x-5 for x in bins]
        histo_trans = ROOT.TH1F(histo.GetName() + "_trans", "", histo.GetNbinsX(), array('f', bins_trans))
        for x in range(histo.GetNbinsX()):
            histo_trans.SetBinContent( x+1, histo.GetBinContent(x+1))
            histo_trans.SetBinError  ( x+1, histo.GetBinError(x+1))
    else:
        histo_trans = histo.Clone(histo.GetName() + "_trans" + uuid.uuid4().hex)
        for i in range(histo_trans.GetN()):
            histo_trans.SetPointX(i, histo.GetPointX(i)-5)
    return histo_trans


def get_stack_total_data_from( fil, fit, channel ):
    tf=r.TFile.Open( fil )
    fit_shapes=tf.Get(f"shapes_{fit}/{channel}")
    total=fit_shapes.Get("total").Clone( "total"+channel ) 
    data=fit_shapes.Get("data").Clone( "data"+channel ) 
    stack={}

    collected=[]
    for proc in process_groups:
        for samp in process_groups[proc]:
            collected.append( samp )
            if not fit_shapes.Get( samp ): continue
            if proc not in stack:
                stack[proc]=fit_shapes.Get( samp ).Clone( proc + fit + channel )
                stack[proc].SetFillColor( colors[proc] )
                stack[proc].SetLineColor( colors[proc] )
                if proc == "Non-prompt":
                    stack[proc].SetFillStyle(3005)
            else:
                stack[proc].Add( fit_shapes.Get( samp ) )
    total.SetDirectory(0)
    for kk in fit_shapes.GetListOfKeys():
        if kk.GetName() not in collected:
            print( f"We missed {kk.GetName()}")

    #data.SetDirectory(0)
    for x in stack: stack[x].SetDirectory(0)
    tf.Close()
    for x in stack: stack[x] = transformHisto(stack[x])
    
    return transformHisto(total), transformHisto(data), stack




plots = {
    'ch1' : {
        "rangex" : (-5,5),
        'rangey' : (0.,320),
        'label' : "Discretized c_{tW}^{I} score"
    },
    'ch2' : {
        "rangex" : (-5,5),
        'rangey' : (0.,600),
        'label' : "Discretized c_{tZ}^{I} score"
    },
}


for ch in plots:
    total, data, hist_dict=get_stack_total_data_from( fil_lin, fit, ch)
    for k in range(total.GetNbinsX()):
        print(total.GetBinContent(k+1), total.GetBinLowEdge(k+1))
    prefit, _, _=get_stack_total_data_from( fil_lin, 'prefit', ch)
    total_quad, _, _=get_stack_total_data_from( fil_quad, fit, ch)

    # plot with ratio
    dicanv = CMS.cmsDiCanvas(f"canvas_ratio_{ch}",plots[ch]['rangex'][0],plots[ch]['rangex'][1],plots[ch]['rangey'][0],plots[ch]['rangey'][1], 0.2,1.8, plots[ch]['label'],"Events","x/prefit", square=CMS.kSquare,extraSpace=0.1,iPos=iPos)
    stack2 = ROOT.THStack("stack2", "Stacked2")
    dicanv.cd(1)
    leg = CMS.cmsLeg(0.175, 0.89 - 0.05 * 9, 0.44, 0.89, textSize=0.05)
    CMS.cmsDrawStack(stack2, leg, hist_dict, data=None)

    CMS.cmsDraw(total, "e2same0", lcolor = 335, lwidth = 1, msize = 0, fcolor = ROOT.kBlack, fstyle = 3004,)
    data.SetMarkerStyle(ROOT.kFullCircle)
    data.Draw("P,same")
    for i in range(data.GetN()):
        data.SetPointEXhigh(i,0);
        data.SetPointEXlow(i,0);

    leg.AddEntry( data, "Data", "PE")
    leg.Draw("same")
    
    dicanv.cd(2)

    denominator = prefit.Clone("denominator")
    for i in range(denominator.GetNbinsX()):
        denominator.SetBinError(i+1,0)


    div=data.Clone()

    for i in range(div.GetN()):
        x = div.GetX()[i]
        den = denominator.GetBinContent( denominator.FindBin(x) )
        #if den == 0: continue
        div.SetPoint(i, x, div.GetY()[i] / den )
        div.SetPointError( i, div.GetErrorXlow(i), div.GetErrorXhigh(i),
                           div.GetErrorYlow(i)/den if den > 0 else 0,
                           div.GetErrorYhigh(i)/den if den > 0 else 0)

    total_over_prefit = total.Clone( "total_over_prefit")
    total_quad_over_prefit = total_quad.Clone( "total_quad_over_prefit")
    total_over_prefit.Divide(denominator)
    total_over_prefit_line=total_over_prefit.Clone()
    total_quad_over_prefit.Divide(denominator)
    total_quad_over_prefit_line=total_quad_over_prefit.Clone()

    prefit.Divide(denominator)
    prefit_line=prefit.Clone()
    
    for what in [total_over_prefit_line, prefit_line, total_quad_over_prefit_line]:
        for i in range(what.GetNbinsX()):
            what.SetBinError(i+1,0.)


    CMS.cmsDraw(total_quad_over_prefit, "e2same0", lcolor = ROOT.kGreen, lwidth = 0, msize = 0, fcolor = ROOT.kGreen, fstyle = 1001, alpha=0.2)
    CMS.cmsDraw(total_quad_over_prefit_line, "hist,same", lcolor = ROOT.kGreen, lwidth = 2, msize = 0, fcolor = ROOT.kWhite)

    CMS.cmsDraw(total_over_prefit, "e2same0", lcolor = ROOT.kBlue, lwidth = 0, msize = 0, fcolor = ROOT.kBlue, fstyle = 3005,)
    CMS.cmsDraw(total_over_prefit_line, "hist,same", lcolor = ROOT.kBlue, lwidth = 2, msize = 0, fcolor = ROOT.kWhite)

    CMS.cmsDraw(prefit, "e2same0", lcolor = ROOT.kRed, lwidth = 0, msize = 0, fcolor = ROOT.kRed, fstyle = 3004,)
    for i in range(prefit_line.GetNbinsX()):
        prefit_line.SetBinError(i+1,0)
    CMS.cmsDraw(prefit_line, "hist,same", lcolor = ROOT.kRed, lwidth = 2, msize = 0, fcolor = ROOT.kWhite)
    div.Draw("P,same") 

    leg2 = CMS.cmsLeg(0.15, 0.35, 0.92, 0.45, textSize=0.1)
    leg2.AddEntry( div, "Data", "PE")
    leg2.AddEntry( prefit, "Prefit", "lf")
    leg2.AddEntry( total_over_prefit, "Postfit (lin.)", "lf")
    leg2.AddEntry( total_quad_over_prefit, "Postfit (lin.+quad.)", "lf")
    leg2.SetNColumns(4)

    dicanv.SaveAs( f"{ch}_ratio.png") 
    dicanv.SaveAs( f"{ch}_ratio.pdf") 
    
