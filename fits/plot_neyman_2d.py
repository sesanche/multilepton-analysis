import ROOT 
import numpy as np 
import math 
import sys
import glob 
import re
import numpy as np 
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)
import cmsstyle as CMS


for what in [ 'quad', 'lin']:
    for nsig in [2,1]:
        points=[]
        pattern=re.compile( f"criterion_2d_{nsig}sig_{what}_ctZI_(?P<ctZI>.*)_ctWI_(?P<ctWI>.*).txt" )
        for point in glob.glob( f'{sys.argv[1]}/criterion_2d_{nsig}sig_{what}_ctZI_*_ctWI_*.txt'):
            match = pattern.search( point.split("/")[-1])
            if not match:
                raise RuntimeError
            ctWI=float(match.group( 'ctWI' ) )
            ctZI=float(match.group( 'ctZI' ) )
            with open( point ) as f:
                q=float(f.read().rstrip().replace(f"This point is rejected at the {nsig} sigma level if the test stat -2*deltaNLL  >", ""))
            if np.isnan(q):
                continue
            wilksq={ 1 : 2.3, 2 : 5.99}[nsig]
            points.append( (ctWI, ctZI, q-wilksq) )

    
        gr=ROOT.TGraph2D( len(points) )
        for i, (x,y,z) in enumerate(points):
            gr.SetPoint(i,x,y,z)
    
        canv = CMS.cmsCanvas('', -3, 3, -3, 3, 'c_{tW}^{I}', 'c_{tZ}^{I}', square = CMS.kSquare, with_z_axis=True)
        canv.Draw()
    
    
        
    
        iPos = 0 
        CMS.SetEnergy("")
        CMS.ResetAdditionalInfo()
        CMS.SetLumi("140 fb^{-1} (13 TeV) + 35 fb^{-1} (13.6 TeV)",unit="")
    
    
    
        gr.SetNpx(400)
        gr.SetNpy(400)
        gr.Draw('colz,same')
    
        canv.SaveAs(f"{sys.argv[1]}/qstat_{what}_withtoys_nsig_{nsig}.pdf")
        canv.SaveAs(f"{sys.argv[1]}/qstat_{what}_withtoys_nsig_{nsig}.png") 
    
        #CMS.SetAlternative2DColor( histo )
        #histo.Draw("colz,same") 
        
            

