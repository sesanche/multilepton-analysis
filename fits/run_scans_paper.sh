# change directory - only argument is the directory with the cards
pushd /afs/cern.ch/work/s/sesanche/private/CPV_code/combine/CMSSW_14_1_0_pre4/src
cmsenv
popd
pushd $1

for op in ctWI ctZI; do
    python3 ../fits/plot1DScan.py higgsCombinelin_fix_${op}.MultiDimFit.mH120.root --POI ${op} -o scan_lin_fix_prof_${op} --main-label "Fixed" --other "higgsCombinelin_prof_${op}.MultiDimFit.mH120.root:Profiled:2" --translate ../fits/translate.json --y-max 10 --y-cut 8 --logo-sub "  Preliminary"
    python3 ../fits/plot1DScan.py higgsCombinequad_fix_${op}.MultiDimFit.mH120.root --POI ${op} -o scan_quad_fix_prof_${op} --main-label "Fixed" --other "higgsCombinequad_prof_${op}.MultiDimFit.mH120.root:Profiled:2" --translate ../fits/translate.json --crossing1sigma quad_${op}_1sig  --crossing2sigma quad_${op}_2sig --y-max 10 --y-cut 8 --logo-sub "  Preliminary"
    
    python3 ../fits/plot1DScan.py higgsCombineunblind_lin_fix_${op}.MultiDimFit.mH120.root --POI ${op} -o scan_unblind_lin_fix_prof_${op} --main-label "Fixed" --other "higgsCombineunblind_lin_prof_${op}.MultiDimFit.mH120.root:Profiled:2" --translate ../fits/translate.json --y-max 10 --y-cut 8 --logo-sub "  Preliminary"
    python3 ../fits/plot1DScan.py  higgsCombineunblind_quad_fix_${op}.MultiDimFit.mH120.root --POI ${op} -o scan_unblind_quad_fix_prof_${op} --main-label "Fixed" --other "higgsCombineunblind_quad_prof_${op}.MultiDimFit.mH120.root:Profiled:2" --translate ../fits/translate.json --crossing1sigma quad_${op}_1sig  --crossing2sigma quad_${op}_2sig --y-max 10 --y-cut 8 --logo-sub "  Preliminary"
    
done
