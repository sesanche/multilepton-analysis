import os
import ROOT 
import ROOT as r 
import cmsstyle as CMS
r.gROOT.SetBatch(True)
import sys
from array import array
import uuid




opvalue=1
fil          =sys.argv[1] + "/" +  "fitDiagnosticsasimov_sm.root"
fil_ctWI     =sys.argv[1] + "/" + f"fitDiagnosticsasimov_ctWI_{opvalue}.root"
fil_ctZI     =sys.argv[1] + "/" + f"fitDiagnosticsasimov_ctZI_{opvalue}.root"
fil_ctWI_quad=sys.argv[1] + "/" + f"fitDiagnosticsasimov_quad_ctWI_{opvalue}.root"
fil_ctZI_quad=sys.argv[1] + "/" + f"fitDiagnosticsasimov_quad_ctZI_{opvalue}.root"




process_groups = {
    "Conversions" : ["Conversions"],
    "Diboson"     : ["Diboson"],
    "Non-prompt"  : ["Fakes"],
    "Rares"       : ["Rares"],
    "t#bar{t}Z"   : ["TTLL_CPV_ctWI_1p0","TTLL_CPV_ctWI_m1p0","TTLL_CPV_ctZI_1p0", "TTLL_CPV_ctZI_m1p0", "TTZ", "TTLL_CPV_sm"],
    "tZq"         : ["TZQ_ctWI_1p0", "TZQ_ctWI_m1p0","TZQ_ctZI_1p0", "TZQ_ctZI_m1p0", "tZq"],
}

colors = { "Rares"       : r.TColor.GetColor("#3f90da"),
           "t#bar{t}Z"         : r.TColor.GetColor("#ffa90e"),
           "tZq"         : r.TColor.GetColor("#bd1f01"), 
           "Diboson"     : r.TColor.GetColor("#94a4a2"),
           "Non-prompt"       : r.TColor.GetColor("#832db6"), 
           "Conversions" : r.TColor.GetColor("#a96b59"),

}

other_colors = [r.TColor.GetColor(x) for x in [ "#e76300", "#b9ac70", "#717581", "#92dadd"]]

iPos = 0 
CMS.SetEnergy("")
CMS.ResetAdditionalInfo()
CMS.SetLumi("140 fb^{-1} (13 TeV) + 35 fb^{-1} (13.6 TeV)",unit="")

def transformHisto( histo ):
    if histo.InheritsFrom("TH1"):
        bins = [ histo.GetBinLowEdge(i+1) for i in range(histo.GetNbinsX())] + [ histo.GetBinLowEdge( histo.GetNbinsX() ) + histo.GetBinWidth( histo.GetNbinsX() )]
        bins_trans  = [x-5 for x in bins]
        histo_trans = ROOT.TH1F(histo.GetName() + "_trans", "", histo.GetNbinsX(), array('f', bins_trans))
        for x in range(histo.GetNbinsX()):
            histo_trans.SetBinContent( x+1, histo.GetBinContent(x+1))
            histo_trans.SetBinError  ( x+1, histo.GetBinError(x+1))
    else:
        histo_trans = histo.Clone(histo.GetName() + "_trans" + uuid.uuid4().hex)
        for i in range(histo_trans.GetN()):
            histo_trans.SetPointX(i, histo.GetPointX(i)-5)
    return histo_trans


def get_stack_total_data_from( fil, fit, channel ):
    tf=r.TFile.Open( fil )
    fit_shapes=tf.Get(f"shapes_{fit}/{channel}")
    total=fit_shapes.Get("total").Clone( "total"+channel ) 
    data=fit_shapes.Get("data").Clone( "data"+channel ) 
    stack={}

    for proc in process_groups:
        for samp in process_groups[proc]:
            if not fit_shapes.Get( samp ): continue
            if proc not in stack:
                stack[proc]=fit_shapes.Get( samp ).Clone( proc + fit + channel )
                stack[proc].SetFillColor( colors[proc] )
                stack[proc].SetLineColor( colors[proc] )
                if proc == "Non-prompt":
                    stack[proc].SetFillStyle(3005)
            else:
                stack[proc].Add( fit_shapes.Get( samp ) )
    total.SetDirectory(0)
    #data.SetDirectory(0)
    for x in stack: stack[x].SetDirectory(0)
    tf.Close()
    for x in stack: stack[x] = transformHisto(stack[x])
    
    return transformHisto(total), transformHisto(data), stack






plots = {
    'ch1' : {
        "rangex" : (-5,5),
        'rangey' : (-10.,40),
        'label' : "Discretized c_{tW}^{I} score",
        'process' : "tZq",
    },
    'ch2' : {
        "rangex" : (-5,5),
        'rangey' : (-10.,230),
        'label' : "Discretized c_{tZ}^{I} score",
        'process' : "t#bar{t}Z",
    },
}


for ch in plots:
    _, _ ,  sm        = get_stack_total_data_from( fil, "prefit", ch)
    _, _ , ctZI       = get_stack_total_data_from( fil_ctZI, "prefit", ch)
    _, _ , ctWI       = get_stack_total_data_from( fil_ctWI, "prefit", ch)
    _, _ , ctZI_quad  = get_stack_total_data_from( fil_ctZI_quad, "prefit", ch)
    _, _ , ctWI_quad  = get_stack_total_data_from( fil_ctWI_quad, "prefit", ch)

    print( plots[ch]['rangey'][0],plots[ch]['rangey'][1] ) 
    canv = CMS.cmsCanvas(f"canvas_{ch}",plots[ch]['rangex'][0],plots[ch]['rangex'][1],plots[ch]['rangey'][0],plots[ch]['rangey'][1], plots[ch]['label'],"Events",square=CMS.kSquare,extraSpace=0.01,iPos=iPos)
    
    leg = CMS.cmsLeg(0.175, 0.89 - 0.05 * 3, 0.44, 0.89, textSize=0.05)

    sm = sm[plots[ch]['process']]
    lin  = (ctWI      if ch == "ch1" else ctZI     )[plots[ch]['process']]
    quad = (ctWI_quad if ch == "ch1" else ctZI_quad)[plots[ch]['process']]

    lin .Add( sm, -1 )
    quad.Add( sm, -1 ); quad.Add( lin, -1.)
    if ch == 'ch2':
        lin.Scale(4)

    for what in [ sm, lin, quad]:
        what.SetFillColor(r.kWhite)
        for i in range(sm.GetNbinsX()):
            what.SetBinError(i+1,0.)
            
    
    sm.Draw('hist, same')
    lin.Draw("hist,same")
    quad.Draw("hist,same")


    sm.SetLineWidth(3)
    lin.SetLineWidth(3)
    quad.SetLineWidth(3)

    sm  .SetLineColor( other_colors[0] )
    lin .SetLineColor( other_colors[1] )
    quad.SetLineColor( other_colors[2] )
    
    
    leg.AddEntry( sm , "SM"       , "l")
    if ch == "ch2":
        leg.AddEntry(lin , "Linear x 4"   , "l")
    else:
        leg.AddEntry(lin , "Linear"   , "l")
    leg.AddEntry(quad, "Quadratic", "l")

    leg.Draw('same')
    
    canv.SaveAs( f"{ch}_distribution.png") 
    canv.SaveAs( f"{ch}_distribution.pdf") 
