import ROOT as r
import cmsstyle as CMS
r.gROOT.SetBatch(True)
import sys
import glob 
import numpy as np

CMS.SetEnergy("")
CMS.ResetAdditionalInfo()
CMS.SetLumi("",unit="")

grfs=[]
count=0
colors=[r.TColor.GetColor("#3f90da"), r.TColor.GetColor("#ffa90e"),
        r.TColor.GetColor("#bd1f01"),r.TColor.GetColor("#94a4a2"),
        r.TColor.GetColor("#832db6"), r.TColor.GetColor("#a96b59")]
graph_names=[]
for what in [ 'quad', 'lin']:
    for op in ['ctZI', 'ctWI']:
        for nsig in ["_2sig", ""]:
            points=[]
            for p in glob.glob(f'{sys.argv[1]}/criterion{nsig}_{what}_{op}_*.txt'):
                xaxis=float(p.split(f'criterion{nsig}_{what}_{op}_')[1].replace(".txt", ""))
                with open(p) as f:
                    if nsig == "":
                        yaxis=float(f.read().split( "This point is rejected at the 1 sigma level if the test stat -2*deltaNLL  >")[1].rstrip())
                    else:
                        yaxis=float(f.read().split( "This point is rejected at the 2 sigma level if the test stat -2*deltaNLL  >")[1].rstrip())
                if np.isnan(yaxis):
                    continue
                points.append((xaxis, yaxis))
            points.sort(key=lambda x : x[0])
            graph=r.TGraph(len(points))
            for i, (x,y) in enumerate(points):
                graph.SetPoint( i, x, y)
                graph.SetMarkerStyle(r.kFullCircle)
                graph.SetLineColor( colors[int(count/2)] )
                graph.SetMarkerColor( colors[int(count/2)] )
            nsigtext = nsig if nsig else "_1sig"
            print(np.array(points))
            np.savetxt( f'{sys.argv[1]}/{what}_{op}{nsigtext}', np.array(points) )
            grfs.append(graph)
            count=count+1
            graph_names.append( f'{what} {op}') 
        
canv = CMS.cmsCanvas("canv", -3, 3, 0, 10,  "Operator (ctZI or ctWI)", "-2#Delta NLL", square=CMS.kSquare)
leg = CMS.cmsLeg(0.5, 0.9 - 0.05 * 6, 0.7, 0.9, textSize=0.05)

for name, gr in zip(graph_names, grfs):
    gr.Draw("P,L,same")
    leg.AddEntry( gr, name, "PL")
                  
canv.SaveAs( "wilks_validity.pdf")
canv.SaveAs( "wilks_validity.png") 
            
outf.Close()
