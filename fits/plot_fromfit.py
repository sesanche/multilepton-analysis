import os
import ROOT 
import ROOT as r 
import cmsstyle as CMS
r.gROOT.SetBatch(True)
import sys




opvalue=2
fil          =sys.argv[1] + "/" +  "fitDiagnosticsasimov_sm.root"
fil_ctWI     =sys.argv[1] + "/" + f"fitDiagnosticsasimov_ctWI_{opvalue}.root"
fil_ctZI     =sys.argv[1] + "/" + f"fitDiagnosticsasimov_ctZI_{opvalue}.root"
fil_ctWI_quad=sys.argv[1] + "/" + f"fitDiagnosticsasimov_quad_ctWI_{opvalue}.root"
fil_ctZI_quad=sys.argv[1] + "/" + f"fitDiagnosticsasimov_quad_ctZI_{opvalue}.root"

fit="prefit"



process_groups = {
    "Conversions" : ["Conversions"],
    "Diboson"     : ["Diboson"],
    "Non-prompt"  : ["Fakes"],
    "Rares"       : ["Rares"],
    "t#bar{t}Z"   : ["TTLL_CPV_ctWI_1p0","TTLL_CPV_ctWI_m1p0","TTLL_CPV_ctZI_1p0", "TTLL_CPV_ctZI_m1p0", "TTZ"],
    "tZq"         : ["TZQ_ctWI_1p0", "TZQ_ctWI_m1p0","TZQ_ctZI_1p0", "TZQ_ctZI_m1p0", "tZq"],
}

colors = { "Rares"       : r.TColor.GetColor("#3f90da"),
           "t#bar{t}Z"         : r.TColor.GetColor("#ffa90e"),
           "tZq"         : r.TColor.GetColor("#bd1f01"), 
           "Diboson"     : r.TColor.GetColor("#94a4a2"),
           "Non-prompt"       : r.TColor.GetColor("#832db6"), 
           "Conversions" : r.TColor.GetColor("#a96b59"),

}

other_colors = [r.TColor.GetColor(x) for x in [ "#e76300", "#b9ac70", "#717581", "#92dadd"]]

iPos = 0 
CMS.SetEnergy("")
CMS.ResetAdditionalInfo()
CMS.SetLumi("140 fb^{-1} (13 TeV) + 35 fb^{-1} (13.6 TeV)",unit="")


def get_stack_total_data_from( fil, fit, channel ):
    tf=r.TFile.Open( fil )
    fit_shapes=tf.Get(f"shapes_{fit}/{channel}")
    total=fit_shapes.Get("total").Clone( "total"+channel ) 
    data=fit_shapes.Get("data").Clone( "data"+channel ) 
    stack={}

    for proc in process_groups:
        for samp in process_groups[proc]:
            if proc not in stack:
                stack[proc]=fit_shapes.Get( samp ).Clone( proc + fit + channel )
                stack[proc].SetFillColor( colors[proc] )
                stack[proc].SetLineColor( colors[proc] )
                if proc == "Non-prompt":
                    stack[proc].SetFillStyle(3005)
            else:
                stack[proc].Add( fit_shapes.Get( samp ) )
    total.SetDirectory(0)
    #data.SetDirectory(0)
    for x in stack: stack[x].SetDirectory(0)
    tf.Close()
    return total, data, stack

def get_hist_from( fil, fit, channel, proc ):
    tf=r.TFile.Open( fil )
    fit_shapes=tf.Get(f"shapes_{fit}/{channel}/{proc}").Clone( fit + channel + proc )
    fit_shapes.SetDirectory(0)
    tf.Close()
    return fit_shapes




plots = {
    'ch1' : {
        "rangex" : (0,10),
        'rangey' : (0.,300),
        'label' : "c_{tW}^{I} score"
    },
    'ch2' : {
        "rangex" : (0,10),
        'rangey' : (0.,600),
        'label' : "c_{tZ}^{I} score"
    },
}


for ch in plots:
    total, data, hist_dict=get_stack_total_data_from( fil, fit, ch)


    # plot without ratio
    stack = ROOT.THStack("stack", "Stacked")
    canv = CMS.cmsCanvas(f"canvas/{ch}",plots[ch]['rangex'][0],plots[ch]['rangex'][1],plots[ch]['rangey'][0],plots[ch]['rangey'][1], plots[ch]['label'],"Events",square=CMS.kSquare,extraSpace=0.01,iPos=iPos)
    
    leg = CMS.cmsLeg(0.175, 0.89 - 0.05 * 9, 0.44, 0.89, textSize=0.05)

    # Put samples in a dict {sample: th1} and draw
    CMS.cmsDrawStack(stack, leg, hist_dict, data=None)

    ctZI, _, _ = get_stack_total_data_from( fil_ctZI, "prefit", ch)
    ctWI, _, _ = get_stack_total_data_from( fil_ctWI, "prefit", ch)
    ctZI_quad, _, _ = get_stack_total_data_from( fil_ctZI_quad, "prefit", ch)
    ctWI_quad, _, _ = get_stack_total_data_from( fil_ctWI_quad, "prefit", ch)

    ctZI.SetLineWidth(3); ctZI.SetLineColor( other_colors[0] ) 
    ctWI.SetLineWidth(3); ctWI.SetLineColor( other_colors[1] )
    ctZI_quad.SetLineWidth(3); ctZI_quad.SetLineColor( other_colors[0] ); ctZI_quad.SetLineStyle( 2 ) 
    ctWI_quad.SetLineWidth(3); ctWI_quad.SetLineColor( other_colors[1] ); ctWI_quad.SetLineStyle( 2 ) 

    ctWI.Draw("hist,same")
    ctZI.Draw("hist,same")
    ctWI_quad.Draw("hist,same")
    ctZI_quad.Draw("hist,same")
    
    leg.AddEntry( ctWI, "c_{tW}^{I}=%4.1f"%opvalue, "l")
    leg.AddEntry( ctZI, "c_{tZ}^{I}=%4.1f"%opvalue, "l")

    
    canv.SaveAs( f"{ch}.png") 


    # plot with ratio
    dicanv = CMS.cmsDiCanvas(f"canvas_ratio_{ch}",plots[ch]['rangex'][0],plots[ch]['rangex'][1],plots[ch]['rangey'][0],plots[ch]['rangey'][1], 0.2,1.8, plots[ch]['label'],"Events","BSM/SM", square=CMS.kSquare,extraSpace=0.1,iPos=iPos)
    stack2 = ROOT.THStack("stack2", "Stacked2")
    dicanv.cd(1)
    leg = CMS.cmsLeg(0.175, 0.89 - 0.05 * 9, 0.44, 0.89, textSize=0.05)
    CMS.cmsDrawStack(stack2, leg, hist_dict, data=None)
    ctWI.Draw("hist,same")
    ctZI.Draw("hist,same")
    ctWI_quad.Draw("hist,same")
    ctZI_quad.Draw("hist,same")



    CMS.cmsDraw(total, "e2same0", lcolor = 335, lwidth = 1, msize = 0, fcolor = ROOT.kBlack, fstyle = 3004,)
    data.Draw("P,same")
    
    leg.AddEntry( total, "Uncertainty", 'f')
    leg.AddEntry( ctWI, "c_{tW}^{I}=%4.1f"%opvalue, "l")
    leg.AddEntry( ctZI, "c_{tZ}^{I}=%4.1f"%opvalue, "l")
    total.SetLineColor(0)

    dicanv.cd(2)
    ctWI_ratio=ctWI.Clone()
    ctWI_ratio.Divide( stack2.GetStack()[len(hist_dict)-1] )

    ctZI_ratio=ctZI.Clone()
    ctZI_ratio.Divide( stack2.GetStack()[len(hist_dict)-1] )

    ctWI_quad_ratio=ctWI_quad.Clone()
    ctWI_quad_ratio.Divide( stack2.GetStack()[len(hist_dict)-1] )

    ctZI_quad_ratio=ctZI_quad.Clone()
    ctZI_quad_ratio.Divide( stack2.GetStack()[len(hist_dict)-1] )

    ctZI_ratio.Draw("hist,same")
    ctWI_ratio.Draw("hist,same")
    ctZI_quad_ratio.Draw("hist,same")
    ctWI_quad_ratio.Draw("hist,same")



    dicanv.SaveAs( f"{ch}_ratio.png") 
    dicanv.SaveAs( f"{ch}_ratio.pdf") 
    
