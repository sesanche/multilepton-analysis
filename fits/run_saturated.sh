cards=$1
what=$2 # run or collect

# change directory - only argument is the directory with the cards
pushd /afs/cern.ch/work/s/sesanche/private/CPV_code/combine/CMSSW_14_1_0_pre4/src
cmsenv
popd
pushd $cards


if [ "$what" == "run" ]; then

    combineTool.py wps.root       -M GoodnessOfFit --algo=saturated -t 50 -s 1000:1019:1 -n lin  --job-mode condor --sub-opts='+JobFlavour="workday" \n +AccountingGroup = "group_u_CMST3.all"' --task-name lin
    combineTool.py wps_quad.root  -M GoodnessOfFit --algo=saturated -t 50 -s 1000:1019:1 -n quad  --job-mode condor --sub-opts='+JobFlavour="workday"\n +AccountingGroup = "group_u_CMST3.all"' --task-name quad

    combineTool.py wps.root       -M GoodnessOfFit --algo=saturated -n lin_obs
    combineTool.py wps_quad.root  -M GoodnessOfFit --algo=saturated -n quad_obs


    combineTool.py wps.root       -M GoodnessOfFit --algo=saturated -t 50 --freezeParameters ctWI,ctZI --setParameters ctWI=0,ctZI=0 -s 1000:1019:1 -n lin_sm  --job-mode condor --sub-opts='+JobFlavour="workday" \n +AccountingGroup = "group_u_CMST3.all"' --task-name lin_sm
    combineTool.py wps_quad.root  -M GoodnessOfFit --algo=saturated -t 50 --freezeParameters ctWI,ctZI --setParameters ctWI=0,ctZI=0 -s 1000:1019:1 -n quad_sm  --job-mode condor --sub-opts='+JobFlavour="workday"\n +AccountingGroup = "group_u_CMST3.all"' --task-name quad_sm

    combineTool.py wps.root       -M GoodnessOfFit --algo=saturated --freezeParameters ctWI,ctZI --setParameters ctWI=0,ctZI=0 -n lin_sm_obs
    combineTool.py wps_quad.root  -M GoodnessOfFit --algo=saturated --freezeParameters ctWI,ctZI --setParameters ctWI=0,ctZI=0 -n quad_sm_obs

elif [ "$what" == "collect" ]; then
    for what in lin quad lin_sm quad_sm; do
        hadd -f higgsCombine${what}.GoodnessOfFit.mH120.merged.root higgsCombine${what}.GoodnessOfFit.mH120.1*.root
        combineTool.py -M CollectGoodnessOfFit --input higgsCombine${what}.GoodnessOfFit.mH120.merged.root higgsCombine${what}_obs.GoodnessOfFit.mH120.root  -o goodness_of_fit_${what}.json
        plotGof.py goodness_of_fit_${what}.json --statistic saturated --mass 120.0 -o goodness_of_fit_${what}
    done
fi 
