# change directory - only argument is the directory with the cards
pushd $1 

# # Linear fits (fixed) with and without uncertainties
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n unblind_lin_fix_ctWI --setParameterRanges ctWI=-5,5 
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n unblind_lin_fix_ctZI --setParameterRanges ctZI=-10,10  
plot1DScan.py higgsCombineunblind_lin_fix_ctWI.MultiDimFit.mH120.root --POI ctWI -o unblind_scan_lin_fix_ctWI
plot1DScan.py higgsCombineunblind_lin_fix_ctZI.MultiDimFit.mH120.root --POI ctZI -o unblind_scan_lin_fix_ctZI

# # Linear fits (profiled) with and without uncertainties - to update in unblinded
combineTool.py wps.root -M MultiDimFit --algo grid -P ctWI -n unblind_lin_prof_ctWI --setParameterRanges ctWI=-5,5  --floatOtherPOIs 1 
combineTool.py wps.root -M MultiDimFit --algo grid -P ctZI -n unblind_lin_prof_ctZI --setParameterRanges ctZI=-10,10  --floatOtherPOIs 1 
plot1DScan.py higgsCombineunblind_lin_prof_ctWI.MultiDimFit.mH120.root --POI ctWI -o unblind_scan_lin_prof_ctWI
plot1DScan.py higgsCombineunblind_lin_prof_ctZI.MultiDimFit.mH120.root --POI ctZI -o unblind_scan_lin_prof_ctZI


# # Quadratic fits (fixed) with and without uncertainties - to update in unblinded
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n unblind_quad_fix_ctWI --setParameterRanges ctWI=-5,5 
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n unblind_quad_fix_ctZI --setParameterRanges ctZI=-5,5  
python3 ../fits/plot1DScan.py higgsCombineunblind_quad_fix_ctWI.MultiDimFit.mH120.root --POI ctWI -o unblind_scan_quad_fix_ctWI --crossing1sigma quad_ctWI_1sig  --crossing2sigma quad_ctWI_2sig
python3 ../fits/plot1DScan.py higgsCombineunblind_quad_fix_ctZI.MultiDimFit.mH120.root --POI ctZI -o unblind_scan_quad_fix_ctZI --crossing1sigma quad_ctZI_1sig  --crossing2sigma quad_ctZI_2sig

# Quadratic fits (profiled) with and without uncertainties - to update in unblinded
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -n unblind_quad_prof_ctWI --setParameterRanges ctWI=-5,5  --floatOtherPOIs 1 
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctZI -n unblind_quad_prof_ctZI --setParameterRanges ctZI=-5,5  --floatOtherPOIs 1 
python3 ../fits/plot1DScan.py higgsCombineunblind_quad_prof_ctWI.MultiDimFit.mH120.root --POI ctWI -o unblind_scan_quad_prof_ctWI --crossing1sigma quad_ctWI_1sig  --crossing2sigma quad_ctWI_2sig
python3 ../fits/plot1DScan.py higgsCombineunblind_quad_prof_ctZI.MultiDimFit.mH120.root --POI ctZI -o unblind_scan_quad_prof_ctZI --crossing1sigma quad_ctZI_1sig  --crossing2sigma quad_ctZI_2sig


# FitDiagnostics for plots 
combineTool.py wps.root -M FitDiagnostics  -n unblind_bestfit   --setParameters ctWI=0,ctZI=0 --saveShapes --saveWithUncertainties
combineTool.py wps_quad.root -M FitDiagnostics  -n unblind_bestfit_quad   --setParameters ctWI=0,ctZI=0 --saveShapes --saveWithUncertainties

combineTool.py wps.root      -M MultiDimFit --algo grid -P ctWI -P ctZI -n unblind_lin_ctZI_ctWI  --setParameterRanges ctWI=-7,5:ctZI=-15,15  --points 5000
combineTool.py wps_quad.root -M MultiDimFit --algo grid -P ctWI -P ctZI -n unblind_quad_ctZI_ctWI --setParameterRanges ctWI=-7,5:ctZI=-4,4  --points 5000

python3 ../fits/plot_likelihood_2d.py higgsCombineunblind_lin_ctZI_ctWI.MultiDimFit.mH120.root unblind_plot_lin_ctZI_ctWI -6 2.5 -1 11 20
python3 ../fits/plot_likelihood_2d.py higgsCombineunblind_quad_ctZI_ctWI.MultiDimFit.mH120.root unblind_plot_quad_ctZI_ctWI  -4 4 -2 3 40


# dNLL wrt SM
combineTool.py wps_quad.root -M MultiDimFit --algo fixed -P ctZI -P ctWI -n unblind_quad_prof_fixed_SM --setParameterRanges ctWI=-5,5  --floatOtherPOIs 1 --fixedPointPOIs ctZI=0,ctWI=0 
combineTool.py wps.root      -M MultiDimFit --algo fixed -P ctZI -P ctWI -n unblind_lin_prof_fixed_SM  --setParameterRanges ctZI=-10,10  --floatOtherPOIs 1 --fixedPointPOIs ctZI=0,ctWI=0

combineTool.py wps_quad.root -M MultiDimFit --algo fixed -P ctWI -n unblind_quad_ctWI_fixed_SM --setParameterRanges ctWI=-10,10    --floatOtherPOIs 0 --fixedPointPOIs ctZI=0,ctWI=0 
combineTool.py wps.root      -M MultiDimFit --algo fixed -P ctWI -n unblind_lin_ctWI_fixed_SM  --setParameterRanges ctWI=-10,10    --floatOtherPOIs 0  --fixedPointPOIs ctZI=0,ctWI=0

combineTool.py wps_quad.root -M MultiDimFit --algo fixed -P ctZI -n unblind_quad_ctZI_fixed_SM --setParameterRanges ctWI=-10,10    --floatOtherPOIs 0 --fixedPointPOIs ctZI=0,ctWI=0 
combineTool.py wps.root      -M MultiDimFit --algo fixed -P ctZI -n unblind_lin_ctZI_fixed_SM  --setParameterRanges ctWI=-10,10    --floatOtherPOIs 0  --fixedPointPOIs ctZI=0,ctWI=0
