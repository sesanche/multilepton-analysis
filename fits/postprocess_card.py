import sys
import ROOT as r
r.gROOT.SetBatch(True)
from copy import deepcopy 
import CombineHarvester.CombineTools.ch as ch 
print("Warning, this script is very hardcoded, edit with extreme care")


channels = ['ch1', 'ch2']

if __name__=="__main__":
    cardname=sys.argv[1]
    doperyear=sys.argv[2] if len(sys.argv) > 2 else False

    if doperyear:
        channels=["SR_3l_ctWI_2016APV_","SR_3l_ctWI_2016_","SR_3l_ctWI_2017_","SR_3l_ctWI_2018_","SR_3l_ctWI_2022EE_","SR_3l_ctWI_2022_","SR_3l_ctZI_2016APV_","SR_3l_ctZI_2016_","SR_3l_ctZI_2017_","SR_3l_ctZI_2018_","SR_3l_ctZI_2022EE_","SR_3l_ctZI_2022_"]
    
    
    cb = ch.CombineHarvester()
    cb.SetFlag('workspaces-use-clone', True)
    cb.SetFlag('import-parameter-err', False)
    cb.SetFlag('filters-use-regex', True)
    cb.SetFlag('check-negative-bins-on-import', False)

    cb.ParseDatacard(cardname)
    tzq_ratio={}
    for bin in cb.bin_set():
        print( cb.cp().bin([bin]).process_set())
        tzq_lo=cb.cp().bin([bin]).cp().process(["TZQ_CPV_sm"])
        tzq_nlo=cb.cp().bin([bin]).cp().process(["tZq"])
        numerator=tzq_nlo.GetShape()
        denominator=tzq_lo.GetShape()
        numerator.Divide(denominator)
        tzq_ratio[bin]=numerator
        for i in range(tzq_ratio[bin].GetNbinsX()):
            tzq_ratio[bin].SetBinError(i+1,0.)


    def ModifyProc( p, proc ):
        newname=proc.replace( "_CPV", "")
        p.set_process(newname)
        updated_hist=p.ShapeAsTH1F()
        updated_hist.Scale( p.rate() )
        updated_hist.Multiply( tzq_ratio[p.bin()])
        p.set_shape( updated_hist, True )

    def ModifySyst( chob, p, proc ):
        nomshape_prerename=chob.cp().process([p.process()]).bin([p.bin()]).GetShape() #Need to get the original process normalization, not the renamed one which we already adjusted
        newname=proc.replace( "_CPV", "")
        p.set_process(newname) #Rename the process for the systs 
        updated_hist_up=p.ShapeUAsTH1F()
        updated_hist_up.Scale(p.value_u()*nomshape_prerename.Integral()) #Update to correct normalization before correcting the shape
        updated_hist_down=p.ShapeDAsTH1F()
        updated_hist_down.Scale(p.value_d()*nomshape_prerename.Integral()) #Same as for hist_up


        updated_hist_up.Multiply(tzq_ratio[p.bin()])
        updated_hist_down.Multiply(tzq_ratio[p.bin()])
        nomshape=chob.cp().process([p.process()]).bin([p.bin()]).GetShape() #Now get the nominal 
        p.set_shapes(updated_hist_up,updated_hist_down,nomshape)  #and update up/down shapes


        
    to_transform=['TZQ_CPV_ctWI_1p0', 'TZQ_CPV_ctWI_1p0_ctZI_1p0', 'TZQ_CPV_ctWI_m1p0', 'TZQ_CPV_ctZI_1p0', 'TZQ_CPV_ctZI_m1p0', 'TZQ_CPV_sm']
    for proc in to_transform:
        ch.CloneProcs( cb.cp().process([proc] ), cb, lambda x : ModifyProc(x, proc))
        ch.CloneSysts( cb.cp().process([proc] ), cb, lambda x : ModifySyst(cb, x, proc))
    
    
    # i remove the LO samples, also ttZ because we still have the nlo ones
    for proc in to_transform + [ 'TZQ_sm'] + [ 'TTZ_CPV_sm']: # i also remove ttz 
        cb.FilterProcs( lambda p : p.process() == proc )

    
    cb.WriteDatacard(cardname.replace(".dat", "_nlo.dat"), cardname.replace(".dat", "_nlo.root"))

    newcard=open(cardname.replace(".dat", "_nlo.dat")).readlines()
    for lin in newcard:
        if "process" in lin and "TZQ" in lin:
            processes_list=lin.split()
            break
    newline="tzq_nlo        shape       "
    for proc in processes_list[1:]:
        print(proc)
        newline += ("1" if proc.startswith("TZQ") else "-") + "         "
    newline+='\n'
    newcard=newcard[:-2]+[newline]+newcard[-2:]
    outf=open( cardname.replace(".dat", "_nlo.dat"), 'w')
    outf.write("".join(newcard))

    tf=r.TFile.Open( cardname.replace(".dat", "_nlo.root"), "update" )
    for channel in channels:
        for proc in [ 'TZQ_CPV_ctWI_1p0', 'TZQ_CPV_ctWI_1p0_ctZI_1p0', 'TZQ_CPV_ctWI_m1p0', 'TZQ_CPV_ctZI_1p0', 'TZQ_CPV_ctZI_m1p0' ]:
            procname=proc.replace("_CPV", "")
            nominal=tf.Get(f"{channel}/{procname}")
            dn=nominal.Clone(procname + "_tzq_nloDown")
            up=nominal.Clone(procname + "_tzq_nloUp")
            up.Divide( tzq_ratio[channel] )
            print( f'{channel}/{procname}_tzq_nloDown' )
            tf.Get( channel ).WriteTObject( dn, f'{procname}_tzq_nloDown')
            tf.Get( channel ).WriteTObject( up, f'{procname}_tzq_nloUp')
    tf.Close()
            

            
