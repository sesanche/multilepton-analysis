#include <ROOT/RVec.hxx>
#include "Math/GenVector/LorentzVector.h"
#include "Math/GenVector/PtEtaPhiM4D.h"
#include "Math/GenVector/PxPyPzM4D.h"
#include "Math/GenVector/Boost.h"
#include <random>
#include <algorithm>    // std::shuffle



typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>> PtEtaPhiMVector;

struct cpvTraining {
  float _input_zplus_px, _input_zplus_py, _input_zplus_pz, _input_zminus_px, _input_zminus_py, _input_zminus_pz;
  float _input_lw_px, _input_lw_py, _input_lw_pz;
  float _input_lw_q;
  float _input_met_px, _input_met_py;
  float _input_j1_px=0., _input_j1_py=0., _input_j1_pz=0., _input_j1_btag=0.;
  float _input_j2_px=0., _input_j2_py=0., _input_j2_pz=0., _input_j2_btag=0.;
  float _input_j3_px=0., _input_j3_py=0., _input_j3_pz=0., _input_j3_btag=0.;
  float _input_j4_px=0., _input_j4_py=0., _input_j4_pz=0., _input_j4_btag=0.;
  float _input_j5_px=0., _input_j5_py=0., _input_j5_pz=0., _input_j5_btag=0.;

};

template<class T>
void removeValueFromVec( std::vector<T>& vec, T val){
  auto it= std::find( vec.begin(), vec.end(), val);
  vec.erase(it);
}

auto randomgenerator=std::default_random_engine(42); 
cpvTraining prepareTraining( const ROOT::RVec<PtEtaPhiMVector>& lep_p4, const ROOT::RVec<int>& lep_pdgId,
                             const ROOT::RVec<PtEtaPhiMVector>& jet_p4, const ROOT::RVec<int>& jet_bMediumMask, const ROOT::RVec<int>& jet_blooseMedium,
                             const std::pair<size_t, size_t>& Z_candidate_pair, float MET_pt, float MET_phi)
{

  // w lepton is the leading lepton that is not in the Z boson candidate. need to improve this, the Z should be among the leading 3 
  std::vector<size_t> all_indices; for (size_t i=0; i<lep_p4.size(); i++) all_indices.push_back( i );
  removeValueFromVec<size_t>( all_indices, Z_candidate_pair.first);
  removeValueFromVec<size_t>( all_indices, Z_candidate_pair.second);

  size_t lw_index=all_indices.at(0);

  size_t zplus_index =(lep_pdgId.at(Z_candidate_pair.first) < 0) ? Z_candidate_pair.first : Z_candidate_pair.second;
  size_t zminus_index=(lep_pdgId.at(Z_candidate_pair.first) > 0) ? Z_candidate_pair.first : Z_candidate_pair.second;

  cpvTraining ret;
  ret._input_zplus_px=lep_p4.at( zplus_index ).Px();   ret._input_zplus_py=lep_p4.at( zplus_index ).Py();   ret._input_zplus_pz=lep_p4.at( zplus_index ).Pz();  
  ret._input_zminus_px=lep_p4.at( zminus_index ).Px(); ret._input_zminus_py=lep_p4.at( zminus_index ).Py(); ret._input_zminus_pz=lep_p4.at( zminus_index ).Pz();
  ret._input_lw_px=lep_p4.at( lw_index ).Px(); ret._input_lw_py=lep_p4.at( lw_index ).Py(); ret._input_lw_pz=lep_p4.at( lw_index ).Pz();
  ret._input_lw_q= -lep_pdgId.at( lw_index ) / abs( lep_pdgId.at( lw_index ) );

  size_t j1_index, j2_index;


  auto bjetScore = jet_bMediumMask+jet_blooseMedium; // medium=2, loose=1

  if (jet_p4.size() > 0){
    ret._input_j1_px  =jet_p4.at( 0 ).Px();
    ret._input_j1_py  =jet_p4.at( 0 ).Py();
    ret._input_j1_pz  =jet_p4.at( 0 ).Pz();
    ret._input_j1_btag=bjetScore.at(0);
  }

  if (jet_p4.size() > 1){
    ret._input_j2_px  =jet_p4.at( 1 ).Px();
    ret._input_j2_py  =jet_p4.at( 1 ).Py();
    ret._input_j2_pz  =jet_p4.at( 1 ).Pz();
    ret._input_j2_btag=bjetScore.at(1);
  }

  if (jet_p4.size() > 2){
    ret._input_j3_px  =jet_p4.at( 2 ).Px();
    ret._input_j3_py  =jet_p4.at( 2 ).Py();
    ret._input_j3_pz  =jet_p4.at( 2 ).Pz();
    ret._input_j3_btag=bjetScore.at(2);
  }

  if (jet_p4.size() > 3){
    ret._input_j4_px  =jet_p4.at( 3 ).Px();
    ret._input_j4_py  =jet_p4.at( 3 ).Py();
    ret._input_j4_pz  =jet_p4.at( 3 ).Pz();
    ret._input_j4_btag=bjetScore.at(3);
  }
  if (jet_p4.size() > 4){
    ret._input_j5_px  =jet_p4.at( 4 ).Px();
    ret._input_j5_py  =jet_p4.at( 4 ).Py();
    ret._input_j5_pz  =jet_p4.at( 4 ).Pz();
    ret._input_j5_btag=bjetScore.at(4);
  }


  ret._input_met_px=MET_pt*TMath::Cos(MET_phi);
  ret._input_met_py=MET_pt*TMath::Sin(MET_phi);

  

  return ret;
  
  
  
  
}


struct cpv_weights{
  float _input_weight_sm, _input_weight_lin_ctZI, _input_weight_lin_ctWI, _input_weight_lin_ctGI;
};



cpv_weights get_cpv_weights( const ROOT::RVec<float>& weights ) {
  cpv_weights ret;
  if (weights.size() > 0){
    ret._input_weight_sm = weights[81];
    ret._input_weight_lin_ctZI = (weights[11]-weights[12])/2.;
    ret._input_weight_lin_ctWI = (weights[7] -weights[8] )/2.;
    ret._input_weight_lin_ctGI = (weights[15]-weights[16])/2.;
  }
  else{
    ret._input_weight_sm = 0.;
    ret._input_weight_lin_ctZI = 0.;
    ret._input_weight_lin_ctWI = 0.;
    ret._input_weight_lin_ctGI = 0.;
  }
  return ret;
}
